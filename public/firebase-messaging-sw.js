importScripts("https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js")
importScripts("https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js")

firebase.initializeApp({
  'messagingSenderId': '831487422014'
})

const notifiToString = (res) => {
  console.log('----------------->',res);
  const notificationType = res.notificationType
  const friendName = res.friendName
  const postId = res.postId
  const message = res.message
  
  switch (notificationType){
    case 'push_approved_post': {
      return {
        msg: "タイムラインが承認されました",
        link: "/home/timeline",
      }
    }
    case 'push_denied_post': {
      return {
        msg: "タイムラインが否認されました",
        link: "/home/timeline",
      }
    }
    case 'push_approved_comment': {
      return {
        msg: "タイムラインコメントが承認されました",
        link: "/home/timeline",
      }
    }
    case 'push_denied_comment': {
      return {
        msg: "タイムラインコメントが否認されました",
        link: "/home/timeline",
      }
    }
    case 'push_approved_subcomment': {
      return {
        msg: "コメント返信が承認されました",
        link: "/home/timeline",
      }
    }
    case 'push_denied_subcomment': {
      return {
        msg: "コメント返信が否認されました",
        link: "/home/timeline",
      }
    }
    case 'push_approved_profile': {
      return {
        msg: "プロフィール情報が更新されました",
        link: "/home/setting"
      }
    }
    case 'push_modify_teller_avatar': {
      return {
        msg: "プロフィール画像の加工が完了しました。",
        link: "/home/setting"
      }
    }
    case 'push_approved_teller_avatar': {
      return {
        msg: "プロフィール画像は承認されました。",
        link: "/home/setting"
      }
    }
    case 'push_denied_teller_avatar': {
      return {
        msg: "プロフィール画像は否認されました。",
        link: "/home/setting"
      }
    }
    case 'push_denied_apart_user_profile': {
      return {
        msg: "一部のプロフィール情報が更新されませんでした。",
        link: "/home/setting"
      }
    }
    case 'push_approved_apart_user_profile': {
      return {
        msg: "プロフィール情報が一部更新されました",
        link: "/home/setting"
      }
    }
    case 'push_denied_profile': {
      return {
        msg: "プロフィール情報の更新が否認されました",
        link: "/home/setting"
      }
    }
    case 'push_comment_post': {
      return {
        msg: friendName + "さんからタイムラインにコメントがありました",
        link: '/home/timeline'
      }
    }
    case 'push_appeal_comment_approved': {
      return {
        msg: "一言コメントが承認されました",
        link: '/home'
      }
    }
    case 'push_appeal_comment_denied': {
      return {
        msg: "一言コメントが否認されました。",
        link: '/home'
      }
    }
    case 'push_admin_send_message': {
      return {
        msg: message,
        link: '/home'
      }
    }
    case 'push_like': {
      return {
        msg: friendName + "さんからいいねがつきました",
        link: '/home/timeline'
      }
    }
    case 'push_user_favorited_teller': {
      return {
        msg: friendName + "さんがお気に入り登録しました",
        link: '/home/customermanagement/follower'
      }
    }
    case 'push_request_chat': {
      return {
        msg: friendName + "さんからチャット相談リクエストがありました",
        link: '/home'
      }
    }
    case 'push_request_call': {
      return {
        msg: friendName + "さんから電話相談リクエストがありました",
        link: '/home'
      }
    }
    case 'push_cancel_expected_request_chat': {
      const userId = res.userId;
      return {
        msg: "チャットリクエストがキャンセルされました。",
        link: `/home/chat/${userId}`
      }
    }
    case 'push_cancel_expected_request_call': {
      const userId = res.userId;
      return {
        msg: "通話リクエストがキャンセルされました。",
        link: `/home/chat/${userId}`
      }
    }
    case 'push_login_bonus': {
      return {
        msg: 'ログインボーナスを獲得しました',
        link: '/home'
      }
    }
    case 'push_daily_bonus': {
      return {
        msg: 'ログインボーナスを獲得しました',
        link: '/home'
      }
    }
    case 'push_complete_mission': {
      return {
        msg: 'ミッションをクリアしました',
        link: '/home'
      }
    }
    default: {
      return {
        msg: notificationType + friendName,
        link: '/home'
      }
    }
  }
}


const messaging = firebase.messaging()
messaging.setBackgroundMessageHandler( payload => {
  const pushData = JSON.parse(payload.data.aps)
  const pushDetail = notifiToString({
    notificationType: pushData.alert["loc-key"],
    friendName: (pushData.alert["loc-args"]['0']) ? pushData.alert["loc-args"]['0'] : '',
    postId: (pushData.data.postId !== undefined) ? pushData.data.postId : '',
    message: (pushData.data.message !== undefined ) ? pushData.data.message : '',
    userId: (pushData.data.userId !== undefined ) ? pushData.data.userId : ''
  })
  return self.registration.showNotification("ステラ", {
    body: pushDetail.msg,
    icon: "/img/icon_webpush.png",
    badge: "/img/icon_webpush.png",
    vibration: [200,100,200,100,200],
    data:{
      url: pushDetail.link
    }
  })
})

self.addEventListener("notificationclick", function(event) {

  event.notification.close();
  const urlPath = event.notification.data.url ? event.notification.data.url : '/home'
  const urlToOpen = new URL(urlPath, self.location.origin).href;
  const promiseChain = clients.matchAll({
    type: 'window',
    includeUncontrolled: true
  })
  .then(function(windowClients) {
    let matchingClient = null
    for (var i = 0; i < windowClients.length; i++) {
      client = windowClients[i];
      if ( client.url === urlToOpen && 'focused' in client) {
        if( client.visibilityState === 'hidden'){
          matchingClient = client
          break
        }
      }
    }
    if (matchingClient) {
      return matchingClient.focus()
    }
    return clients.openWindow(urlToOpen)
  })
  
  event.waitUntil(promiseChain)

})