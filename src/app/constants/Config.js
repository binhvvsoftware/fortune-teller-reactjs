
const InitConfig = () => {
  if(process.env.REACT_APP_ENV==="prod"){
    return {
      api_base_url: "https://api.stella-app.com",
      ws_base_url: "https://ws.stella-app.com",
      img_base_url: "https://picture.stella-app.com",
      staff_blog_url: "https://wp.stella-app.com/wp-json/wp/v2/posts",
      outside_base_url: "https://api2.stella-app.com"
    }
  } else {
    return {
      api_base_url: "https://api.dev.stella-app.com",
      ws_base_url: "https://ws.dev.stella-app.com",
      img_base_url: "https://picture.dev.stella-app.com",
      staff_blog_url: "https://wp.dev.stella-app.com/wp-json/wp/v2/posts",
      outside_base_url: "https://api2.dev.stella-app.com"
    }
  }
}

const initConfig = InitConfig()

/**
 * Glasに関する設定
 */
export const glasConfig = {
  application: "stella",
  url_base: initConfig.api_base_url,
  url_websocket: initConfig.ws_base_url,
  url_image: initConfig.img_base_url,
  url_outside: initConfig.outside_base_url,
  staff_blog_url: initConfig.staff_blog_url,
  path_teller_login: '/ums/v1/fortunetellersessions/',
  path_teller_info: '/ums/v1/fortunetellers/',
  path_password: '/ums/v1/tellerverifycodes',
  path_user_info: '/ums/v1/users/',
  path_last_chat: '/chat/v1/lastchats/',
  path_chat_request: '/ums/v1/requests/',
  path_offline_request: '/ums/v1/offlineuserrequests/',
  path_news: '/ums/v1/news',
  path_file: '/static/v1/files/',
  path_chat_file : '/chat/v1/files',
  path_template: '/ums/v1/templates/',
  path_memos: '/ums/v1/memos/',
  path_logcalls: '/ums/v1/logcalls',
  path_chat_history: '/chat/v1/histories',
  path_jpns_notifications: '/jpns/v1/notifications',
  path_timeline: '/buzz/v1/posts/',
  path_timeline_like: '/buzz/v1/likes/',
  path_timeline_comment: '/buzz/v1/comments/',
  path_timeline_subcomment: '/buzz/v1/subcomments/',
  path_image: '/images',
  path_ageverify: '/static/v1/ageverify',
  path_review: '/ums/v1/reviews',
  path_customer_report: '/ums/v1/customerreports/',
  path_point_feasibleword: '/ums/v1/point/feasibleword',
  path_payment: '/payoff',
  path_payment_reports: '/payment/v1/reports',
  path_payment_history: '/payoff/history',
  path_bank: '/bank',
  path_bank_branch: '/siten',
  path_bank_account: '/account',
  refresh_token: '/ums/v1/token',
  path_missions: '/ums/v1/missions',
  length_mood: 32,
  chatrequest_limit: 90,
  leftmenu_maxlength_history: 100,
  leftmenu_maxlength_follower: 100,
  reward_commission: 540,
  payoff_min: 5540,
  chatpoint_rate: 1.0,
  setting: {
    name: 12,
    genres: 9,
    methods: 6,
    styles: 3,
    message: 1500,
    phone: 11,
    profile: {
      width: 300,
      height: 300,
      drag: 1,
      maxTop: 200,
      circle: 125
    }
  },
  template: {
    template: 10,
    title: 6
  },
  account: {
    password: 6
  },
  bank: {
    accountNo: 7,
    yucho: '9900'
  },
  timeline: {
    getPostSize: 20,
    autoLoadPixel: 100
  },
  usermanagement: {
    management: 10,
    follower: 20,
    autoLoadPixel: 96
  },
  review: {
    size: 20,
    autoLoadPixel: 100
  }
}



/**
 * 待機ステータス
 */
export const fortuneTellerStatusToString = {
  0: "チャット/通話待機中",
  1: "チャットのみ待機中",
  2: "通話のみ待機中",
  3: "チャット相談中",
  4: "チャット相談中",
  5: "通話相談中",
  6: "オフライン",
}

/**
 * 待機ステータス
 */
export const fortuneTellerStatus = {
  "waiting": 0, 
  "chat": 1,
  "call": 2,
  "chatting": 3,
  "chattingFull": 4,
  "calling": 5,
  "offline": 6,
}


/**
 * リクエストタイプ
 */
export const requestTypes = {
  1: "チャット",
  2: "通話",
  3: "？？",
  4: "？？",
}



/**
 * 外部APIに関しての設定
 */
export const NexivConfig = {

}

/**
 * 銀行
 */
export const representativeBank = [
  {"code": "0005", "name": "三菱東京UFJ銀行"},
  {"code": "0001", "name": "みずほ銀行"},
  {"code": "0009", "name": "三井住友銀行"},
  {"code": "0010", "name": "りそな銀行"},
  {"code": "0017", "name": "埼玉りそな銀行"},
  {"code": "0397", "name": "新生銀行"},
  {"code": "0033", "name": "ジャパンネット銀行"},
  {"code": "0036", "name": "楽天銀行(旧イーバンク)"},
  {"code": "9900", "name": "ゆうちょ銀行"},
  {"code": "0116", "name": "北海道銀行"},
  {"code": "0501", "name": "北洋銀行"},
  {"code": "0125", "name": "七十七銀行"},
  {"code": "0130", "name": "常陽銀行"},
  {"code": "0134", "name": "千葉銀行"},
  {"code": "0138", "name": "横浜銀行"},
  {"code": "0150", "name": "スルガ銀行"},
  {"code": "0149", "name": "静岡銀行"},
  {"code": "0158", "name": "京都銀行"},
  {"code": "0169", "name": "広島銀行"},
  {"code": "0177", "name": "福岡銀行"},
  {"code": "0190", "name": "西日本シティ銀行"},
]

/**
 * カタカナ48音
 */
export const katakanaList = [
  {"initials": "ア", "katakana": "ア"},
  {"initials": "カ", "katakana": "カ"},
  {"initials": "サ", "katakana": "サ"},
  {"initials": "タ", "katakana": "タ"},
  {"initials": "ナ", "katakana": "ナ"},
  {"initials": "ハ", "katakana": "ハ"},
  {"initials": "マ", "katakana": "マ"},
  {"initials": "ヤ", "katakana": "ヤ"},
  {"initials": "ラ", "katakana": "ラ"},
  {"initials": "ワ", "katakana": "ワ"},
  {"initials": "イ", "katakana": "イ"},
  {"initials": "キ", "katakana": "キ"},
  {"initials": "シ", "katakana": "シ"},
  {"initials": "チ", "katakana": "チ"},
  {"initials": "ニ", "katakana": "ニ"},
  {"initials": "ヒ", "katakana": "ヒ"},
  {"initials": "ミ", "katakana": "ミ"},
  {"initials": "", "katakana": "　"},
  {"initials": "リ", "katakana": "リ"},
  {"initials": "", "katakana": "　"},
  {"initials": "ウ", "katakana": "ウ"},
  {"initials": "ク", "katakana": "ク"},
  {"initials": "ス", "katakana": "ス"},
  {"initials": "ツ", "katakana": "ツ"},
  {"initials": "ヌ", "katakana": "ヌ"},
  {"initials": "フ", "katakana": "フ"},
  {"initials": "ム", "katakana": "ム"},
  {"initials": "ユ", "katakana": "ユ"},
  {"initials": "ル", "katakana": "ル"},
  {"initials": "", "katakana": "　"},
  {"initials": "エ", "katakana": "エ"},
  {"initials": "ケ", "katakana": "ケ"},
  {"initials": "セ", "katakana": "セ"},
  {"initials": "テ", "katakana": "テ"},
  {"initials": "ネ", "katakana": "ネ"},
  {"initials": "ヘ", "katakana": "ヘ"},
  {"initials": "メ", "katakana": "メ"},
  {"initials": "", "katakana": "　"},
  {"initials": "レ", "katakana": "レ"},
  {"initials": "", "katakana": "　"},
  {"initials": "オ", "katakana": "オ"},
  {"initials": "コ", "katakana": "コ"},
  {"initials": "ソ", "katakana": "ソ"},
  {"initials": "ト", "katakana": "ト"},
  {"initials": "ノ", "katakana": "ノ"},
  {"initials": "ホ", "katakana": "ホ"},
  {"initials": "モ", "katakana": "モ"},
  {"initials": "ヨ", "katakana": "ヨ"},
  {"initials": "ロ", "katakana": "ロ"},
  {"initials": "", "katakana": "　"},
]

/**
 * タイムライン
 */
export const TimeLineTabDefine = {
  GET_ALL: 0,
  GET_FAVORITE: 1,
  GET_ID: 2
}
export const TimeLineTab = [
  'all',
  'favorite',
  'id'
]
export const TimeLineContentType = {
  CONTENT: 1,
  IMAGE: 2,
  VIDEO: 3
}

export const Gender = {
  1: '男性',
  2: '女性',
  3: 'その他'
}

export const GenderAvator = {
  1: "/img/thumb_male.png",
  2: "/img/thumb_female.png",
  3: "/img/thumb_noimage.png",
}

export const Styles = [
  "簡潔",
  "素早い",
  "ゆっくり",
  "じっくり",
  "丁寧",
  "優しい",
  "暖かい",
  "癒し",
  "ズバッと",
  "論理的",
  "ユーモア",
  "フレンドリー",
  "ポジティブ",
  "頼りになる",
  "聞き上手",
  "話し上手"
]

export const Methods = [
  "透視",
  "霊感",
  "送念",
  "祈願",
  "祈祷",
  "波動修正",
  "遠隔ヒーリング",
  "オーラ",
  "ルーン",
  "タロット",
  "オラクルカード",
  "ルノルマンカード",
  "パワーストーン",
  "数秘術",
  "東洋占星術",
  "西洋占星術",
  "夢占い",
  "血液型",
  "レイキ",
  "ダウジング",
  "スピリチュアル",
  "チャネリング",
  "チャクラ",
  "カウンセリング",
  "セラピー",
  "守護霊対話",
  "前世観",
  "易",
  "風水",
  "人相",
  "手相",
  "九星気学",
  "姓名判断",
  "四柱推命",
  "紫微斗数",
  "算命学"
]

export const Genres = [
  "相性",
  "結婚",
  "離婚",
  "夫婦仲",
  "復縁",
  "不倫",
  "縁結び",
  "縁切り",
  "遠距離恋愛",
  "同性愛",
  "三角関係",
  "金運",
  "仕事",
  "起業",
  "転職",
  "対人関係",
  "自分の気持ち",
  "相手の気持ち",
  "家庭問題",
  "運勢",
  "開運方法"
]

export const AvatarStatus = {
  PENDDING: 1,
  APPROVED: 2,
  DENY: 3,
  MODIFY: 4
}

export const LIMIT_CHAT_DEFAULT = 5;

export const LIMIT_SEND_PICTURES = 5;

export const MissionLevel = {
  1: '初級',
  2: '中級',
  3: '上級',
  4: 'ゲリラ'
}

export const MissionLevelId = {
  'ELEMENTARY': 1,
  'INTERMEDIATE': 2,
  'ADVANCED': 3,
  'SPECIAL': 4
}

export const MissionUnit = {
  'GET_POINT': 'pts',
  'RECIVE_MESSAGE_CHARATER': '文字',
  'RECIVE_MESSAGE_TIMES': '回',
  'SEND_CHARGE_TEXT_CHARACTER': '文字',
  'SEND_FREE_TEXT_CHARACTER': '文字',
  'SEND_FREE_TEXT_TIMES': '回',
  'CALL_MINUTES': '分',
  'WAITING_BOTH_MINUTES': '分',
  'WAITING_CHAT_MINUTES': '分',
  'WAITING_CALL_MINUTES': '分',
  'TIMELINE_POSTED': '回',
  'CONTINOUS_LOGIN': '日',
  'PAYMENT_TIMES': '回',
  'EDIT_PROFILE': '回',
  'PROFILE_IMAGE_REGISTRATION': '回',
  'REVIEW': '回'
}