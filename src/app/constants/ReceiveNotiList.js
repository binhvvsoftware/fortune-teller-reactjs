export const receiveNotiList = [
    {id:1,label:'お客様からチャット・電話相談リクエストがあった場合は通知する',checkboxId:'recieve_noti_mail',receiveState:'receiveNotiByEmail'},
    {id:2,label:'お客様からレビューがあった場合は通知する',checkboxId:'recieve_noti_review',receiveState:'receiveReviewNotiByEmail'},
    {id:3,label:'お客様からお気に入りがあった場合は通知する',checkboxId:'recieve_noti_favorite',receiveState:'receiveFavoriteNotiByEmail'},
    {id:4,label:'ログイン予定時間の1時間前に通知する',checkboxId:'recieve_noti_before_login',receiveState:'receiveReloginNotiByEmail'},
]

export const receiveNotiListSP = [
    {id:1,label:'チャット・電話相談リクエスト受信時',checkboxId:'recieve_noti_mail',receiveState:'receiveNotiByEmail'},
    {id:2,label:'レビュー受信時',checkboxId:'recieve_noti_review',receiveState:'receiveReviewNotiByEmail'},
    {id:3,label:'お気に入りされた時',checkboxId:'recieve_noti_favorite',receiveState:'receiveFavoriteNotiByEmail'},
    {id:4,label:'ログイン予定時間の1時間前',checkboxId:'recieve_noti_before_login',receiveState:'receiveReloginNotiByEmail'}
]