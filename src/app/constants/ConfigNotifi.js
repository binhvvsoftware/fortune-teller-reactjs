export const notificationType = (notificationType,friendName) => {
  switch (notificationType){
    case 2: {
      return friendName + "さんがオンラインになりました"
    }
    case 3: {
      return "ログインボーナスを獲得しました"
    }
    case 4: {
      return friendName + "さんがタイムラインを投稿しました"
    }
    case 5: {
      return "タイムラインが承認されました"
    }
    case 6: {
      return "タイムラインが否認されました"
    }
    case 7: {
      return "タイムラインコメントが承認されました"
    }
    case 8: {
      return "タイムラインコメントが否認されました"
    }
    case 9: {
      return "コメント返信が承認されました"
    }
    case 10: {
      return "コメント返信が否認されました"
    }
    case 11: {
      return "プロフィール情報が更新されました"
    }
    case 12: {
      return "プロフィール情報が一部更新されました"
    }
    case 13: {
      return "プロフィール情報の更新が否認されました"
    }
    case 14: {
      return friendName + "さんからチャット相談リクエストがありました"
    }
    case 15: {
      return friendName + "さんから電話相談リクエストがありました"
    }
    case 17: {
      return friendName + "さんからタイムラインにコメントがありました"
    }
    case 18: {
      return friendName + "さんからコメントに返信がありました"
    }
    case 19: {
      return friendName + "さんからいいねがつきました"
    }
    case 21: {
      return friendName + "さんからお気に入り登録されました"
    }
    case 23: {
      return 'プロフィール画像の加工が完了しました。'
    }
    case 24: {
      return 'プロフィール画像は承認されました。'
    }
    case 25: {
      return 'プロフィール画像は否認されました。'
    }
    case 27: {
      return "チャットリクエストがキャンセルされました。"
    }
    case 28: {
      return "通話リクエストがキャンセルされました。"
    }
    case 29: {
      return "ログインボーナスを獲得しました"
    }
    case 30: {
      return "ミッションをクリアしました"
    }
    default: {
      return notificationType + friendName;
    }
  }
}