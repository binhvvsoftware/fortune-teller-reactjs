import {
  REVIEW_GET,
} from '../constants/ActionTypes'
import { glasConfig, TimeLineContentType } from '../constants/Config'
import request from 'axios'


const resultSuccess = (type,data) => {
  return {
    type: type,
    data
  }
}

//レビュー取得
export const reviewGet = (params) => {
  return (dispatch) => {
    const token = localStorage.getItem('token')

    return request
      .get(glasConfig.url_base + glasConfig.path_review + params, {
        headers: {
          Authorization: token
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(REVIEW_GET, response.data))
        } else {
          throw 'システムエラー'
        }
      })
      .catch(error => {
        console.log('error on call ' + REVIEW_GET, error)
        throw error
      })
  }
}
