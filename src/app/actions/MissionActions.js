import {
    MISSION_GET,
    MISSION_GET_ARCHIVED,
    MISSION_START,
    MISSION_GET_POINT
} from '../constants/ActionTypes'
import { glasConfig } from '../constants/Config'
import request from 'axios'

const resultSuccess = (type,data) => {
  return {
    type: type,
    data
  }
}

export const getMissions = () => {
  return (dispatch) => {
    return request
      .get(glasConfig.url_base + glasConfig.path_missions, {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MISSION_GET, response.data.data))
        } else {
          dispatch(resultSuccess(MISSION_GET, []))
        }
      })
      .catch(error => {
        console.log('error on call ' + MISSION_GET, error)
        throw error
      })
  }
}

export const getArchivedMissions = () => {
  return (dispatch) => {
    return request
      .get(glasConfig.url_base + glasConfig.path_missions + '/archievedpoint', {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MISSION_GET_ARCHIVED, response.data.data))
        } else {
          dispatch(resultSuccess(MISSION_GET_ARCHIVED, []))
        }
      })
      .catch(error => {
        console.log('error on call ' + MISSION_GET_ARCHIVED, error)
        throw error
      })
  }
}

export const startMission = (id) => {
  return (dispatch) => {
    // dispatch(resultSuccess(MISSION_START, {id, completeUnit: 20}))
    return request
      .put(glasConfig.url_base + glasConfig.path_missions + '/' + id, {}, {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MISSION_START, {id, completeUnit: response.data.data.completeUnit}))
        } else {
          dispatch(resultSuccess(MISSION_START, null))
        }
      })
      .catch(error => {
        console.log('error on call ' + MISSION_START, error)
        throw error
      })
  }
}

export const getPoint = (id) => {
  return (dispatch) => {
    // dispatch(resultSuccess(MISSION_GET_POINT, id))
    return request
      .put(glasConfig.url_base + glasConfig.path_missions + '/' + id + '/archievedmissionpoint', {}, {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        console.log(response)
        if (response.data.code === 0) {
          dispatch(resultSuccess(MISSION_GET_POINT, id))
        } else {
          dispatch(resultSuccess(MISSION_GET_POINT, null))
        }
      })
      .catch(error => {
        console.log('error on call ' + MISSION_GET_POINT, error)
        throw error
      })
  }
}