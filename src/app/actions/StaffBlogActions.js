import { STAFFBLOG_FETCH } from '../constants/ActionTypes'
import { glasConfig } from '../constants/Config'
import request from 'axios'


const resultSuccess = (type,isload,data) => {
  return {
    type: type,
    loaded: isload,
    data
  }
}

export const fetchData = (page,size) => {
  return (dispatch) => {
    return request
      .get(glasConfig.staff_blog_url + "?_embed", {
        params: {
          page: page,
          per_page: size
        },
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        dispatch(resultSuccess(STAFFBLOG_FETCH,true,response.data))
      })
      .catch(error => {
        // console.log('error on call ' + STAFFBLOG_FETCH, error)
        // throw error
      })
  }
}
