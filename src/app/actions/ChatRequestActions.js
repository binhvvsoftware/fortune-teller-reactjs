/**
 * チャットリクエスト関連
 */
import { 
  CHAT_REQUEST,
  CHAT_REQUEST_ANSWER,
  CHAT_REQUEST_MERGE,
  CHAT_REQUEST_DELETE,
} from '../constants/ActionTypes'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import request from 'axios'


const resultSuccess = (type,data) => {
  return {
    type: type,
    data
  }
}

export const fetch = (req) => {
  return (dispatch) => {

    const params = {
      page: req.page,
      size: req.size,
      inConversation: true
    }

    const tellerId = Fetch.tellerId()
    const token = localStorage.getItem('token')
    const url = glasConfig.url_base + glasConfig.path_last_chat

    const options = {
      method: 'GET',
      url: url ,
      params,
      headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
      },
      json: true
    }

    return request(options)
      .then(response => {
        if (response.data.code === 0) {
          let data = {}
          if (response.data.data) {
            for ( var i in response.data.data ) {
              const userId = ( response.data.data[i].toId === tellerId ) ?  response.data.data[i].fromId : response.data.data[i].toId
              data[userId] = response.data.data[i]
            }
          }
          dispatch(resultSuccess(CHAT_REQUEST,data))
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + CHAT_REQUEST, error)
        throw error
      })
  }
}


/**
 * 相談申し込みのキャンセル・許可
 * @param {object} req 
 */
export const post = (req) => {
  return (dispatch) => {

    const params = {
      accept: req.accept
    }

    const token = localStorage.getItem('token')
    const url = glasConfig.url_base + glasConfig.path_chat_request + req.msgId

    const options = {
      method: 'PUT',
      url: url ,
      data: params,
      headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
      },
      json: true
    }

    return request(options)
      .then(response => {
        if (response.data.code === 0) {
          if (!req.accept){
            dispatch(resultSuccess(CHAT_REQUEST_DELETE,req.userId))
          }
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + CHAT_REQUEST_ANSWER, error)
        throw error
      })
  }
}

/**
 * stateに保存されているデータを一部更新する
 * 通知から左カラムの情報を更新したい場合など。
 */
export const merge = (req) => {
  return (dispatch) => dispatch(resultSuccess(CHAT_REQUEST_MERGE,req))
}

/**
 * stateに保存されているデータを一部削除する
 * 通知から左カラムの情報を更新したい場合など。
 */
export const del = (userId) => {
  return (dispatch) => dispatch(resultSuccess(CHAT_REQUEST_DELETE,userId))
}