/**
 * エンドポイントが/ums/v1/fortuneteller/:{tellerid}/templates
 * 
 */
import {
  MYINFO_TEMPLATES,
  MYINFO_TEMPLATES_PUT,
  MYINFO_TEMPLATES_POST,
  MYINFO_TEMPLATES_DELETE,
} from '../constants/ActionTypes'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import request from 'axios'

const resultSuccess = (type,data) => {
  return {
    type: type,
    data
  }
}

//参照
export const fetch = () => {
  return (dispatch) => {

    const token = localStorage.getItem('token')
    const tellerId = Fetch.tellerId()

    return request
      .get(glasConfig.url_base + glasConfig.path_teller_info + tellerId + '/templates', {
        headers: {
          Authorization: token
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MYINFO_TEMPLATES,response.data.data))
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + MYINFO_TEMPLATES, error)
        throw error
      })
  }
}

//更新
export const put = (templateId,templateContent) => {
  return (dispatch) => {

    const token = localStorage.getItem('token')
    const tellerId = Fetch.tellerId()

    return request
      .put(glasConfig.url_base + glasConfig.path_teller_info + tellerId + '/templates', {
        params: {
          templateId: templateId,
          templateContent: templateContent
        },
        headers: {
          Authorization: token
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MYINFO_TEMPLATES_PUT,response.data.data))
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + MYINFO_TEMPLATES_PUT, error)
        throw error
      })
  }
}

//新規作成(post)
export const post = (templateContent) => {
  return (dispatch) => {
    
    const token = localStorage.getItem('token')
    const tellerId = Fetch.tellerId()
    
    return request
      .post(glasConfig.url_base + glasConfig.path_teller_info + tellerId + '/templates', {
        params: {
          templateContent: templateContent
        },
        headers: {
          Authorization: token
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MYINFO_TEMPLATES_POST,response.data.data))
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + MYINFO_TEMPLATES_POST, error)
        throw error
      })
  }
}

//削除
export const del = (templateId) => {
  return (dispatch) => {
    
    const token = localStorage.getItem('token')
    const tellerId = Fetch.tellerId()
    
    return request
      .post(glasConfig.url_base + glasConfig.path_teller_info + tellerId + '/templates', {
        params: {
          templateId: templateId
        },
        headers: {
          Authorization: token
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          dispatch(resultSuccess(MYINFO_TEMPLATES_DELETE,response.data.data))
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + MYINFO_TEMPLATES_DELETE, error)
        throw error
      })
  }
}