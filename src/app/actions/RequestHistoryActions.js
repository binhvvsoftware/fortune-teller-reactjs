/**
 * リクエスト履歴（左カラムの真ん中のタブをクリック）
 */
import { 
  REQUEST_HISTORY,
  REQUEST_HISTORY_MERGE,
} from '../constants/ActionTypes'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import request from 'axios'


const resultSuccess = (type,data) => {
  return {
    type: type,
    data
  }
}

export const fetch = (req) => {
  return (dispatch) => {

    const params = {
      page: req.page,
      size: req.size,
      inConversation: false,
    }

    const tellerId = Fetch.tellerId()
    const token = localStorage.getItem('token')
    const url = glasConfig.url_base + glasConfig.path_last_chat

    const options = {
      method: 'GET',
      url: url ,
      params,
      headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
      },
      json: true
    }

    return request(options)
      .then(response => {
        if (response.data.code === 0) {
          let data = {}
          if (response.data.data) {
            for ( var i in response.data.data ) {
              const userId = ( response.data.data[i].toId === tellerId ) ?  response.data.data[i].fromId : response.data.data[i].toId
              data[userId] = response.data.data[i]
            }
          }
          dispatch(resultSuccess(REQUEST_HISTORY,data))
        } else {
          throw "システムエラー"
        }
      })
      .catch(error => {
        console.log('error on call ' + REQUEST_HISTORY, error)
        throw error
      })
  }
}


/**
 * stateに保存されているデータを一部更新する
 * 通知から左カラムの情報を更新したい場合など。
 */
export const merge = (req) => {
  return (dispatch) => dispatch(resultSuccess(REQUEST_HISTORY_MERGE,req))
}
