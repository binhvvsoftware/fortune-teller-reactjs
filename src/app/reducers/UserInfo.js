import {
  USER_INFO_FETCH,
  USER_INFO_POST,
  USER_INFO_MERGE,
} from '../constants/ActionTypes'

const initialState = {
  loaded: false,
  data : {}
}

export const UserInfo = (state = initialState, action) => {
  switch (action.type) {
    case USER_INFO_FETCH: {
      return Object.assign({}, state, {
        loaded: true,
        data: action.data
      })
    }
    case USER_INFO_POST: {
      return Object.assign({}, state, {
        loaded: true,
        data: action.data
      })
    }
    case USER_INFO_MERGE: {
      if ( state.data.userId === action.data.userId ) {
        Object.assign(state.data, action.data)
      }
      return Object.assign({}, state, {
        loaded: true,
      })
    }
    default: {
      return state
    }
  }
}