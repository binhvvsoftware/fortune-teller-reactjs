import { 
  NEXTLOGIN_PUT,
} from '../constants/ActionTypes'

const initialState = {
  loaded: false,
  data: {}
}

export const NextLogin = (state = initialState, action) => {
  switch (action.type) {
    case NEXTLOGIN_PUT: {
      return Object.assign({}, state, {
        loaded: true,
        data: action.data
      })
    }
    default: {
      return state
    }
  }
}
