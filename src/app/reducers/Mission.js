import {
  MISSION_GET,
  MISSION_GET_ARCHIVED,
  MISSION_START,
  MISSION_GET_POINT
} from '../constants/ActionTypes'

const initialState = {
  loaded: {
    missions: false,
    archivedMissions: false
  },
  missions: [],
  archivedMissions: []
}

export const Mission = (state = initialState, action) => {
  let loaded, newState;
  switch (action.type) {
    case MISSION_GET:
      let missions = Object.assign([], action.data)
      return Object.assign({}, state, {missions})
    case MISSION_GET_ARCHIVED:
      let archivedMissions = Object.assign([], action.data)
      archivedMissions.sort(function(a, b) {
        return b.archievedPointTime - a.archievedPointTime
      });
      loaded = Object.assign({}, state.loaded)
      loaded.archivedMissions = true;
      return Object.assign({}, state, {archivedMissions, loaded})
    case MISSION_START:
      if (action.data !== null) {
        newState = Object.assign({}, state)
        state.missions.forEach((mission, index) => {
          if (mission.id == action.data.id) {
            newState.missions[index].tellerStart = true
            newState.missions[index].completeUnit = action.data.completeUnit
          }
        })

        return Object.assign({}, newState)
      }

      return state
    case MISSION_GET_POINT:
      if (action.data !== null) {
        newState = Object.assign({}, state)
        state.missions.forEach((mission, index) => {
          if (mission.id == action.data) {
            newState.missions.splice(index, 1)
            newState.archivedMissions.unshift(mission)
          }
        })

        return Object.assign({}, newState)
      }

      return state
    default:
      return state;
  }
}