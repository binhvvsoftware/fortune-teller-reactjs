import { 
  NEWS_FETCH,
} from '../constants/ActionTypes'

const initialState = {
  loaded: false,
  data: {}
}

export const News = (state = initialState, action) => {
  switch (action.type) {
    case NEWS_FETCH: {
      return Object.assign({}, state, {
        loaded: action.loaded,
        data: action.data
      })
    }
    default: {
      return state
    }
  }
}