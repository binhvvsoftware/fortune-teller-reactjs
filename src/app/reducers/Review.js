import {
  REVIEW_GET,
} from '../constants/ActionTypes'

const initialState = {
  data: {},
}

export const Review = (state = initialState, action) => {
  switch (action.type) {
    case REVIEW_GET:
      return Object.assign({}, state, action.data)
    default:
      return state
  }
}
