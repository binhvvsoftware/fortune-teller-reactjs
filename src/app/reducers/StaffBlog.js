import { 
  STAFFBLOG_FETCH,
} from '../constants/ActionTypes'

const initialState = {
  loaded: false,
  data: {}
}

export const StaffBlog = (state = initialState, action) => {
  switch (action.type) {
    case STAFFBLOG_FETCH: {
      return Object.assign({}, state, {
        loaded: action.loaded,
        data: action.data
      })
    }
    default: {
      return state
    }
  }
}