/**
 * websocket接続用
 */
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import io from 'socket.io-client'
import moment from 'moment'

export function connect () {

  const socket = io.connect(glasConfig.url_websocket)
  const tellerId = Fetch.tellerId()
  const message = {
    fromId: tellerId,
    msgType: "AUTHEN",
    value: localStorage.getItem('token'),
    time: moment.utc(420).format('YYYYMMDDhhmmss')
  }

  socket.emit('authen', JSON.stringify(message))
  // socket.on('authen', (res) => {
  //   const message = JSON.parse(res)
  //   if (message.msgType !== "AUTHEN_SUCCESS"){
  //     console.log('認証失敗')
  //   }
  // })
  return socket
}
