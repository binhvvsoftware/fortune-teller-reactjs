import * as firebase from 'firebase'
import * as ConfigFirebase from '../constants/ConfigFirebase'

export const Fire = firebase.initializeApp(ConfigFirebase.Config)
export const FireMsg = Fire.messaging()
