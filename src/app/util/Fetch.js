import jwtdecode from 'jwt-decode'

export function tellerId () {
  const token = localStorage.getItem('token')
  if (!token) {
    return ''
  }
  const decoded = jwtdecode(token)
  return decoded.userId
}

export function applicationId () {
  const token = localStorage.getItem('token')
  if (!token) {
    return ''
  }
  const decoded = jwtdecode(token)
  return decoded.usingApplication
}

export function chkExpireToken () {
  const token = localStorage.getItem('token')
  if (!token) {
    return false
  }
  const expiredRemainTime = localStorage.getItem('expiredRemainTime')
  if (!expiredRemainTime) {
    return false
  }
  const date = new Date()
  if ( expiredRemainTime < date.getTime() ) {
    return false
  }
  return true
}

export function fetchUserAgent() {
  const userAgent = navigator.userAgent.toLowerCase();
  if(userAgent.indexOf('iphone') != -1)
    return 1;

  if(userAgent.indexOf('android') != -1)
    return 2;

  return 3;
}

export function getDeviceName() {
  const userAgent = navigator.userAgent.toLowerCase();
  if(userAgent.indexOf('iphone') != -1)
    return 'Mac';

  if(userAgent.indexOf('android') != -1)
    return 'Android';

  return 'Web';
}

/**
 * 身分証審査OKの判定
 * @param {string} status MyAccountやMyProfileで取得できるverifyAge
 * @return {boolean} true:身分証OK
 */
export function isAgeVerify (status) {
  return (status === "1" || status.toLowerCase() === "verified" )
}

export function formatDate (date, format) {
  if (!format) format = 'YYYY-MM-DD hh:mm:ss.SSS';
  format = format.replace(/YYYY/g, date.getFullYear());
  format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
  format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
  format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
  format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
  format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
  if (format.match(/S/g)) {
    var milliSeconds = ('00' + date.getMilliseconds()).slice(-3);
    var length = format.match(/S/g).length;
    for (var i = 0; i < length; i++) format = format.replace(/S/, milliSeconds.substring(i, i + 1));
  }
  return format;
}

/**
 * number_format
 * @param {int} date 
 */
export function formatNumber (date) {
  return String( date ).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' )
}