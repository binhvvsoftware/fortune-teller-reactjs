/**
 * チャット画面 右カラムのユーザー情報表示
 */
import React, {Component} from 'react'
import moment from 'moment'
import ChatUserMemo from './ChatUserMemo'
import ChatUserLogCall from './ChatUserLogCall'
import { glasConfig,Gender } from '../constants/Config';
import $ from 'jquery';

export default class ChatUserInfo extends Component {

  constructor (props) {
    super(props)
    this.state = {
      tab: "memo",
    }
  }

  showTab (tab) {
    this.setState({
      tab: tab,
    })
  }
  
  closeChatInfo = () => {
    $('.chat-box-customer').removeClass('show_chat-box-customer');
    $('.chat-karte-list-wrap').removeClass('hiddenClass');
    $('.chat-memo-history').removeClass('hiddenNotImportant');
    $('.chat-memo-history__nav').removeClass('hiddenClass');  
    $('.chat-memo__fullscreen').removeClass('hiddenClass');
    $('body').removeClass('noScroll');
  }

  switchTab = (event) => {
    if (event.target.className === "tab chat-memo-history__tab partner") {
      $('.partner').addClass('is-active')
      $('.memo').removeClass('is-active')
      $('.chat-karte-list').removeClass('hiddenNotImportant');
      $('.chat-call-history').addClass('hiddenClass');
    }
    if (event.target.className === "tab chat-memo-history__tab memo") {
      $('.partner').removeClass('is-active')
      $('.memo').addClass('is-active')
      $('.chat-karte-list').addClass('hiddenNotImportant');
      $('.call-log-sp').addClass('call-log-sp-show');
      $('.chat-call-history').removeClass('hiddenClass');
      $('.chat-call-history').removeClass('hiddenNotImportant');
    }
    event.stopPropagation();
  }
 

  render () {

    const { UserInfo } = this.props

    if (UserInfo.userId === undefined) {
      return null
    }

    //一覧
    const worries = Array.isArray(UserInfo.worries) ? (UserInfo.worries).join('、') : '-----'

    //ユーザーの情報
    const userData = (
        <li className="list__item">
          <section className="chat-karte is_principal">
            <h2 className="chat-karte-title"><i className="material-icons chat-karte-title__icon">person</i><span className="chat-karte-title__text">相談者</span></h2>
            <div className="chat-karte-name">
              <p className="chat-karte-name__main">{UserInfo.userName}</p>
              <p className="chat-karte-name__sex">({(UserInfo.gender) ? Gender[UserInfo.gender] : '----'})</p>
            </div>
            <dl className="chat-karte-chart">
              <dt className="chat-karte-chart__title">ふりがな</dt>
              <dd className="chat-karte-chart__data">{UserInfo.nickName}</dd>
            </dl>
            <dl className="chat-karte-chart">
              <dt className="chat-karte-chart__title">生年月日</dt>
              <dd className="chat-karte-chart__data">{moment(UserInfo.birthday).format('YYYY-MM-DD')}</dd>
            </dl>
            <dl className="chat-karte-chart">
              <dt className="chat-karte-chart__title">星座</dt>
              <dd className="chat-karte-chart__data">{getZodiac(UserInfo.birthday)}</dd>
            </dl>
            <dl className="chat-karte-chart">
              <dt className="chat-karte-chart__title">出生地</dt>
              <dd className="chat-karte-chart__data">{UserInfo.placeOfBirth === "UNKNOWN" ? '---' : placeFilter(UserInfo.placeOfBirth)}</dd>
            </dl>
            <dl className="chat-karte-chart">
              <dt className="chat-karte-chart__title">出生時間</dt>
              <dd className="chat-karte-chart__data">{moment(UserInfo.timeOfBirth,"HHmm").isValid() ? moment(UserInfo.timeOfBirth,"HHmm").format('HH:mm') :'---'}</dd>
            </dl>
            <dl className="chat-karte-chart">
              <dt className="chat-karte-chart__title">悩み</dt>
              <dd className="chat-karte-chart__data">{worries}</dd>
            </dl>
          </section>
        </li>
      )

    return(
      <div className = "wrap-chat-box-customer">
        <div className="chat-box-customer" onClick={this.closeChatInfo} >
          <div className="chat-karte-list-wrap" id="js_chat_karte" onClick={ this.switchTab } >
            <ul className="list column_2 chat-memo-history__list_sp">
                  <li className="list__item" style={{zIndex:99}} ><a className='tab chat-memo-history__tab partner is-active' href="javascript:;">相談者情報</a></li>
                  <li className="list__item" style={{zIndex:99}} ><a className='tab chat-memo-history__tab memo' >通話履歴</a></li>
            </ul>
            <ul className="chat-karte-list list column_1">
            {userData}
            <Partner name="相手1" partner={UserInfo.partner1}/>
            <Partner name="相手2" partner={UserInfo.partner2}/>
            </ul>
            <div className="call-log-sp" >
              <ChatUserLogCall show={true} UserInfo={UserInfo}/>
            </div>
          </div>
          <div className="chat-memo-history" id="js_chat_memo_history">
            <nav className="tab-bar chat-memo-history__nav">
              <ul className="list column_2 chat-memo-history__list">
                <li className="list__item"><a className={this.state.tab === 'memo' ? 'tab chat-memo-history__tab is-active' : 'tab chat-memo-history__tab'} onClick={()=>this.showTab('memo')} href="javascript:;">メモ</a></li>
                <li className="list__item"><a className={this.state.tab === 'calllog' ? 'tab chat-memo-history__tab is-active' : 'tab chat-memo-history__tab'} onClick={()=>this.showTab('calllog')} href="javascript:;">通話履歴</a></li>
              </ul>
            </nav>
            <ChatUserMemo show={this.state.tab === "memo"} UserInfo={UserInfo}/>
            <ChatUserLogCall show={this.state.tab === "calllog"} UserInfo={UserInfo}/>
          </div>
          <div className="btn-close-tab" >
                <i className="material-icons tab__icons">clear</i>
            </div>
        </div>
      </div>
    )
  }
}




/**
 * ユーザーのパートナー情報
 * @param {*} props ユーザーのパートナー情報
 */
const Partner = (props) => {

  let partnerData = {
    name: "---",
    nickName: "---",
    gender: "---",
    birthday: "---",
    placeOfBirth: "---",
    timeOfBirth: "---",
    relation: "---",
  }

  if ( props.partner.name ) partnerData.name = props.partner.name
  if ( props.partner.nickName ) partnerData.nickName = props.partner.nickName
  if ( props.partner.gender ) partnerData.gender = Gender[props.partner.gender]
  if ( props.partner.birthday ) partnerData.birthday = moment(props.partner.birthday).format('YYYY-MM-DD')
  if ( props.partner.placeOfBirth && props.partner.placeOfBirth !== "UNKNOWN" ) partnerData.placeOfBirth = placeFilter(props.partner.placeOfBirth)
  if ( moment(props.partner.timeOfBirth,"HHmm").isValid() ) partnerData.timeOfBirth = moment(props.partner.timeOfBirth,"HHmm").format('HH:mm')
  if ( props.partner.relation ) partnerData.relation = props.partner.relation

  return (
    <li className="list__item">
      <section className="chat-karte is_partner">
        <h2 className="chat-karte-title"><i className="material-icons chat-karte-title__icon">person_outline</i><span className="chat-karte-title__text">{props.name}</span></h2>
        <div className="chat-karte-name">
          <p className="chat-karte-name__main">{partnerData.name}</p>
          <p className="chat-karte-name__sex">({partnerData.gender})</p>
        </div>
        <dl className="chat-karte-chart">
          <dt className="chat-karte-chart__title">ふりがな</dt>
          <dd className="chat-karte-chart__data">{partnerData.nickName}</dd>
        </dl>
        <dl className="chat-karte-chart">
          <dt className="chat-karte-chart__title">生年月日</dt>
          <dd className="chat-karte-chart__data">{partnerData.birthday}</dd>
        </dl>
        <dl className="chat-karte-chart">
          <dt className="chat-karte-chart__title">星座</dt>
          <dd className="chat-karte-chart__data">{getZodiac(partnerData.birthday)}</dd>
        </dl>
        <dl className="chat-karte-chart">
          <dt className="chat-karte-chart__title">出生地</dt>
          <dd className="chat-karte-chart__data">{partnerData.placeOfBirth}</dd>
        </dl>
        <dl className="chat-karte-chart">
          <dt className="chat-karte-chart__title">出生時間</dt>
          <dd className="chat-karte-chart__data">{partnerData.timeOfBirth}</dd>
        </dl>
        <dl className="chat-karte-chart">
          <dt className="chat-karte-chart__title">関係</dt>
          <dd className="chat-karte-chart__data">{partnerData.relation}</dd>
        </dl>
      </section>
    </li>
  )
}

const placeFilter = (place) => {
    let exceptPlaces = ['東京', '大阪', '京都', '北海道']
    return exceptPlaces.indexOf(place) === -1 ? `${place}県` : place
}

const getZodiac = (birthday) => {
  const zodiacs = [
    {
      name: 'おひつじ座',
      start: {
        month: 3,
        day: 21
      },
      end: {
        month: 4,
        day: 19
      }
    },
    {
      name: 'おうし座',
      start: {
        month: 4,
        day: 20
      },
      end: {
        month: 5,
        day: 20
      }
    },
    {
      name: 'ふたご座',
      start: {
        month: 5,
        day: 21
      },
      end: {
        month: 6,
        day: 21
      }
    },
    {
      name: 'かに座',
      start: {
        month: 6,
        day: 22
      },
      end: {
        month: 7,
        day: 22
      }
    },
    {
      name: 'しし座',
      start: {
        month: 7,
        day: 23
      },
      end: {
        month: 8,
        day: 22
      }
    },
    {
      name: 'おとめ座',
      start: {
        month: 8,
        day: 23
      },
      end: {
        month: 9,
        day: 22
      }
    },
    {
      name: 'てんびん座',
      start: {
        month: 9,
        day: 23
      },
      end: {
        month: 10,
        day: 23
      }
    },
    {
      name: 'さそり座',
      start: {
        month: 10,
        day: 24
      },
      end: {
        month: 11,
        day: 22
      }
    },
    {
      name: 'いて座',
      start: {
        month: 11,
        day: 23
      },
      end: {
        month: 12,
        day: 21
      }
    },
    {
      name: 'やぎ座',
      start: {
        month: 12,
        day: 22
      },
      end: {
        month: 1,
        day: 19
      }
    },
    {
      name: 'みずがめ座',
      start: {
        month: 1,
        day: 20
      },
      end: {
        month: 2,
        day: 18
      }
    },
    {
      name: 'うお座',
      start: {
        month: 2,
        day: 19
      },
      end: {
        month: 3,
        day: 20
      }
    }
  ];

  const date = moment(birthday)
  const month = date.month() + 1
  const day = date.date()
  let zodiacName = ''

  for (let x in zodiacs) {
    let zodiac = zodiacs[x]

    if ((month == zodiac.start.month && day >= zodiac.start.day) || (month == zodiac.end.month && day <= zodiac.end.day)) {
      zodiacName = zodiac.name
      break
    }
  }

  return zodiacName
}