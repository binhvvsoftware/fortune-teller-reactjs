/**
 * 通話・チャットリクエスト（左カラムの対応中(inConversation:true)、相談依頼申請一覧(inConversation:false)）
 */
import React, {Component} from 'react'
import { requestTypes } from '../constants/Config'
import * as ConfigMessage from '../constants/ConfigMessage'
import * as Fetch from '../util/Fetch'
import { Link } from 'react-router-dom'
import moment from 'moment';
import * as ChatRequestActions from '../actions/ChatRequestActions';
import { LIMIT_CHAT } from '../constants/Config';

require('moment/locale/ja')

export default class LeftRequestList extends Component {

  constructor (props) {
    super(props)
    moment.locale('ja')
    const orgtime = moment(this.props.data.serverTime,"YYYYMMDDHHmmss").add(9,'hours').format('YYYY-MM-DD HH:mm:ss')
    this.state = {
      org: orgtime,
      view: moment(orgtime).fromNow()
    }
  }

  countUp () {
    this.setState({
      view : moment(this.state.org).fromNow()
    })
  }

  componentDidMount () {
    this.interval = setInterval(()=>this.countUp(), 1000);
  }

  componentWillUnmount () {
    clearInterval(this.interval);
  }

  disableWhenClick (e) {
    const { data, chatUserId, numbers, maxNumberChating, openWarningFullChatDialog, dispatch, no, closeNavLeft } = this.props;
    const tellerId = Fetch.tellerId();
    const userId = ( data.fromId === tellerId ) ? data.toId : data.fromId;

    if(chatUserId === userId){
      e.preventDefault()
    }

    const numberUserInListChat = document.querySelectorAll(".accpet-user .btn-customer.customer_choose").length + 
      document.querySelectorAll(".accpet-user .btn-customer.customer_default").length;
    if(numbers > maxNumberChating && numberUserInListChat >= maxNumberChating){
      openWarningFullChatDialog(userId);
      return;
    }

    this.props.history.push(`/home/chat/${userId}`);
    closeNavLeft()
  }

  render () {

    const { data, chatUserId } = this.props

    if (!data.fromId) {
      return null
    }

    const tellerId = Fetch.tellerId()
    const userName = data.fromName ? data.fromName : data.friendName
    const userId = ( data.fromId === tellerId ) ? data.toId : data.fromId
    const unreadMessageBadge = data.unreadMessageBadge ? data.unreadMessageBadge : ''
    const msgMap = ConfigMessage.getMessageData(data,"displayTextLeftHistory")
    const message = msgMap.msg

    let chooseClass =  (chatUserId === userId) ? "btn-customer customer_choose" : "btn-customer customer_default"
    let viewtime = this.state.view

    switch (data.msgType) {
      case 'ACCEPT_CALL':
      case 'ACCEPT_CHAT': {
        const pastSeconds = moment().diff( moment(this.state.org), 'seconds')
        viewtime = ( pastSeconds <= 90 ) ? pastSeconds + '秒' : this.state.view
        chooseClass = (chatUserId === userId) ? "btn-customer customer_chat-choose" : "btn-customer customer_chat"
        break
      }
    }

    return (
      <a href='javascript:void(0)' key={'request_' + data.msgId + data.serverTime} onClick={(e) => this.disableWhenClick(e)} className="list__item">
        <div className={chooseClass}>
          <p className="btn-customer__item is_name">{userName}</p>
          <p className="btn-customer__item is_time">{viewtime}</p>
          <p className="btn-customer__item is_comment">{data.msgType === "SENT_FILE" ? "添付画像あり" : message}</p>
        </div>
      </a>
    )
  }
}
