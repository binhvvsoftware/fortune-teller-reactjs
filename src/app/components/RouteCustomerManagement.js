import React, { Component } from 'react'
import { PrivateRoute } from './PrivateRoute'
import CustomerManagement from './CustomerManagement'
import CustomerManagementFollower from './CustomerManagementFollower'

class RouteCustomerManagement extends Component {

  componentWillMount() {
      this.props.activeItem(3)
  }

  render(){
    return (
      <span>
        <PrivateRoute exact strict path={`${this.props.match.path}`} component={CustomerManagement} />
        <PrivateRoute exact strict path={`${this.props.match.path}/follower`} component={CustomerManagementFollower} />
      </span>
    )
  }
}

export default RouteCustomerManagement
