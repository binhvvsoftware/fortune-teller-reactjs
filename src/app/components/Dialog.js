/**
 * 通知ダイアログ
 *
 */
import React, {Component} from 'react'
import DialogChat from './DialogChat'
import DialogCall from './DialogCall'
import DialogAlert from './DialogAlert'
import DialogAgeAuth from './DialogAgeAuth'
import DialogRequirePhoneNumber from './DialogRequirePhoneNumber'

export default class DialogNotifi extends Component {

  constructor (props) {
    super(props)
    this.state = {
      kind: '',
      show: false,
      data: {},
      modal: false
    }
  }


  handleOpen (kind,data,modal=false) {
    this.setState({
      kind: kind,
      show: true,
      data: data,
      modal: modal,
    })
  }

  handleClose () {
    this.setState({
      kind: '',
      show: false,
      data: {},
      modal: false
    })
  }

  /**
   * modal: Dialogのmodalに渡すboolan型の値。trueだと背景クリックで非表示にできないようになります。（現在：DialogChatのみ有効）
   */
  render () {

    const kind = this.state.kind
    const data = this.state.data
    const modal = this.state.modal

    switch (kind) {
      case 'CHAT': {
        return <DialogChat show={this.state.show} data={data} closeFunc={()=>this.handleClose()} />
      }
      case 'CALL': {
        return <DialogCall show={this.state.show} data={data} closeFunc={()=>this.handleClose()} />
      }
      case 'ALERT': {
        return <DialogAlert show={this.state.show} data={data} closeFunc={()=>this.handleClose()} />
      }
      case 'REQUIRE_PHONE_NUMBER': {
        return <DialogRequirePhoneNumber show={this.state.show} data={data} closeFunc={()=>this.handleClose()} />
      }
      case 'AGEAUTH': {
        return <DialogAgeAuth show={this.state.show} closeFunc={()=>this.handleClose()} />
      }
      default : {
        return ''
      }
    }
  }

}
