/**
 * 通話・チャットリクエスト（左カラムの対応中(inConversation:true)、相談依頼申請一覧(inConversation:false)）
 */
import React, {Component} from 'react'
import { Gender } from '../constants/Config'
import { Link } from 'react-router-dom'

export default class LeftFollowerList extends Component {

  render () {

    const { data, chatUserId } = this.props

    if (!data.userId) {
      return null
    }

    const userId = data.userId

    return (
      <Link key={'history_' + data.userId} className="list__item" to={`/home/chat/${userId}`} >
        <div className={(chatUserId === userId) ? "btn-customer customer_request-choose" : "btn-customer customer_default"}>
          <p className="btn-customer__item is_name">{data.userName}</p>
          <p className="btn-customer__item is_time">{Gender[data.gender]}</p>
        </div>
      </Link>
    )
  }
}
