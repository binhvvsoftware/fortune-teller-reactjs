/**
 * チャット申し込みダイアログ
 *
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import * as ChatRequestActions from '../actions/ChatRequestActions'
import { withRouter } from 'react-router'
import moment from 'moment'
import Dialog from 'material-ui/Dialog'
import { fortuneTellerStatus } from '../constants/Config'
import * as MyProfileActions from '../actions/MyProfileActions'

class DialogChat extends Component {

  constructor (props) {
    super(props)
    /**
     * show: ダイアログ true:表示,false:非表示
     * time: カウントダウン表示秒数
     */
    this.state = {
      show: false,
      time: 0
    }
  }


  componentWillMount () {
    const { data, show } = this.props
    this.setState({
      show: show,
      time: this.getCountDownStartTime(data)
    })
  }

  componentDidMount () {
    this.interval = setInterval(()=>this.countDown(), 1000);
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  /**
   * 多重で送信されてくる場合、最新を反映させる
   * @param {*} nextProps 
   */
  componentWillReceiveProps (nextProps) {
    this.setState({
      show: nextProps.show,
      time: this.getCountDownStartTime(nextProps.data)
    })
  }

  countDown () {
    const time = this.state.time - 1
    this.setState({
      time : (time < 0) ? 0 : time
    })
  }

  /**
   * チャットリクエストからカウントダウン開始秒数を取得
   * @param {object} data チャットリクエストのリクエスト内容
   * @return {number} カウントダウン開始秒数
   */
  getCountDownStartTime (data) {
    if (!data) {
      return 0
    }
    moment.locale('ja')
    if (!moment(data.serverTime,"YYYYMMDDHHmmss").isValid()) {
      //申請時間が不正の場合
      return 0
    }
    const registTime = moment(data.serverTime,"YYYYMMDDHHmmss").add(9,'hours')
    const diff = moment().diff(registTime,'seconds')
    if ( diff >= glasConfig.chatrequest_limit ) {
      //90秒以上経っていればダイアログでチャット開始ボタンは非表示
      return 0
    }
    return glasConfig.chatrequest_limit - diff
  }

  checkFullChat = () => {
    const numberUserInListChat = document.querySelectorAll(".accpet-user .btn-customer.customer_choose").length + 
    document.querySelectorAll(".accpet-user .btn-customer.customer_default").length;
    if(numberUserInListChat + 1 == this.props.MyProfile.data.maxNumberUserChatting){
      this.doChangeStatus(fortuneTellerStatus.chattingFull);
    }
  }

  doChangeStatus(status) {
    const { dispatch, MyProfile } = this.props
    const data = MyProfile.data
    data['fortuneTellerStatus'] = status
    dispatch(MyProfileActions.put('status', data))
  }

  render () {
    const { data, modal, closeFunc, dispatch } = this.props

    if ( !this.state.show ) {
      return null
    }

    const tellerId = Fetch.tellerId()
    const userId = (data.toId === tellerId) ? data.fromId : data.toId
    let userName = data.userName ? data.userName : data.fromName
    if ( userName === undefined ) {
      userName = data.friendName
    }

    //ゼロ秒以下の場合時間切れのため、チャット開始できないようにする
    const canStart = ( this.state.time <= 0 ) ? false : true


    const handleChatStart = () => {
      closeFunc()
      dispatch(ChatRequestActions.post({
        userId: userId,
        msgId: data.msgId,
        accept: true
      }))
      this.checkFullChat();
      this.props.history.push(`/home/chat/${userId}`)
    }

    const handleClose = () => {
      closeFunc()
    }

    const handleCancel = () => {
      dispatch(ChatRequestActions.post({
        userId: userId,
        msgId: data.msgId,
        accept: false
      }))
      closeFunc()
    }

    // const actions = [
    //   <FlatButton
    //     label="チャットを開始"
    //     primary={true}
    //     disabled={!canStart}
    //     onClick={()=>handleChatStart()}
    //   />,
    //   <FlatButton
    //     label="申し込みキャンセル"
    //     primary={true}
    //     keyboardFocused={true}
    //     onClick={()=>handleCancel()}
    //   />,
    // ]

  

    const secStyle = {
      textAlign: "center",
      color: "red",
      padding: "8px 0 0",
      fontSize: "1.2em"
    }

    const styleCancel = {
      color: "#c6c6c6",
    }

    const captionStyle = {
      color: "#c6c6c6",
      fontSize: "0.7em"
    }


    return (
      <div>
        <Dialog 
          title="チャット相談申し込み" 
          modal={true} 
          open={this.state.show} 
          bodyClassName="dialog__content"
          contentClassName="dialog-content"
          onRequestClose={()=>handleClose()} 
        >
          <h3>{userName}さん</h3>
          <div>チャット相談の申し込みが届いています</div>
          { this.state.time ? 
            <div style={secStyle}>{this.state.time}秒</div>
            : ''
          }
          <div className="logout-dialog-btn-wrap">
            <div className="btn-wrap margin_bottom btn-dialog">
              <button onClick={()=>handleChatStart()} className="btn-raised color_default">チャットを開始</button>
            </div>
            <div className="btn-wrap margin_bottom btn-bottom-dialog"><a href="javascript:;" onClick={()=>handleCancel()} style={styleCancel}>キャンセル</a></div>
            <p style={captionStyle}>※原則、キャンセルはしないようにしてください</p>
          </div>
        </Dialog>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
  }
}

export default withRouter(connect(
  mapStateToProps
)(DialogChat))