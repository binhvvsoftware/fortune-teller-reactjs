/**
 * 通話・チャットリクエスト（左カラムの対応中(inConversation:true)、相談依頼申請一覧(inConversation:false)）
 */
import React, {Component} from 'react'
import { GenderAvator } from '../constants/Config'
import * as ConfigMessage from '../constants/ConfigMessage'
import moment from 'moment'
import exif from 'exif-js'
import $ from 'jquery'
import Snackbar from 'material-ui/Snackbar'
require('moment/locale/ja')

export default class ChatContentMessageDetails extends Component {

  constructor(props) {
    super(props);

    this.imageLoaded = false;
    this.modal = null;
    this.state = {
        errorClickPicture: false
    }
  }

  componentDidMount() {
      const {data, getOnBottom} = this.props

      let messageList = $('.chat-list-area');
      let actualList = messageList.find('ul');
      let t = this;
      let heightWindow = $( window ).height()
      if (typeof this.imageRef !== 'undefined') {
          this.imageRef.addEventListener('load', function () {
              exif.getData(this, function () {
                  let orientation = exif.getTag(this, "Orientation");
                  let imageClone = this.parentElement.cloneNode(true);

                  if (typeof orientation !== 'undefined' && orientation == 6) {
                      let width = this.offsetWidth;
                      let height = this.offsetHeight;
                      this.style.transform = 'rotate(90deg)';
                      this.style.transformOrigin = '0 100%';
                      this.style.position = 'relative';
                      this.style.top = `-${height}px`;
                      this.parentElement.style.height = `${width}px`;
                      imageClone.getElementsByTagName("img")[0].style.width = '60%'
                      imageClone.getElementsByTagName("img")[0].style.transform = 'rotate(90deg)'
                      imageClone.getElementsByTagName("img")[0].style.marginTop = '80px'
                  } else {
                    imageClone.getElementsByTagName("img")[0].style.width = ''
                    imageClone.getElementsByTagName("img")[0].style.maxHeight = `${heightWindow - 50}px`
                    imageClone.getElementsByTagName("img")[0].style.marginBottom = '0'
                  }

                  if (parseInt(getOnBottom())) {
                      messageList.prop('scrollTop', actualList.height() + 16 - messageList.height());
                  }
                  t.modal = $(this).closest('.chat-list').find('.modal');
                  t.modal.find('.modal-content').append(imageClone);
                  t.imageLoaded = true;
              });
          });
      }

      if (typeof this.loadingIcon !== 'undefined') {
          this.loadingIcon.addEventListener('load', function() {
              if (parseInt(getOnBottom())) {
                  messageList.prop('scrollTop', actualList.height() + 16 - messageList.height());
              }
          });
      }
      $(document).keydown(function(event) { 
        if (event.keyCode == 27) {
          if (t.modal) {
            t.modal.hide()
          }   
        }
      });
    
  }

  handleClickPicture(e){
    if (this.imageLoaded) {
      this.modal.show();
    } else {
      this.setState({
        errorClickPicture: true
      })
    }
  }
  closeModalPicture(e){
    this.modal.hide();
  }
  handleErrorFileClose(e){
    this.setState({
        errorClickPicture: false
      })
  }

  render() {
      let {data} = this.props
      moment.locale('ja')
      const msgTime = (data.message.serverTime) ? moment(data.message.serverTime,"YYYYMMDDHHmmss").add(9,'hours').format('HH:mm') : moment.unix(data.message.senddate).format('YYYY-MM-DD HH:mm:ss')
      const msgId = data.message.msgId
      const fromId = data.message.fromId
      const msgType = data.message.msgType
      const msgMap = ConfigMessage.getMessageData(data.message)
      const msg = msgMap.msg
      const name = ( data.MyId === fromId ) ? data.MyName : data.UserName
      const avatartPath = ( data.MyId === fromId ) ? data.MyAvatartPath : GenderAvator[data.UserGender]

      let messageFile = ''
       if (msgType === "SENT_FILE" || msgType === "SEND_FILE" || msgType === "SEND_FILE_FREE" || msgType === "SENT_FILE_FREE"){
          messageFile = (<div className="message-image">
          <img src={msg} ref={ image => this.imageRef = image } style = {{width : 300 ,  display: "block"}} onClick = {(e)=> this.handleClickPicture(e)}/>
        </div>)
        } else {
        messageFile = msg && msg.match("\n")? msg.split("\n").map( (m,i) => {
            return (<p key={`${msgId}_${i}`}>{m}</p>)
        }) : msg
       }

      return (
          <div key={msgId}>
            <div className="chat-list">
              <figure className="chat-list-figure">
                <img className="chat-list-figure__image" src="/img/spacer.gif" style={{backgroundImage:`url(${avatartPath})`}} alt="プロフィール画像" />
              </figure>
              <div className="chat-list-status">
                <p className="chat-list-status__name">{name}</p>
                <p className="chat-list-status__time">{msgTime}</p>
                  { (data.message.readTime && data.MyId === fromId) ? <p className="chat-list-status__read">既読</p> : '' }
              </div>
                { msgMap.label ? <div className="chat-list-label">
                  <p className="chat-list-label__text">{msgMap.label}</p>
                </div> : ''}
              <div className="chat-list-message">
                <div>
                    {msgMap.icon ? <i className="material-icons">{msgMap.icon}</i> : '' }
                    {messageFile}
                </div>
              </div>
              <div class="modal" onFocus = {e => this.closeModalPicture(e)}  >
                <span class="closePicture" onClick = {e => this.closeModalPicture(e)}>&times;</span>
                <div class="modal-content"></div>
              </div>
            </div>
            <Snackbar
                  open={this.state.errorClickPicture}
                  message={'読み込み中です。少々お待ちください。'}
                  autoHideDuration={2000}
                  onRequestClose={e=>this.handleErrorFileClose(e)}
                />
          </div>
      )
  }

}
