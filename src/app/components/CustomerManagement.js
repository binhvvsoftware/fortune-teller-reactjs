import React, {Component} from 'react'
import { connect } from 'react-redux';
import CustomerManagementMenu from './CustomerManagementMenu'
import * as Fetch from '../util/Fetch'
import * as CustomerManagementActions from '../actions/CustomerManagementActions'
import * as moment from 'moment'
import { glasConfig } from '../constants/Config'

class CustomerManagement extends Component {
  constructor (props) {
    super(props)
    this.state = {
      pageRecords: [],
      totalRecords: 0,
      nowPage: 0,
      dataloaded: false
    }
  }

  componentWillMount() {
    const { dispatch } = this.props
    dispatch(CustomerManagementActions.supportHistoryGet(this.createUrlQuery()))
  }

  componentWillReceiveProps(nextProps){
    let dataloaded = true
    this.setState({
      pageRecords: nextProps.CustomerManagement.data.pageRecords,
      totalRecords: nextProps.CustomerManagement.data.totalRecords,
      nowPage: nextProps.CustomerManagement.page,
      dataloaded
    })
  }

  createUrlQuery(page){
    const tellerId = Fetch.tellerId()
    let postPage = page || 0
    let params = [
      'tab=all',
      `fortuneTellerId=${tellerId}`,
      `page=${postPage}`,
    ]
    return `?${params.join('&')}`
  }

  handlePage(event, page){
    const { dispatch } = this.props
    event.preventDefault()
    dispatch(CustomerManagementActions.supportHistoryGet(this.createUrlQuery(page), page))
  }

  getSupportHistory(){
    if(this.state.dataloaded === true && (this.state.totalRecords == 0 || this.state.totalRecords == undefined)){
      return (
        <tr>
          <td className="table-align-center" colSpan="6">対応履歴がありません</td>
        </tr>
      )
    }else if(this.state.dataloaded === false){
      return (
        <tr>
          <td className="table-align-center" colSpan="6">対応履歴を読み込んでいます...</td>
        </tr>
      )
    }

    return Array.prototype.map.call(this.state.pageRecords, (current, index)=>{
      return (
        <tr key={current.userId}>
          <td className="table-align-center">{current.userName}</td>
          <td className="table-align-center">{current.numberChat}</td>
          <td className="table-align-center">{current.numberCall}</td>
          <td className="table-align-center">{(current.lastCallTime)?moment(current.lastCallTime, 'YYYYMDHm').add(9,'hours').format('YYYY/MM/DD H:mm'):'-'}</td>
          <td className="table-align-center">{(current.lastLoginTime)?moment(current.lastLoginTime, 'YYYYMDHm').add(9,'hours').format('YYYY/MM/DD H:mm'):'-'}</td>
          <td className="table-align-center">{(current.isPurchasedUser||false)?'有':'無'}</td>
        </tr>
      )
    })
  }

  getSupportHistorySP(){
    if(this.state.dataloaded === true && (this.state.totalRecords == 0 || this.state.totalRecords == undefined)){
      return (
        <tr>
          <td className="table-align-center" colSpan="6">対応履歴がありません</td>
        </tr>
      )
    }else if(this.state.dataloaded === false){
      return (
        <tr>
          <td className="table-align-center" colSpan="6">対応履歴を読み込んでいます...</td>
        </tr>
      )
    }

    return Array.prototype.map.call(this.state.pageRecords, (current, index)=>{
      return (
        <div className="warpper-item" key={current.userId} >
          <div className="top-item" >
            <span className="userName" >{current.userName}</span>
            <span className="pay" >購入履歴：{(current.isPurchasedUser||false)?'有':'なし'}</span>
          </div>
          <div className="content-item">
            <ul className="list-info" >
              <li className="info-user" >チャット相談 : {current.numberChat}</li>
              <li className="info-user" >電話相談 : {current.numberCall}</li>
              <li className="info-user" >最終対応 : {(current.lastCallTime)?moment(current.lastCallTime, 'YYYYMDHm').add(9,'hours').format('YYYY/MM/DD HH:mm'):'-'}</li>
              <li className="info-user" >最終ログイン : {(current.lastLoginTime)?moment(current.lastLoginTime, 'YYYYMDHm').add(9,'hours').format('YYYY/MM/DD HH:mm'):'-'}</li>
            </ul>
          </div>
        </div>
      )
    })
  }

  setPager(){
    if(this.state.totalRecords === 0 || this.state.totalRecords === undefined){
      return false
    }

    let nowPage = parseInt(this.state.nowPage, 10)
    let totalRecords = parseInt(this.state.totalRecords, 10)
    let item = glasConfig.usermanagement.management
    let endPage = (totalRecords > item)?parseInt(totalRecords / item, 10):0
    endPage = (endPage !== 0 && totalRecords % item !== 0)?endPage:endPage - 1

    let showPage = (endPage<5)?endPage:5
    let showHalfPage = Math.floor(showPage / 2)

    let loopStart = nowPage - showHalfPage
    let loopEnd = nowPage + showHalfPage

    if(loopStart <= -1){
      loopStart = 0
      loopEnd = showPage - 1
    }
    if(loopEnd > endPage){
      loopStart = endPage - showPage
      loopEnd = endPage
    }

    let firstPage = true
    let prevPage = true
    let lastPage = true
    let nextPage = true

    if(nowPage !== 0){
      firstPage = false
    }
    if(nowPage !== 0 && endPage !== 0){
      prevPage = false
    }
    if(nowPage >= 0 && endPage !== nowPage){
      lastPage = false
    }
    if(nowPage >= 0 && endPage !== nowPage){
      nextPage = false
    }

    let prevNav = []
    if(endPage > 0){
      prevNav.push(
        <li className="list__item">
          <button className="btn" disabled={firstPage} onClick={e=>this.handlePage(e, 0)}><i className="material-icons">first_page</i></button>
        </li>
      )
      let prev = (nowPage>0)?nowPage - 1:0
      prevNav.push(
        <li className="list__item">
          <button className="btn" disabled={prevPage} onClick={e=>this.handlePage(e, prev)}><i className="material-icons">navigate_before</i></button>
        </li>
      )
    }

    let nextNav = []
    if(endPage > 0){
      let next = (nowPage!==endPage)?nowPage + 1:endPage
      nextNav.push(
        <li className="list__item">
          <button className="btn" disabled={nextPage} onClick={e=>this.handlePage(e, next)}><i className="material-icons">navigate_next</i></button>
        </li>
      )
      nextNav.push(
        <li className="list__item">
          <button className="btn" disabled={lastPage} onClick={e=>this.handlePage(e, endPage)}> <i className="material-icons">last_page</i></button>
        </li>
      )
    }

    let pageNav = []
    for(let i = loopStart; i<=loopEnd; i++){
      let pageClass = ['btn']
      if(i === nowPage){
        pageClass.push('is-active')
      }

      pageNav.push(
        <li className="list__item">
          <button className={pageClass.join(' ')} onClick={e=>this.handlePage(e, i)}>{i + 1}</button>
        </li>
      )
    }

    return (
      <div className="primary__inner">
        <div className="pager">
          <ul className="list">
            {prevNav}
            {pageNav}
            {nextNav}
          </ul>
        </div>
      </div>
    )
  }

  render () {
    return (
    <div className="content">
      <h1 className="content__title hidden-sp">顧客管理</h1>
      <CustomerManagementMenu />
      <div className="primary">
        <div className="primary-title hidden-sp">
          <h1 className="primary-title__main">対応履歴</h1>
        </div>
        <div className="frame-table hidden-sp">
          <table className="table-data">
            <thead>
              <tr>
                <th className="table-align-center">名前</th>
                <th className="table-align-center">チャット相談回数</th>
                <th className="table-align-center">電話相談回数</th>
                <th className="table-align-center">最終対応日時</th>
                <th className="table-align-center">最終ログイン日時</th>
                <th className="table-align-center">購入履歴</th>
              </tr>
            </thead>
            <tbody>
              {this.getSupportHistory()}
            </tbody>
          </table>
        </div>
        <div className="customer-list-sp display-mobile">{this.getSupportHistorySP()}</div>
        {this.setPager()}
      </div>
    </div>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    CustomerManagement: store.CustomerManagement
  }
}

export default connect(
  mapStateToProps
)(CustomerManagement)
