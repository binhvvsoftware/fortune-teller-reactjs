import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as MyAccountActions from '../actions/MyAccountActions'
import * as Fetch from '../util/Fetch'
import { glasConfig } from '../constants/Config'
import * as FileActions from '../actions/FileActions'
import {denyStatuses} from '../constants/VerifyStatus'
import SettingMenu from './SettingMenu'
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar'
import Loading from './Loading'
import Checkbox from 'material-ui/Checkbox'
import {receiveNotiList,receiveNotiListSP} from'../constants/ReceiveNotiList';
import '../../App.css'
import RequireSubmitChanges from '../components/RequireSubmitChanges'

class SettingAccount extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      phoneNumber: '',
      listCheckbox: { receiveNotiByEmail: false, receiveReviewNotiByEmail: false, receiveFavoriteNotiByEmail: false, receiveReloginNotiByEmail: false },
      error: false,
      message: '',
      loading: false,
      checkSubmit:true,
      show: false,
      targetPathName:'',
      cancel:false,
    }
  }

  componentWillMount() {
    const { dispatch, Account } = this.props
    dispatch(MyAccountActions.fetch())
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.loading && !nextProps.MyAccount.loading || this.state.loading && nextProps.MyAccount.loading) {
      this.setState({ loading: false })
    }
    if (!this.props.MyAccount.loadedGetAccount && nextProps.MyAccount.loadedGetAccount) {
      this.setState({
        email: nextProps.MyAccount.data.email,
        phoneNumber: nextProps.MyAccount.data.phoneNumber,
        listCheckbox: {
          ...this.state.listCheckbox,
          receiveNotiByEmail: nextProps.MyAccount.data.receiveNotiByEmail,
          receiveReviewNotiByEmail: nextProps.MyAccount.data.receiveReviewNotiByEmail,
          receiveFavoriteNotiByEmail: nextProps.MyAccount.data.receiveFavoriteNotiByEmail,
          receiveReloginNotiByEmail: nextProps.MyAccount.data.receiveReloginNotiByEmail,
        }
      })
    }
  }

  componentDidMount(){
    this.props.history.block((location,action) => {
      const currentPathName = window.location.pathname;
      if(!this.state.checkSubmit && !this.state.cancel){
        this.setState({show:true,targetPathName:location.pathname})
        return false;
      }
    })
  }

  onClickCancel = () => {
    this.setState({show:false})
  }

  onClickOk = () => {
    this.setState({checkSubmit : true}, () => {
      this.props.history.push(this.state.targetPathName);
    });
  }

  handleSubmit(e) {
    // 更新
    if(e.target.name === 'settingAccount'){
      this.setState({checkSubmit:true})
      const { dispatch, MyAccount } = this.props
      
      e.preventDefault()

      let email = document.settingAccount.email.value
      let phoneNumber = document.settingAccount.phoneNumber.value
      let listCheckbox = this.state.listCheckbox
      this.setState({ loading: true })
      if (glasConfig.setting.phone !== phoneNumber.length) {
        this.setState({
          error: true,
          message: `電話番号は${glasConfig.setting.phone}桁で入力してください`,
          loading: false
        })
        return
      }

      let data = {
        email: email,
        phoneNumber: phoneNumber,
        listCheckbox: listCheckbox
      }

      dispatch(MyAccountActions.put(email, phoneNumber, listCheckbox))
    }
  }

  handleChange(e) {
    let email = ''
    let phoneNumber = ''

    if (e.target.id === 'mail_address') {
      email = e.target.value
      phoneNumber = document.settingAccount.phoneNumber.value
    }

    if (e.target.id === 'telephone_number') {
      email = document.settingAccount.email.value
      phoneNumber = e.target.value
    }

    this.setState({
      email,
      phoneNumber,
      checkSubmit: false
    })
  }

  handleClick(event, isInputChecked, receiveState){
   this.setState({checkSubmit:false, listCheckbox: { ...this.state.listCheckbox, [receiveState]: isInputChecked }})
  }

  handleKeyUp(e) {
    let email = ''
    let phoneNumber = ''

    if (e.target.id === 'mail_address') {
      email = e.target.value
      phoneNumber = document.settingAccount.phoneNumber.value
    }
    if (e.target.id === 'telephone_number') {
      email = document.settingAccount.email.value
      phoneNumber = e.target.value
    }

    this.setState({
      email,
      phoneNumber,
    })
  }

  checkAgeVerify() {
    const { MyAccount } = this.props

    if (MyAccount.data.verifyAge === null || parseInt(MyAccount.data.verifyAge, 10) === -2) {
      return ''
    }
    let text = ''
    denyStatuses.forEach((status) => {
      if(MyAccount.data.verifyAge === status.name){
        text = status.text
      }
    });

    let verifyState = ''
    if (MyAccount.data.verifyAge === 'Reviewing'
     ||MyAccount.data.verifyAge === 'WaitingBoss' 
     || MyAccount.data.verifyAge === 'WaitingEmployee' 
     || MyAccount.data.verifyAge === 'Reviewed' 
     || parseInt(MyAccount.data.verifyAge, 10) === 0) {
      verifyState = (<strong>審査中</strong>)
    } else if (MyAccount.data.verifyAge === 'Verified' || parseInt(MyAccount.data.verifyAge, 10) === 1) {
      verifyState = (<strong>認証済み。お仕事を開始できます。</strong>)
    } else if(text != ''){
      verifyState = (<strong>審査NG</strong>)
    }

    return (
      <div>
      <dl className="definition title_colon">
        <dt className="definition__title">年齢認証状況</dt>
        <dd className="definition__data">
          <div className="text-accent">{verifyState}</div>
        </dd>
      </dl>
      <p><span dangerouslySetInnerHTML={{ __html:text}} /></p>
      </div>
    )
  }

  handleErrorClose() {
    this.setState({
      error: false,
      message: ''
    })
  }

  render() {

    const { MyAccount,MyProfile } = this.props

    const listCheckbox = receiveNotiList.map((checkbox, index) => {
      return <div className="list-check-box" key={index}>
        <Checkbox
          style={{ marginBottom: 10 }}
          iconStyle={{ margin: 0 }}
          label={checkbox.label}
          labelStyle={{ paddingTop: 3, fontFamily: "Times New Roman", fontSize: 13 }}
          id={checkbox.id}
          checked={this.state.listCheckbox[checkbox.receiveState]}
          onCheck={(e, i, receiveState) => this.handleClick(e, i, checkbox.receiveState)} />
      </div>
    })

    const listCheckboxSP = receiveNotiListSP.map((checkbox,index) => {
      return <div className="list-check-box-sp" key={index}>
        <Checkbox 
            style={{marginBottom:10}}
            iconStyle={{margin:0}}
            label={checkbox.label}
            labelStyle	={{ paddingTop:3,fontFamily:"Times New Roman",fontSize:12 }}
            id={checkbox.id} 
            checked={this.state.listCheckbox[checkbox.receiveState]} 
            onCheck={(e,i,receiveState) => this.handleClick(e,i,checkbox.receiveState)}/>
      </div>
  })

    return (
      <div className="content">
        <h1 className="content__title">設定</h1>
        <SettingMenu /> 
        <div className="primary">
          <div className="primary-title">
            <h1 className="primary-title__main">アカウント</h1>
            <p className="primary-title__subtitle title-setting-account">お客様側には公開されません。</p>
          </div>
          <div className="primary__inner">
            <form name="settingAccount" onSubmit={e => this.handleSubmit(e)}>
              <dl className="definition">
                <dt className="definition__title write_down">本名</dt>
                <dt className="definition__title write_down_sp">本名</dt>
                <dd className="definition__data margin_bottom">
                  <p>{MyAccount.data.realName? MyAccount.data.realName : MyProfile.data.realName}</p>
                </dd>
                <dt className="definition__title write_down">メールアドレス</dt>
                <dt className="definition__title write_down_sp">メールアドレス</dt>
                <dd className="definition__data">
                  <TextField
                    className="textfield__input"
                    type="text"
                    id="mail_address"
                    name="email"
                    value={this.state.email}
                    hintText="メールアドレス"
                    onKeyUp={e => this.handleKeyUp(e)}
                    onChange={e => this.handleChange(e)}
                  />
                  <span style = {{color:"red",marginLeft:"8px",fontSize:17}}> {MyAccount.data.type === "MYINFO_ACCOUNT_PUT_FALSE"? "* メールアドレスは既に存在しています、または、正しい形式ではありません。": ''}</span>
                </dd>
                {listCheckbox}
                <dt className="definition__title write_down">電話番号 「-（ハイフン）なしで入力してください」</dt>
                <dt className="definition__title write_down_sp">電話番号「-（ハイフン）なしで入力」</dt>
                <dd className="definition__data margin_bottom">
                  <TextField
                    className="textfield__input"
                    maxlength="11"
                    type="text"
                    id="telephone_number"
                    name="phoneNumber"
                    value={this.state.phoneNumber}
                    hintText="電話番号"
                    onKeyUp={e => this.handleKeyUp(e)}
                    onChange={e => this.handleChange(e)}
                  />
                </dd>
                <dt className="title-list-check-sp" >通知</dt>
                {listCheckboxSP}
              </dl>
              <div className="btn-wrap">
                <input type="submit" className="btn-raised color_default" value="登録" />
              </div>
            </form>
          </div>
        </div>
        <div className="primary">
          <div className="primary-title">
            <h1 className="primary-title__main">年齢認証</h1>
          </div>
          <div className="primary__inner">
            <p className="text-disabled margin_bottom">ご本人様確認と18歳以上であることの確認のため身分証の提出が必要です。 <br />事務局での確認が済みますと、お仕事を開始できます。</p>
            {
              MyAccount.data.verifyAge !== 'Verified' &&
              <div className="btn-wrap margin_bottom">
                <Link to='/home/setting/ageauth' className="btn-raised color_accent">年齢認証を行う</Link>
              </div>
            }
            {this.checkAgeVerify()}
          </div>
        </div>
        <div className="primary">
          <div className="primary__inner">
            <Link className="link-setting-pass" to='/home/setting/password'>パスワード変更はこちら</Link>
          </div>
        </div>
        <Snackbar
          open={this.state.error}
          message={this.state.message}
          autoHideDuration={1000}
          onRequestClose={e => this.handleErrorClose(e)}
        />
        <RequireSubmitChanges {...this.props} onClickCancel={() => this.onClickCancel()} onClickOk={() => this.onClickOk()} show={this.state.show} targetPathName={this.state.targetPathName} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyAccount: state.MyAccount,
    MyProfile: state.MyProfile,
  }
}

export default connect(
  mapStateToProps
)(SettingAccount);
