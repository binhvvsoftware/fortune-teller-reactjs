/**
 * ヘッダーのナビゲーション
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import NextLogin from './NextLogin'
import Dialog from 'material-ui/Dialog'
import DialogNotifi from './Dialog'
import HeaderNotifi from './HeaderNotifi'
import * as Fetch from '../util/Fetch'
import { fortuneTellerStatus } from '../constants/Config'
import * as RequestHistoryActions from '../actions/RequestHistoryActions'
import * as RequestOfflineActions from '../actions/RequestOfflineActions'
import * as ChatRequestActions from '../actions/ChatRequestActions'
import * as MyProfileActions from '../actions/MyProfileActions'
import * as MyNotificationActions from '../actions/MyNotificationActions'
import { playNotificationSould } from '../util/Sound';
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import { Nav, Col, Navbar } from 'react-bootstrap';
import LoadingBar from './LoadingBar';
import { glasConfig } from '../constants/Config'

class Header extends Component {

  constructor (props) {
    super(props)
    /**
     * showNextLogin: 次回ログイン日時の表示判定
     * showNotifiLog：通知ログの表示判定
     */
    this.state = {
      userId: this.props.userId,
      showNextLogin: false,
      showNotifiLog: false
    }
  }

  componentDidMount () {
    let path = window.location.pathname
    this.receiveNotification()
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      userId: nextProps.userId
    })
  }
  
  /**
   * 通知を受信する
   */
  receiveNotification () {
    const { dispatch, socket } = this.props
    const tellerId = Fetch.tellerId()

    //占い師のポイントが送られて来る
    socket.on('point',(res) => {

      const message = JSON.parse(res)

      if (message.msgType === "POINT") {
        if ( tellerId === message.toId && message.fromId === "SERVER") {
          const newPoint = Number(message.curPoint)
          dispatch(MyProfileActions.merge({
            point: newPoint
          }))
        }
      }
    })

    socket.on('userRequest',(res) => {
      const message = JSON.parse(res)
      console.log('userRequest ============> ', message)
      if (message.msgType === "CANCEL_REQUEST_CHAT" || message.msgType === "REQUEST_LIMIT_TIME") {
        //リクエストキャンセル
        const deluserId = ( tellerId === message.toId ) ? message.fromId : message.toId
        dispatch(ChatRequestActions.del(deluserId))
        //ダイアログ非表示
        if (Object.keys(this.refs).length) {
          this.refs.dialog.handleClose()
        }
      } else if (message.msgType === "ACCEPT_CHAT"){
        // 通知種別：チャット相談申し込み
        // 処理１：オフラインリクエストから削除
        // 処理２：対応中一覧に追加
        dispatch(RequestOfflineActions.del(message.fromId))
        dispatch(ChatRequestActions.merge({
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: tellerId,
            fromName: message.userName,
            msgType: message.msgType,
            serverTime: message.serverTime,
          }
        }))
        playNotificationSould();
        if (!this.state.userId) {
          if (Object.keys(this.refs).length) {
            this.refs.dialog.handleOpen('CHAT',message)
          }
        }
      } else if (message.msgType === "CLOSE_CHAT" || message.msgType === "NO_ACTION_LONG_TIME" || message.msgType === "NO_CONNECT_AVAILABLE") {
        // 通知種別：チャット(相談)終了
        // 処理１：対応中一覧から削除
        // 処理２：履歴に追加
        dispatch(ChatRequestActions.del(message.fromId))
        dispatch(RequestHistoryActions.fetch({page: 0,size: glasConfig.leftmenu_maxlength_history + 1}))
      } else if (message.msgType === "USER_REQUEST_CALL" ) {
        playNotificationSould();
        // 通知種別：通話リクエスト
        // 処理１：オフラインリクエスト一覧を更新
        dispatch(RequestOfflineActions.merge({
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: message.toId,
            fromName: message.userName,
            type: 2,
            msgType: message.msgType,
            serverTime: message.serverTime
          }
        }))
      } else if (message.msgType === "USER_REQUEST_CHAT") {
        // 通知種別：チャットリクエスト
        // 処理１：オフラインリクエスト一覧を更新
        playNotificationSould();
        dispatch(RequestOfflineActions.merge({
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: message.toId,
            fromName: message.userName,
            type: 1,
            msgType: message.msgType,
            serverTime: message.serverTime
          }
        }))
      } else if (message.msgType === "CLOSE_CALL") {
      // 通知種別：通話終了
      // 処理１： ダイアログ非表示
      // 処理２： 自分の情報を再取得
      // 処理３： 対応中一覧から削除
        console.log('CLOSE_CALL')
        dispatch(ChatRequestActions.del(message.fromId))
        this.chgCallStatus('CLOSE')
        if (Object.keys(this.refs).length) {
          this.refs.dialog.handleOpen('CALL',{
            endType: 'CLOSE'
          })
        }
      } else if (message.msgType === "RESPONSE_REQUEST_CHAT") {
        // ACCEPT_CHAT に対して チャット相談申し込みOKを占い師が選択した場合にすぐにこの通知がくる
        if (message.value === "OK") {
          dispatch(ChatRequestActions.merge({
            userId: message.toId,
            data: {
              msgId: message.msgId,
              fromId: message.fromId,
              toId: message.toId,
              msgType: message.msgType,
              serverTime: message.serverTime,
              unreadMessageBadge: 0
            }
          }))
        } else if (message.value === "NOT_OK") {
          //拒否した場合
          dispatch(ChatRequestActions.del(message.toId))
        } 
      } else if (message.msgType === "RECONNECT_CHAT") {
        dispatch(RequestOfflineActions.del(message.fromId))
        dispatch(ChatRequestActions.merge({
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: tellerId,
            fromName: message.userName,
            msgType: message.msgType,
            serverTime: message.serverTime,
          }
        }))
        dispatch(ChatRequestActions.post({
          userId: message.fromId,
          msgId: message.msgId,
          accept: true
        }))
        this.props.history.push(`/home/chat/${message.fromId}`)
        
      }
      else if (message.msgType === "EXPECTED_REQUEST_CHAT") {
        playNotificationSould();
        dispatch(RequestOfflineActions.merge({
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: message.toId,
            type: 1,
            msgType: message.msgType,
            serverTime: message.serverTime,
            userName: message.userName
          }
        }))
      } else if (message.msgType === "EXPECTED_REQUEST_CALL") {
        playNotificationSould();
        dispatch(RequestOfflineActions.merge({
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: message.toId,
            type: 2,
            msgType: message.msgType,
            serverTime: message.serverTime,
            userName: message.userName
          }
        }))
      }
    })



    socket.on('response', (res) => {
      const message = JSON.parse(res)
      console.log('response ========>',message)
      if (message.msgType === "CALL") {
        // 通知種別：通話に関する通知　endType:(着信中:INCALL,キャンセル:CANCEL,通話中:BUSY)
        // 処理１：着信中の場合はダイアログ表示、それ以外はダイアログ非表示
        // 処理２：自分のステータスを更新
        if (Object.keys(this.refs).length) {
          this.refs.dialog.handleOpen('CALL',message)
        }
        this.chgCallStatus(message.endType)
      } else if (message.msgType === "CHARGE_TEXT" || message.msgType === "FREE_TEXT" || message.msgType === "SEND_FILE") {
       // 通知種別：有料・無料メッセージの受信
       // 処理１：対応中一覧を更新
        let params = {
          userId: message.fromId,
          data: {
            msgId: message.msgId,
            fromId: message.fromId,
            toId: tellerId,
            value: message.value,
            msgType: message.msgType,
            serverTime: message.serverTime,
          }
        }
        //受信したユーザーのチャット画面を開いている場合は未読0にする
        if (this.state.userId === message.fromId) {
          params.data.unreadMessageBadge = 0
        }
        dispatch(ChatRequestActions.merge(params))

        if (message.msgType === "SEND_FILE") {
          dispatch(MyProfileActions.add({
              numberUnreadNotification: 1
          }))
        }
      }
    })

  }

  /**
   * 通話通知のendTypeによってMyProfileのstatusを更新
   * @param {string} endType (着信中:INCALL, キャンセル:CANCEL, 通話中:BUSY, 切断: CLOSE)
   */
  chgCallStatus (endType) {
    const { dispatch } = this.props
    switch( endType ){
      case 'BUSY': {
        dispatch(MyProfileActions.merge({
          fortuneTellerStatus: fortuneTellerStatus.calling
        }))
        break
      }
      case 'CLOSE': {
        dispatch(MyProfileActions.fetch())
        break
      }
      default : {
        break
      } 
    }
  }
 

  /**
   * バッジがあるタブをクリックした時のイベントハンドラー
   * @param {} kind 
   */
  handeleBadge (kind) {

    const { dispatch, MyProfile } = this.props
    //badgeを0にする
    if (MyProfile.data[kind] !== 0 ){
      dispatch(MyProfileActions.merge({
        [kind]: 0
      }))
    }

    //通知ログを表示
    if (kind === "numberUnreadNotification") {
      this.setState({
        showNotifiLog: !this.state.showNotifiLog
      })
    }

  }

  render () {
    const { MyProfile, bottomItemActive } = this.props

    const logoStyle = {
      backgroundImage: `url(/img/logo.png)`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "60pt 5pt",
    }

    const homeIcon = <FontIcon className="material-icons" style={bottomItemActive === 1 ? {color:'rgb(17,173,157)'} : {}} >home</FontIcon>;
    const assignmentIcon = <i className="fas fa-file-alt" style={bottomItemActive === 2 ? {color:'rgb(17,173,157)'} : {}} ></i>
    const customerIcon = <FontIcon className="material-icons" style={bottomItemActive === 3 ? {color:'rgb(17,173,157)'} : {}} >person</FontIcon>;
    const pointIcon =<i className="fab fa-product-hunt" style={bottomItemActive === 4 ? {color:'rgb(17,173,157)'} : {}} ></i>
    const menuIcon = <FontIcon className="material-icons" style={bottomItemActive === 5 ? {color:'rgb(17,173,157)'} : {}} >menu</FontIcon>;

    return (
      <div>
        <Navbar fixedBottom fluid className="menu-header" >        
          <Row>
            <Navbar.Header style={stylesNav_header} ref="navBar" >
                    <Col xs={12} lgHidden mdHidden smHidden className="navtab-header">
                        <Link to='/home' className="btn-tab global-header__home" data-badge="0" >
                          <BottomNavigationItem
                            label="ホーム"
                            icon={homeIcon}
                            className={bottomItemActive === 1 ? "active-bottom-item": ''}
                          />
                        </Link>
                        <Link to='/home/timeline' className="btn-tab global-header__timeline" data-badge="0" role="button" >
                          <BottomNavigationItem
                            label="タイムライン"
                            icon={assignmentIcon}
                            className={bottomItemActive === 2 ? "active-bottom-item": ''}
                          />
                        </Link>

                        <Link to='/home/customermanagement' className="btn-tab global-header__management" data-badge="0" role="button" >
                          <BottomNavigationItem
                            label="顧客管理"
                            icon={customerIcon}
                            className={bottomItemActive === 3 ? "active-bottom-item": ''}
                          />
                        </Link>
                        <Link to='/home/reward' className="btn-tab global-header__checkout" data-badge="0" role="button" >
                          <BottomNavigationItem
                            label="精算"
                            icon={pointIcon}
                            className={bottomItemActive === 4 ? "active-bottom-item": ''}
                          />
                        </Link>
                        <Link to='/home/menu' className="btn-tab global-header__menu" data-badge="0" role="button" >
                          <BottomNavigationItem
                            label="メニュー"
                            icon={menuIcon}
                            className={bottomItemActive === 5 ? "active-bottom-item": ''}
                          />
                        </Link>
                    </Col>
                  <Col xsHidden>
                    <Navbar.Brand>
                      <Link className="sitelogo" to='/home'>
                        <img className="sitelogo__image"src="/img/logo.png" />
                      </Link>
                    </Navbar.Brand>
                  </Col>

              </Navbar.Header>
              <Navbar.Collapse className="menu-collapse">
                <Nav>
                  <li role="presentation">
                    <Link to='/home' className="btn-tab global-header__home" data-badge="0">
                    ホーム
                    </Link>
                  </li>
                  <li role="presentation">
                    <Link to='/home/timeline' className="btn-tab global-header__timeline" data-badge="0" role="button">タイムライン</Link>
                  </li>
                  <li role="presentation">
                    <Link to='/home/customermanagement' className="btn-tab global-header__management" data-badge="0" role="button">顧客管理</Link>
                  </li>
                  <li role="presentation">
                    <Link to='/home/reward' className="btn-tab global-header__checkout" data-badge="0" role="button">精算</Link>
                  </li>
                  <li role="presentation">
                    <Link to='/home/review' onClick={()=>this.handeleBadge('numberUnreadReview')} className="btn-tab global-header__review" role="button" data-badge={MyProfile.data.numberUnreadReview}>レビュー</Link>
                  </li>
                  <li role="presentation">
                    <Link to='/home/setting' className="btn-tab global-header__settings" data-badge="0" role="button">設定</Link>
                  </li>
                  <li role="presentation">
                    <span onClick={()=>this.handeleBadge('numberUnreadNotification')} role="button" className="btn-tab global-header__notice" data-badge={MyProfile.data.numberUnreadNotification > 99 ? '99+' : MyProfile.data.numberUnreadNotification}>通知</span>
                  </li>
                  <li role="presentation">
                  <Link to='/home/help' className="btn-tab global-header__help" role="button" data-badge="0">ヘルプ</Link>
                  </li>
                </Nav>
                <Nav pullRight>
                  <li role="presentation">
                    <a href="javascript:;" onClick={()=>this.setState({ showNextLogin: !this.state.showNextLogin })} className="btn-tab global-header__logout" role="button">ログアウト</a>
                  </li>
                </Nav>
              </Navbar.Collapse>
              </Row>
            </Navbar>
        <LogoutDialog show={this.state.showNextLogin} closeFunc={()=>this.setState({showNextLogin:false})} />
        <DialogNotifi ref='dialog' />
        <HeaderNotifi show={this.state.showNotifiLog} closeFunc={()=>this.handeleBadge('numberUnreadNotification')}/>
      </div>
    )
  }
}

const styleText = {
  fontSize: '12px',
  transition: 'color 0.3s, font-size 0.3s',
  color: 'rgba(0, 0, 0, 0.54)',
}

const stylesNav_header = {
  width: '256px',
  height: '100%',
  backgroundColor: '#2a2a3b',
}

const customLogoutDialogStyle = {
  width: '485px',
}

const LogoutDialog = ({show,closeFunc}) => {

  if (!show) {
    return null
  }

  return (
    <Dialog
      open={true} 
      modal={false} 
      title="ログアウト" 
      onRequestClose={()=>closeFunc()}
      bodyClassName="dialog__content"
      contentStyle={customLogoutDialogStyle}
    >
      <p className="logout-dialog__text">次回のログイン予定を登録しておくと相談件数が向上します。</p>
      <NextLogin show={true} place='logout' closeFunc={()=>closeFunc()} />
    </Dialog>
  )
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
  }
}

export default connect(
  mapStateToProps
)(Header);
