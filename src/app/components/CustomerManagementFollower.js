import React, {Component} from 'react'
import { connect } from 'react-redux';
import CustomerManagementMenu from './CustomerManagementMenu'
import * as CustomerManagementActions from '../actions/CustomerManagementActions'
import { glasConfig, Gender } from '../constants/Config'
import * as moment from 'moment'
import * as Fetch from '../util/Fetch'

class CustomerManagementFollower extends Component {
  constructor (props) {
    super(props)
    this.state = {
      postSize: glasConfig.usermanagement.follower,
      followers: [],
      autoload: true,
      postPage: 0,
      dataloaded: false
    }
    this.handleScroll = this.handleScroll.bind(this)

    this.followerList = null
  }

  // パラメータ生成
  getQueryParam(postPage = null){
    const tellerId = Fetch.tellerId()
    let page = (postPage !== null)?postPage:this.state.postPage
    let params = [
      'tab=followers',
      `fortuneTellerId=${tellerId}`,
      `size=${this.state.postSize}`,
      `page=${page}`,
    ]

    return `?${params.join('&')}`
  }

  componentWillMount() {
    const { dispatch } = this.props
    dispatch(CustomerManagementActions.followerGet(this.getQueryParam()))
  }

  componentWillReceiveProps(nextProps){
    let autoload = this.state.autoload
    if(autoload && this.props.CustomerManagement.loading && ! nextProps.CustomerManagement.loading){
      let dataloaded = true
      if(nextProps.CustomerManagement.data.length === 0){
        autoload = false
      }
      let followers = this.state.followers.concat(nextProps.CustomerManagement.data)

      this.setState({
        followers,
        autoload,
        dataloaded
      })
    }
  }

  // フォロワー情報
  setRows(){
    if(this.state.dataloaded === true && this.state.followers.length === 0){
      return (
        <tr>
          <td className="table-align-center" colSpan="5">フォロワーがいません</td>
        </tr>
      )
    }else if(this.state.dataloaded === false){
      return (
        <tr>
          <td className="table-align-center" colSpan="5">フォロワーを読み込んでいます...</td>
        </tr>
      )
    }
    return Array.prototype.map.call(this.state.followers, (current, index)=>{
      let favoriteTime = moment()
      if(current.favoriteTime){
        favoriteTime = moment(current.favoriteTime, 'YYYYMMDDhhmmss')
      }
      return (
        <tr key={current.userId}>
          <td className="table-align-center">{current.userId}</td>
          <td className="table-align-center">{current.userName}</td>
          <td className="table-align-center">{(current.birthday)?moment(current.birthday, 'YYYYMDHm').add(9,'hours').format('YYYY/MM/DD'):'-'}</td>
          <td className="table-align-center">{(current.timeOfBirth)?moment(current.timeOfBirth, 'hhmm').add(9,'hours').format('H:mm'):'-'}</td>
          <td className="table-align-center">{(current.gender)?Gender[current.gender]:'-'}</td>
        </tr>
      )
    })
  }

  setRowsSP(){
    if(this.state.dataloaded === true && this.state.followers.length === 0){
      return (
        <tr>
          <td className="table-align-center" colSpan="5">フォロワーがいません</td>
        </tr>
      )
    }else if(this.state.dataloaded === false){
      return (
        <tr>
          <td className="table-align-center" colSpan="5">フォロワーを読み込んでいます...</td>
        </tr>
      )
    }
    return Array.prototype.map.call(this.state.followers, (current, index)=>{
      let favoriteTime = moment()
      if(current.favoriteTime){
        favoriteTime = moment(current.favoriteTime, 'YYYYMMDDhhmmss')
      }
      return (
        <div className="warpper-item" key={current.userId} >
          <div className="top-item" >
            <span className="userName" >{current.userName}</span>
            <span className="pay" >性別 : {(current.gender)?Gender[current.gender]:'-'}</span>
          </div>
          <div className="content-item">
            <ul className="list-info" >
              <li className="info-user" >ユーザID : {current.userId}</li>
              <li className="info-user" >生年月日 : {(current.birthday)?moment(current.birthday, 'YYYYMDHm').add(9,'hours').format('YYYY/MM/DD'):'-'}</li>
              <li className="info-user" >出生時間 : {(current.timeOfBirth)?moment(current.timeOfBirth, 'hhmm').add(9,'hours').format('H:mm'):'-'}</li>
            </ul>
          </div>
        </div>
      )
    })
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event){
    const { dispatch } = this.props
    let rect = this.followerList.getBoundingClientRect()
    if(this.state.autoload && this.state.followers.length > 10 && rect.height - glasConfig.usermanagement.autoLoadPixel < window.innerHeight + Math.abs(rect.top)){
      let page = parseInt(this.state.postPage, 10) + 1
      this.setState({
        postPage: page
      })
      dispatch(CustomerManagementActions.followerGet(this.getQueryParam(page)))
    }
  }

  render () {
    return (
      <div className="content">
        <h1 className="content__title">顧客管理</h1>
        <CustomerManagementMenu />
        <div className="primary">
          <div className="primary-title hidden-sp">
            <h1 className="primary-title__main">フォロワー</h1>
          </div>
          <div className="frame-table hidden-sp" >
            <table className="table-data" ref={(elem)=>{this.followerList = elem}}>
              <thead>
                <tr>
                  <th className="table-align-center">ユーザID</th>
                  <th className="table-align-center">ユーザ名</th>
                  <th className="table-align-center">生年月日</th>
                  <th className="table-align-center">出生時間</th>
                  <th className="table-align-center">性別</th>
                </tr>
              </thead>
              <tbody>
                {this.setRows()}
              </tbody>
            </table>
          </div>
          <div className="customer-list-sp display-mobile">{this.setRowsSP()}</div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (store, ownProps) => {
  return {
    CustomerManagement: store.CustomerManagement
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch
  }
}
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return Object.assign({}, ownProps, stateProps, dispatchProps)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CustomerManagementFollower)
