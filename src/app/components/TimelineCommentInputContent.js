import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as TimelineActions from '../actions/TimelineActions'
import TextField from 'material-ui/TextField';

class TimelineCommentInputContent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      disabled: 'disabled',
      formView: 'none'
    }
  }

  handleChange(event){
    let disabled = 'disabled'
    if(event.target.value.length !== 0){
      disabled = ''
    }

    this.setState({
      'disabled': disabled
    })
  }

  handleSubmit(event){
    event.preventDefault()

    const { dispatch } = this.props
    let commentValue = event.target.timelineComment.value
    dispatch(TimelineActions.timelineCommentPost({postId: this.props.postId, commentId: this.props.commentId, subcommentValue: commentValue}))
    this.toggleForm()
  }

  handleClick(event){
    event.preventDefault()
    this.toggleForm()
  }

  handleCancel(event){
    event.preventDefault()
    this.toggleForm()
  }

  toggleForm(){
    let display = ''
    if(this.state.formView === 'none'){
      display = 'block'
    }else{
      display = 'none'
    }

    this.setState({
      formView: display
    })
  }

  render(){
    return (
      <form name="timelineCommentForm" onSubmit={e=>this.handleSubmit(e)}>
        <div className="timeline-comment__textfield">
          <button className="btn timeline-comment__btn-more" onClick={e=>this.handleClick(e)}>返信する</button>
          <div style={{display: this.state.formView}}>
            <TextField
              hintText="コメントを記入"
              type="text"
              name="timelineComment"
              onChange={e=>this.handleChange(e)}
              fullWidth={true}
            />
            <div className="timeline-comment__buttons">
              <button className="btn" onClick={e=>this.handleCancel(e)}>キャンセル</button>
              <button className="btn-raised color_default" disabled={this.state.disabled}>投稿</button>
            </div>
          </div>
        </div>
      </form>
    )
  }
}

const mapStateToProps = (store) => {
  return store
}

export default connect(
  mapStateToProps
)(TimelineCommentInputContent);
