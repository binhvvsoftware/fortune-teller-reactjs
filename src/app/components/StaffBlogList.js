import React from 'react'
import { Link } from 'react-router-dom'
import * as moment from 'moment'
import * as Fetch from '../util/Fetch'

const StaffBlogList = ({data,exclude}) => {

  return Object.values(data).map( (res) => {

    if (exclude === res.id ) {
      return null
    }

    const backGroundImage = res['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['full']['source_url']
    const categoryData = res['_embedded']['wp:term'][0]

    return (
      <li key={`staffblog_${res.id}`} className="list__item">
        <div className="card">
          <Link className="card-main" to={`/home/staffblog/detail/${res.id}`}>
            <figure className="card-main__figure">
              <img src="/img/spacer.gif" style={{backgroundImage:`url(${backGroundImage?backGroundImage:"/img/demo/sora_003.jpg"})`}} alt={`staffblog_${res.id}`} />
            </figure>
            <div className="card-main__inner">
              <h3 className="card-main__title">{res.title.rendered}</h3>
              <p className="card-main__time">{moment(res.date).format("YYYY/MM/DD HH:mm")}</p>
            </div>
          </Link>
          <div className="card__inner">
            <ul className="list">
            { categoryData ? (
              categoryData.map(categoryVal=>{
                return <li key={`staffblog_${res.id}_${categoryVal.id}`} className="list__item"><Link className="card__category" to={`/home/staffblog/1/${categoryVal.id}`}>{categoryVal.name}</Link></li>
              })
            ) : '' }
            </ul>
          </div>
        </div>
      </li>
    )
  })

}

export default StaffBlogList