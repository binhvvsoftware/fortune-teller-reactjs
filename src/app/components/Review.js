import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import * as WebSocket from '../util/WebSocket'
import * as ReviewActions from '../actions/ReviewActions'
import * as Fetch from '../util/Fetch'
import { glasConfig } from '../constants/Config'
import * as moment from 'moment'

class Review extends Component {
  constructor(props) {
    super(props)
    this.state = {
      postSize: glasConfig.review.size,
      postPage: 0,
      reviews: [],
      autoload: true,
      dataloaded: false
    }
    this.handleScroll = this.handleScroll.bind(this)

    // レビューリストDOMElement
    this.reviewList = null
  }

  componentWillMount() {
    const { dispatch } = this.props
    dispatch(ReviewActions.reviewGet(this.getQueryParam()))
  }

  componentWillReceiveProps(nextProps){
    let autoload = this.state.autoload
    if(autoload){
      let dataloaded = true
      if(nextProps.Review.data.length === 0){
        autoload = false
      }
      let reviews = []
      if(this.state.postPage === 0){
        reviews = nextProps.Review.data
      }else{
        reviews = this.state.reviews.concat(nextProps.Review.data)
      }
      this.setState({
        reviews,
        autoload,
        dataloaded
      })
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  getQueryParam(postPage = null){
    let tellerId = Fetch.tellerId()
    let page = (postPage !== null)?postPage:this.state.postPage
    let param = [
      'fortuneTellerId=' + tellerId,
      'page=' + page,
      'size=' + this.state.postSize
    ]

    return '?' + param.join('&')
  }

  handleScroll(event){
    const { dispatch } = this.props
    let rect = this.reviewList.getBoundingClientRect()
    if(this.state.autoload && rect.height - glasConfig.review.autoLoadPixel < window.innerHeight + Math.abs(rect.top)){
      let page = this.state.postPage + 1
      this.setState({
        postPage: page
      })
      dispatch(ReviewActions.reviewGet(this.getQueryParam(page)))
    }
  }

  // 公開非公開のステータス無し
  setRow(){
    if(this.state.dataloaded === true && this.state.reviews.length === 0){
      return (
        <li className="list__item">
          <section className="review">
            <p className="review__main">レビューはまだありません</p>
          </section>
        </li>
      )
    }else if(this.state.dataloaded === false){
      return (
        <li className="list__item">
          <section className="review">
            <p className="review__main">レビューを読みこんでいます...</p>
          </section>
        </li>
      )
    }

    return Array.prototype.map.call(this.state.reviews, (current, index) => {
      let date = moment(current.reviewDate, 'YYYYMDHms')
      let star = 'star_' + current.star
      return (
        <li className="list__item" key={current.reviewID}>
          <section className="review">
            <h2 className="review__name">{current.userName}</h2>
            <div className={`review__stars ${star}`}>
              <i className="material-icons review__icons">star</i>
              <i className="material-icons review__icons">star</i>
              <i className="material-icons review__icons">star</i>
              <i className="material-icons review__icons">star</i>
              <i className="material-icons review__icons">star</i>
            </div>
            <p className="review__time">{date.format('YY/MM/DD')}</p>
            <p className="review__main">{current.reviewText}</p>
          </section>
        </li>
      )
    })
  }

  render() {
    return (
      <div className="content">
        <div className="content__inner">
          <h1 className="content__title hidden-sp">レビュー</h1>
          <div className="primary">
            <div className="primary__inner review-content">
              <ul className="list column_1" ref={(elem)=>{this.reviewList = elem}}>
                {this.setRow()}
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    Review: store.Review
  }
}

export default connect(
  mapStateToProps
)(Review)
