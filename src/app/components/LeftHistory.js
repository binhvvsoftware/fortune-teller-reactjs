/**
 * 左メニューの真ん中。お客様リスト（チャット・通話の履歴を表示）
 *
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { glasConfig } from '../constants/Config'
import LeftHistoryList from './LeftHistoryList'

class LeftHistory extends Component {

  constructor (props) {
    super(props)
    this.state = {
      offset: 0,
      limit: 20,
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.show && nextProps.show){
      //非表示状態から表示に変わる場合
      this.setState({
        offset: 0,
      })
    }
  }


  loadData () {
    this.setState({
      offset: this.state.offset + this.state.limit
    })
  }

  render () {

    const { show, data, chatUserId, closeNavLeft } = this.props

    if ( !(show && data.loaded) ){
      return null
    }

    //左カラムに表示する件数よりも多ければ「もっと見る」を押した際、顧客ページへ遷移させる
    const maxnum = Object.keys(data.data).length

    if (!maxnum) {
      return (
        <LeftHistoryArea UserList={
            <p className="secondary-definition__nodata">履歴はありません</p>
          }
        />
      )
    }

    //表示する件数
    const num = this.state.offset + this.state.limit

    //もっと見るボタンの生成
    let moreButton = ''
    if ( num >= glasConfig.leftmenu_maxlength_history ) {
      moreButton = <Link className="btn-raised color_default spread_width" to="/home/setting" >もっと見る</Link>
    } else {
      if ( maxnum > num ) {
        moreButton = <div className="btn-raised color_default spread_width" onClick={()=>this.loadData()}>もっと見る</div>
      }
    }

    const UserList = Object.values(data.data).map( (res, i) => {
      if ( i < num ) {
          return (
            <LeftHistoryList key={i + '_history'} data={res} chatUserId={chatUserId} closeNavLeft={closeNavLeft} />
          )
        }
      })

      return (
        <LeftHistoryArea UserList={
            <div className="list column_1">
              {UserList}
              {moreButton}
            </div>
          }
        />
      )
  }

}

const LeftHistoryArea = ({UserList}) => {
  return (
      <div className="secondary-definition-wrap" style={ style.listChatWrap }>
        <dl className="secondary-definition">
          <dt className="secondary-definition__title"><b>対応履歴</b></dt>
          <dd className="secondary-definition__data">
            {UserList}
          </dd>
        </dl>
      </div>
  )
}

const mapStateToProps = (state) => {
  return {}
}

export default connect(
  mapStateToProps
)(LeftHistory)

const style = {
  listChatWrap : {
    height: window.innerHeight - 332
  }
}
