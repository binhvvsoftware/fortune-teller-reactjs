import React, {Component} from 'react'
import { connect } from 'react-redux'
import { glasConfig, MissionLevel } from '../constants/Config'
import MissionMenu from './MissionMenu'
import * as MissionActions from '../actions/MissionActions'
import * as moment from 'moment'

class MissionArchived extends Component {

  componentWillMount() {
    const { dispatch, Mission } = this.props

    if (!Mission.loaded.archivedMissions) {
      dispatch(MissionActions.getArchivedMissions())
    }
  }

  render () {
    const { Mission } = this.props

    const missions = Mission.archivedMissions.map((mission) => {
      return (<ArchivedItem mission={mission} />)
    })

    return (
      <div className="content setting-mission">
        <h1 className="content__title">ミッション</h1>
        <MissionMenu />
        <div className="primary">
          <ul>{missions}</ul>
        </div>
      </div>
    )
  }
}

class ArchivedItem extends Component {

  render() {
    const { mission } = this.props
    let date = new Date();
    let addHours = Math.abs(date.getTimezoneOffset()) / 60;

    return (
      <li key={mission.id} className={`mission-item mission-archived-item mission-level-${mission.catalog} clearfix`}>
        <div className="mission-archived-item__left">
          <img src="/img/mission_done.png" />
        </div>
        <div className="mission-archived-item__right">
          <div className="mission-item__header clearfix">
            <span className="mission-item__level">{MissionLevel[mission.catalog]}</span>
            <span className="mission-item__title">{mission.title}</span>
          </div>
          <div className="mission-item__footer clearfix">
            <span className="mission-item__archivedTime">達成日：{moment(mission.archievedPointTime).format('YYYY/MM/DD')}</span>
            <span className="mission-item__point">報酬：<span>{mission.grandedPoint}PT</span></span>
          </div>
        </div>
      </li>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    Mission: state.Mission
  }
}

export default connect(
  mapStateToProps
)(MissionArchived);
