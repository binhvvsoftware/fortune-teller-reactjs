/**
 * Helpページ
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import HelpDetail from './HelpDetail'
import request from 'axios'


class Help extends Component {

  constructor (props) {
    super(props)
    const {params} = this.props.match
    this.state = {
      data: {},
      searchBody: '',
      id: params.id ? params.id : ''
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!Object.keys(this.state.data).length) {
      this.loadData()
    }
    const nextId = nextProps.match.params.id
    if ( nextId !== this.state.id ) {
      this.setState({
        id: nextId,
      })
    }
  }

  
  componentWillMount () {
    this.loadData()
  }


  /**
   * FAQ一覧取得
   * @return {void}
   */
  loadData () {

    const url = '/help.json'
    const options = {
      method: 'GET',
      url: url ,
      headers: {
          'Authorization': localStorage.getItem('token')
      },
      json: true
    }

    request(options)
      .then(response => {
          this.setState({
            data: response.data.data,
          })
      })
      .catch(error => {
        //throw error
      })
  }


  /**
   * 複数(区切り文字はスペース)のフリーワードでの検索(titleとbodyについて)
   * @param {Object} HelpData 検索元データ
   * @param {string} word 検索ワード
   * @param {boolean} AndSeatchflg true：AND検索, false：OR検索
   */
  filterMultiWord (HelpData,words,AndSeatchflg) {
    let resultData = HelpData
    if (AndSeatchflg) {
      //AND検索
      words.split(/\s+/).map((val,i)=>{
        resultData = this.filterWord(resultData,val)
      })
      return resultData
    } else {
      //OR検索
      const result = words.split(/\s+/).map((val,i)=>{
        return HelpData.filter((item, index) => {
            if ( (item.title).indexOf(val) >= 0 || (item.body).indexOf(val) >= 0 ) {
              return true
            }
          })
      })
      return result[0]
    }
  }

  /**
   * １つのフリーワードで検索(titleとbodyについて)
   * @param {Object} HelpData 検索元データ
   * @param {string} word 検索ワード
   */
  filterWord (HelpData,word) {
    const result = HelpData.filter((item, index) => {
      if ( (item.title).indexOf(word) >= 0 || (item.body).indexOf(word) >= 0 ) {
        return true
      }
    })
    return result
  }


  /**
   * カテゴリ検索
   * @param {Object} HelpData 検索元データ
   * @param {string} category 検索カテゴリ
   */
  filterCategory (HelpData,category) {
    const result = HelpData.filter((item, index) => {
      if (item.category === category) {
        return true
      }
    })
    return result
  }

  /**
   * ID検索
   * @param {Object} HelpData
   * @param {string} category 
   */
  filterId (HelpData,id) {
    let result = {}
    for ( let i in HelpData) {
      if ( HelpData[i].id === id ) {
        result = HelpData[i]
        break
      }
    }
    return result
  }

  handleChange (e) {
    this.setState({
      searchBody: e.target.value
    })
  }

  handleReset () {
    if (this.state.id) {
      return 
    }
    this.setState({
      searchBody: ''
    })
  }

  render () {

    if (!Object.keys(this.state.data).length) {
      return null
    }

    const searchId = this.state.id

    let resultData = this.state.data

    const { MyProfile } = this.props

    let specialCmCode = MyProfile.data.specialCmCode

    if (searchId) {
      //ID検索
      resultData = this.filterId(resultData,searchId)
    } else {
      if (this.state.searchBody) {
        //フリーワード検索
        resultData = this.filterMultiWord(resultData,this.state.searchBody,true)
      }
    }
    
    return (
      <div className="content">
        <div className="content__inner layout_help">
          <h1 className="content__title">ヘルプ</h1>
          { (this.state.searchBody || searchId ) ? (
            <ol className="breadcrumb hidden-sp">
              <li className="breadcrumb__item"><Link to="/home/help" onClick={()=>this.handleReset()}>ヘルプ</Link></li>
              <li className="breadcrumb__item">
                <p>検索結果</p>
              </li>
            </ol>
          ) : '' }
          { searchId ? 
            <HelpDetail data={resultData}/>
           : (
            <div>
              <HelpSearch changeFunc={(e)=>this.handleChange(e)} searchBody={this.state.searchBody} />
              <HelpList data={resultData} searchBody={this.state.searchBody}/>
            </div>
          ) }
        </div>
          { specialCmCode ? (
            <p style={{marginBottom:20}} >
              ここにない質問については、ご担当者様にお尋ねくださいませ。 
            </p>
            ) : (
            <p style={{marginBottom:20}} >
              ここにない質問については<a href="mailto:support@stella-app.com">お問い合わせください。</a>
            </p>
            ) 
          }
      </div>
    )
  }
}

/**
 * 一覧表示
 * @param {List<Object>} data 
 * @param {searchBody} searchBody 
 */
const HelpList = ({data,searchBody}) => {

  if (!Object.keys(data).length) {
    return (
      <div className="primary help-content">
      { searchBody ? (
        <div className="primary-title">
          <h1 className="primary-title__main">{searchBody}</h1>
          <p className="primary-title__subtitle">キーワードの検索結果</p>
        </div>
      ) : (
        ''
      )}
        <div className="primary__inner">
          <p>見つかりませんでした</p>
        </div>
    </div>
    )
  }

  //カテゴリ毎に表示させるために整形
  let listData = {}
  Object.values(data).map( (res) => {
    if ( !listData[res.category] ) {
      listData[res.category] = []
    }
    listData[res.category] = [...listData[res.category],res]
  })

  return (
    <div>
    {Object.keys(listData).map( (key) => {
      return (
        <div className="primary help-content" key={key}>
          <div className="primary-title">
            <h1 className="primary-title__main">{key}</h1><span style={{marginLeft:8}} >{searchBody ? 'の検索結果' : ''}</span>
            {/* { searchBody ? <p className="primary-title__subtitle">キーワードの検索結果</p> : '' } */}
          </div>
          <div className="primary__inner">
            <ul className="list column_1 has_dotto">
            {Object.values(listData[key]).map( (res) => {
              return <li key={res.id} className="list__item"><Link to={`/home/help/${res.id}`}>{res.title}</Link></li>
            })}
          </ul>
          </div>
        </div>
      )
    })}
    </div>
  )
}

/**
 * 検索フォーム
 * @param {function} changeFunc 
 * @param {string} searchBody 
 */
const HelpSearch = ({changeFunc,searchBody}) => {
  return (
    <div className="help-form-wrap">
      <div className="help-form-main"><i className="material-icons">search</i>
        <div className="textfield">
          <input className="textfield__input" type="text" value={searchBody} onChange={(e)=>changeFunc(e)} id="handle_name" placeholder="検索キーワードを入力" />
        </div>
      </div>
      <div className="help-form-actions">
        <div className="btn-wrap help-form-actions__submit">
          <button className="btn-raised color_default spread_width">検索</button>
        </div>
      </div>
    </div>
  )
}


const mapStateToProps = (state) => {
  return { 
    MyProfile: state.MyProfile 
  }
}

export default connect(
  mapStateToProps
)(Help)