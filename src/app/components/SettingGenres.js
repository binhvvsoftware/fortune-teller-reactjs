import React, {Component} from 'react'
import { glasConfig, Genres } from '../constants/Config'
import Checkbox from 'material-ui/Checkbox';

export default class SettingGenres extends Component {
  handleClick(event, isInputChecked){
    this.props.handleClick(event, isInputChecked, 'genres', '得意ジャンルを選択してください')
  }

  render(){
    let list = []
    let genres = Genres

    for(let i in genres){
      let checked = false
      let disabled = false
      let defaultChecked = false
      if(this.props.checked.indexOf(genres[i]) !== -1){
        checked = true
      }
      if(this.props.disabled.indexOf(genres[i]) !== -1){
        disabled = true
      }
      if(this.props.defaultChecked !== undefined && this.props.defaultChecked.indexOf(genres[i]) !== -1){
        defaultChecked = true
      }else if(this.props.defaultChecked !== undefined && this.props.defaultChecked.length === glasConfig.setting.genres){
        disabled = true
      }
      list.push(
        <li className="list__item" key={genres[i]}>
          <Checkbox
            label={genres[i]}
            onCheck={(e,i)=>this.handleClick(e,i)}
            checked={checked}
            defaultChecked={defaultChecked}
            disabled={disabled}
            id={'genres-'+i}
            name="genres[]"
            value={genres[i]}
          />
        </li>
      )
    }

    return (
      <dl className="definition wide_bottom">
        <dt className="definition__title">得意ジャンル<span className="definition__description">3～9つ選択</span></dt>
        <dd className="definition__data data_genres">
          <ul className="list flex_start">
            {list}
          </ul>
        </dd>
      </dl>
    )
  }
}
