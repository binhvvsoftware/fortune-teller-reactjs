/**
 * 左メニュー
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import LeftProfile from './LeftProfile'
import LeftHistory from './LeftHistory'
import LeftRequest from './LeftRequest'
import LeftFollower from './LeftFollower'
import HeaderNotifi from './HeaderNotifi'
import ChatUserInfo from './ChatUserInfo'
import { glasConfig,fortuneTellerStatusToString, fortuneTellerStatus } from '../constants/Config'
import * as RequestOfflineActions from '../actions/RequestOfflineActions'
import * as ChatRequestActions from '../actions/ChatRequestActions'
import * as FollowersActions from '../actions/FollowersActions'
import * as RequestHistoryActions from '../actions/RequestHistoryActions'
import * as MyProfileActions from '../actions/MyProfileActions'
import AppBar from 'material-ui/AppBar'
import FontIcon from 'material-ui/FontIcon';
import { Col, Row, Navbar } from 'react-bootstrap'
import Badge from 'material-ui/Badge'
import NotificationsIcon from 'material-ui/svg-icons/social/notifications'
import { height } from 'window-size';
import request from 'axios';
import $ from 'jquery';

class LeftMenu extends Component {
  constructor (props) {
    super(props)
    //showにはクリックされたコンポーネント名が入り、そのコンポーネントが表示される
    this.state = {
      show: 'Request',
      showLeftMenu: false,
      showNotifiLog: false,
      showUserInfo:false,
      UserData:'',
      registBank:false
    }
  }

  componentWillMount () {
    this.loadItem(this.state.show)
    const { dispatch } = this.props
    request
      .get(glasConfig.url_outside + glasConfig.path_bank_account, {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          if (!response.data.data.accountHolder || !response.data.data.accountNumber || !response.data.data.bankcode
            || !response.data.data.sitencode) {
            this.setState({
              registBank: false
            })
          }
          else{
            this.setState({
              registBank: true
            })
          }
        }
      })
  }

  /**
   * 左メニュの項目のクリックイベント
   * @param {*} e イベント
   * @param {*} item クリックした項目名
   */
  showItem (e,item) {
    e.stopPropagation()
    this.loadItem(item)
    this.setState({
      show: item
    })
  }
 

  /**
   * 表示する一覧をAPIで取得する
   * @param {string} item 左メニューで表示する一覧（Request:対応中、History:履歴、Follower:フォロワー） 
   */
  loadItem (item) {
    const { dispatch, RequestOffline, ChatRequest } = this.props
    switch (item) {
      case 'Request': {
        if (!ChatRequest.loaded) {
          dispatch(ChatRequestActions.fetch({page: 0,size: 100}))
        }
        if (!RequestOffline.loaded) {
          dispatch(RequestOfflineActions.fetch({page: 0,size: 100}))
        }
        break 
      }
      case 'History': {
        dispatch(RequestHistoryActions.fetch({page: 0,size: glasConfig.leftmenu_maxlength_history + 1}))
        break
      }
      case 'Follower': {
        dispatch(FollowersActions.fetch({page: 0,size: glasConfig.leftmenu_maxlength_follower + 1}))
        break
      }
      default : {
        return 
      }
    }
  }

  handeleBadge (kind) {
    //通知ログを表示
    const { dispatch, MyProfile } = this.props
    //badgeを0にする
    if (MyProfile.data[kind] !== 0 ){
      dispatch(MyProfileActions.merge({
        [kind]: 0
      }))
    }
    if (kind === "numberUnreadNotification") {
      this.setState({
        showNotifiLog: !this.state.showNotifiLog
      })
    }

  }

  showNavLeft = () => {
    this.setState({showLeftMenu: true})
  }

  closeNavLeft = () => {
    this.setState({showLeftMenu: false}) 
  }

  showUserInfo = () => {
    $('.chat-box-customer').addClass('show_chat-box-customer');
    $('.chat-memo-history').addClass('hiddenNotImportant');
    $('.chat-call-history').addClass('hiddenNotImportant');
    $('body').addClass('noScroll');
  }
  showLogMemory = () => {
    $('.chat-box-customer').addClass('show_chat-box-customer');
    $('.chat-karte-list-wrap').addClass('hiddenClass');
    $('.chat-memo-history__nav').addClass('hiddenClass');
    $('.chat-memo__fullscreen').addClass('hiddenClass');
    $('body').addClass('noScroll');
  }

  componentWillReceiveProps(nextProps){
    const {userId} = nextProps;
    const url = glasConfig.url_base + glasConfig.path_user_info + userId
    const options = {
      method: 'GET',
      url: url ,
      headers: {
          'Authorization': localStorage.getItem('token')
      },
      json: true
    }
    if(userId) {
      request(options).then(response => {
        if (response.data.code === 0 && response.data.data.userId) {
          this.setState({
            UserData: response.data.data,
          })
        }
      })
    }
  }

  goBack = () => {
    if (window.location.pathname.includes('bank/list')) {
      if ((this.state.registBank)) {
        window.location.pathname = "/home/setting/bank/top";
      }else{
        window.history.back();
      }
    }else{
      window.history.back();
    }
  }

  render () {
    
    const { MyProfile, ChatRequest, Follower, RequestHistory, userId, userNameOfflineRequest } = this.props

    const status = MyProfile.data.fortuneTellerStatus
    const statusToString = fortuneTellerStatusToString[status]
    const showComponent = this.state.show
    const numberNoti = MyProfile.data.numberUnreadNotification > 99 ? '99+' : MyProfile.data.numberUnreadNotification;
    //未読件数の計算
    let unreadMessageBadge = 0
    if (Object.keys(ChatRequest.data).length) {
      for ( let i in ChatRequest.data ) {
        unreadMessageBadge += ChatRequest.data[i].unreadMessageBadge !== undefined ? ChatRequest.data[i].unreadMessageBadge : 0
      }
      unreadMessageBadge = unreadMessageBadge >= 100 ? "99+" : unreadMessageBadge
    }

    let unreadMessageBadgeHistory = 0
    if (Object.keys(RequestHistory.data).length) {
      for ( let i in RequestHistory.data ) {
        unreadMessageBadgeHistory += RequestHistory.data[i].unreadMessageBadge !== undefined ? RequestHistory.data[i].unreadMessageBadge : 0
      }
      unreadMessageBadgeHistory = unreadMessageBadgeHistory >= 100 ? "99+" : unreadMessageBadgeHistory
    }

    return (
      <div>
        <div className={this.state.showLeftMenu ? 'overlay secondary-show' : 'overlay secondary-hidden'} onClick={this.state.showLeftMenu ? this.closeNavLeft : () => {}}>
          <Navbar fixedTop className="secondary-header" >
            <Badge
              style={{paddingLeft:10,paddingTop:15,float:'left',paddingRight:12}}
              onClick={this.showNavLeft}
              className="left-badge"
              secondary={true}
              badgeContent={ unreadMessageBadge }
              secondary={true}
              badgeStyle={ unreadMessageBadge !== 0 ? {top: 6,left:26} : {display:'none'} }
              ><i className="material-icons">sms</i>
              </Badge>
              <span className="message_text" onClick={this.showNavLeft} >{statusToString}</span>
              <Link className="link-to-home" to='/home' >
                <FontIcon className="material-icons" >home</FontIcon>
              </Link>
              <div className="user_name_hidden">{this.state.UserData.userName}</div>
            <Badge
              className="noti_icon"
              onClick={() => this.handeleBadge('numberUnreadNotification')}
              style={{float:'right',paddingTop:12,paddingRight:18}}
              badgeContent={numberNoti > 99 ? numberNoti + '+' : numberNoti}
              secondary={true}
              badgeStyle={numberNoti !== 0 ? {top: 5,left:23} : {display:'none'}}
            > 
              <NotificationsIcon />
            </Badge>
            <div className="nav_right_chat" >
            <button className="pencil" onClick={this.showLogMemory} >
              <img src="/img/pencil.png" />
            </button>
            <button className="user" onClick={this.showUserInfo} >
              <img src="/img/account.png" />
            </button>
            </div>
        </Navbar>
        <div className="header-back hiddenClass">
          <i className="material-icons icon-back" onClick={this.goBack} >keyboard_arrow_left</i>
          <span className="component-title" ></span>
        </div>
        <HeaderNotifi show={this.state.showNotifiLog} closeFunc={()=>this.handeleBadge('numberUnreadNotification')}/>
        </div>

        <div className={this.state.showLeftMenu ? 'secondary secondary-show' : 'secondary secondary-hidden'}>
          <LeftProfile closeNavLeft={this.closeNavLeft} MyProfile={MyProfile} />
          <div className="secondary-customer">
            <nav className="tab-bar" id="js_customer_nav">
              <ul className="list column_3">
                <li className="list__item">
                  <a data-badge={unreadMessageBadge} onClick={e=>this.showItem(e,'Request')} className={ showComponent==='Request' ? "tab is-active" : "tab"}>
                    <i className="material-icons tab__icons">sms</i>
                  </a>
                </li>
                <li className="list__item">
                  <a data-badge={unreadMessageBadgeHistory} onClick={e=>this.showItem(e,'History')} className={ showComponent==='History' ? "tab is-active" : "tab"}>
                    <i className="material-icons tab__icons">history</i>
                  </a>
                </li>
                <li className="list__item">
                  <a onClick={e=>this.showItem(e,'Follower')} className={ showComponent==='Follower' ? "tab is-active" : "tab"}>
                    <i className="material-icons tab__icons">favorite</i>
                  </a>
                </li>
              </ul>
            </nav>
            <LeftRequest 
              show={ showComponent==='Request'} 
              data={ChatRequest} 
              chatUserId={userId}
              userNameOfflineRequest= {userNameOfflineRequest} 
              history={ this.props.history } 
              openWarningFullChatDialog={ chatUserId => this.props.openWarningFullChatDialog(chatUserId) }
              closeNavLeft={this.closeNavLeft}
            />
            <LeftHistory closeNavLeft={this.closeNavLeft} show={ showComponent==='History'} data={RequestHistory} chatUserId={userId} />
            <LeftFollower show={ showComponent==='Follower'} data={Follower} chatUserId={userId} />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
    ChatRequest: state.ChatRequest,
    RequestOffline: state.RequestOffline,
    RequestHistory: state.RequestHistory,
    Follower: state.Follower,
  }
}

const styles = {
  bottom: '0',
  position: 'fixed'
}
export default connect(
  mapStateToProps,
)(LeftMenu)
