/**
 * 左メニューの真ん中。お客様リスト（チャット・通話の履歴を表示）
 *
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { glasConfig } from '../constants/Config'
import LeftFollowerList from './LeftFollowerList'


class LeftFollower extends Component {

  constructor (props) {
    super(props)
    this.state = {
      offset: 0,
      limit: 20,
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.show && nextProps.show){
      //非表示状態から表示に変わる場合
      this.setState({
        offset: 0,
      })
    }
  }


  loadData () {
    this.setState({
      offset: this.state.offset + this.state.limit
    })
  }

  render () {

    const { show, data, chatUserId } = this.props

    if ( !(show && data.loaded) ){
      return null
    }

    //左カラムに表示する件数よりも多ければ「もっと見る」を押した際、顧客ページへ遷移させる
    const maxnum = Object.keys(data.data).length

    if (!maxnum) {
      return (
        <LeftFollowerArea UserList={
            <p className="secondary-definition__nodata">フォロワーはいません。<br />お客様があなたをお気に入り登録すると追加されます。</p>
          }
        />
      )
    }

    //表示する件数
    const num = this.state.offset + this.state.limit

    //もっと見るボタンの生成
    let moreButton = ''
    if ( num >= glasConfig.leftmenu_maxlength_follower ) {
      moreButton = <Link className="btn-raised color_default spread_width" to="/home/setting" >もっと見る</Link>
    } else {
      if ( maxnum > num ) {
        moreButton = <div className="btn-raised color_default spread_width" onClick={()=>this.loadData()}>もっと見る</div>
      }
    }

    const UserList = Object.values(data.data).map( (res, i) => {
      if ( i < num ) {
          return (
            <LeftFollowerList key={`followerlist_${i}`} data={res} chatUserId={chatUserId}/>
          )
        }
      })

      return (
        <LeftFollowerArea UserList={
            <div className="list column_1">
              {UserList}
              {moreButton}
            </div>
          }
        />
      )
  }
}

const LeftFollowerArea = ({UserList}) => {
  return (
      <div className="secondary-definition-wrap" style={ style.listChatWrap }>
        <dl className="secondary-definition">
          <dt className="secondary-definition__title"><b>フォロワー</b></dt>
          <dd className="secondary-definition__data">
            {UserList}
          </dd>
        </dl>
      </div>
  )
}

const mapStateToProps = (state) => {
  return {}
}

export default connect(
  mapStateToProps
)(LeftFollower)

const style = {
  listChatWrap : {
    height: window.innerHeight - 332
  }
}
