/**
 * チャット画面（/chat/:userid で表示）
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import request from 'axios'
import ChatContent from './ChatContent'
import ChatUserInfo from './ChatUserInfo'

class Chat extends Component {

  constructor (props) {
    super(props)
    this.state = {
      UserData: {},
      numberFeasibleWord: 0,
      sendFilePrice : 0,
      sendTextPrice : 0 
    }
  }

  componentDidMount () {
    const {params} = this.props.match
    Promise.all([this.loadUserData(params.userid),this.loadNumberWord(params.userid)]).then( (results) => {
    }).catch( (e) => {
        console.log(e)
    })
  }
  

  componentWillReceiveProps (nextProps) {
    const { params } = this.props.match
    const nextuid = nextProps.match.params.userid
    if ( nextuid !== params.userid ) {
      Promise.all([this.loadUserData(nextuid),this.loadNumberWord(nextuid)]).then( (results) => {
      }).catch( (e) => {
          console.log(e)
      })
    } else {
      Promise.all([this.loadUserData(params.userid),this.loadNumberWord(params.userid)]).then( (results) => {
      }).catch( (e) => {
          console.log(e)
      })
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (nextProps.match.params.userid === nextState.UserData.userId)
  }

  /**
   * 
   * @param {string} userId 
   * @return {void}
   */
  loadUserData (userId) {
    const url = glasConfig.url_base + glasConfig.path_user_info + userId
    const options = {
      method: 'GET',
      url: url ,
      headers: {
          'Authorization': localStorage.getItem('token')
      },
      json: true
    }

    return new Promise((resolve,reject) => {
      request(options)
      .then(response => {
        if (response.data.code === 0 && response.data.data.userId) {
          this.setState({
            UserData: response.data.data,
          })
          resolve(true)
        }
      })
      .catch(error => {
        //throw error
      })
    })
  }


  loadNumberWord (userId) {
    
    const url = glasConfig.url_base + glasConfig.path_point_feasibleword
    const params = {
      fortuneTellerId: Fetch.tellerId(),
      userId: userId
    }
    const options = {
      method: 'GET',
      url: url ,
      params,
      headers: {
          'Authorization': localStorage.getItem('token')
      },
      json: true
    }

    return new Promise((resolve,reject) => {
      request(options)
      .then(response => {
        if (response.data.code === 0) {
          this.setState({
            numberFeasibleWord: response.data.data.numberFeasibleWord,
            sendTextPrice  : response.data.data.sendTextPrice,
            sendFilePrice  : response.data.data.sendFilePrice
          })
          resolve(true)
        }
      })
      .catch(error => {
        //throw error
      })
    })
  }


  render () {
    const { socket } = this.props
    const pointUser = this.state.UserData.point
    return (
      <div>
        <ChatContent socket={socket} UserInfo={this.state.UserData} numberFeasibleWord={this.state.numberFeasibleWord} sendTextPrice={this.state.sendTextPrice} sendFilePrice={this.state.sendFilePrice} pointUser={pointUser}/>
        <ChatUserInfo UserInfo={this.state.UserData}/>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile : state.MyProfile
  }
}

export default connect(
  mapStateToProps
)(Chat)