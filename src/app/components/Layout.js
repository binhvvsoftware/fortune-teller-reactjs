import React, {Component} from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import NotFound from './NotFound'
import Header from './Header'
import LeftMenu from './LeftMenu'
import Home from './Home'
import Chat from './Chat'
import Timeline from './Timeline'
import RouteReward from './RouteReward'
import RouteSetting from './RouteSetting'
import Menu from './Menu'
import RouteCustomerManagement from './RouteCustomerManagement'
import DialogWarningFullChat from './DialogWarningFullChat'
import * as ChatRequestActions from '../actions/ChatRequestActions'
import Review from './Review'
import Help from './Help'
import RouteStaffBlog from './RouteStaffBlog'
import * as WebSocket from '../util/WebSocket'
import * as MyProfileActions from '../actions/MyProfileActions'
import { FireMsg } from '../util/Firebase'
import LoadingBar from './LoadingBar'
import RouteMission from "./RouteMission";

class Layout extends Component {

  constructor (props) {
    super(props)
    this.socket = WebSocket.connect()
    this.state = {
      chatUserId: '',
      showDialogWarningFullChat : false,
      bottomItemActive: 1,
    }
    this.userNameOfflineRequest = ''
  }

  activeItem = (item) => {
    this.setState({bottomItemActive:item})
  }

  componentWillMount () {
    const { dispatch,MyProfile } = this.props
    if ( !MyProfile.loaded ){
      dispatch(MyProfileActions.fetch())
    }
  }

  componentDidMount () {
    const { dispatch } = this.props
    dispatch(MyProfileActions.fetch())
    FireMsg.onMessage( (payload) => {
      // console.log("11111111111111111Message received. ", payload.data.aps.alert);
      const pushData = JSON.parse(payload.data.aps)
      console.log(pushData)
      this.userNameOfflineRequest = pushData.alert['loc-args'][0]
      dispatch(MyProfileActions.add({
        numberUnreadNotification: 1
      }))
      if ( pushData.alert["loc-key"] === "push_like" ) {
        //いいね
      }
      // alert(payload.notification.title+"\r\n"
      //       +payload.notification.body+"\r\n"
      //       +payload.notification.icon);
    })

  }


  componentWillUnmount () {
    this.socket.disconnect()
  }

  
  componentWillReceiveProps (nextProps) {
    const requestPath = nextProps.location.pathname
    const pathinfo = requestPath.split('/')
    const userId = ( pathinfo['2'] === "chat" && pathinfo['3'] ) ? pathinfo['3'] : ''
    this.setState({
      userId: userId
    })
  }

  closeWarningFullChatDialog = () => {
    const { data } = this.props.ChatRequest;
    if(data[this.state.chatUserId]) {
      this.props.dispatch(ChatRequestActions.post({
        userId: this.state.chatUserId,
        msgId: data[this.state.chatUserId].msgId,
        accept: false
      }))
    }
  }

  openWarningFullChatDialog = chatUserId => {
    const { MyProfile } = this.props
    this.setState({ chatUserId });
    this.dialog.openDialog(MyProfile.data.maxNumberUserChatting);
  }
  
  
  render () {

    const { MyProfile, match } = this.props

    if (!MyProfile.loaded) {
      return null
    }

    return (
      <div>
        <Header socket={this.socket} bottomItemActive={this.state.bottomItemActive} userId={this.state.userId} { ...this.props } />
        <LeftMenu userId={ this.state.userId}
          history={ this.props.history } 
          userNameOfflineRequest = {this.userNameOfflineRequest}
          openWarningFullChatDialog={ chatUserId => this.openWarningFullChatDialog(chatUserId) }
        />
        <Switch>
          <Route exact path={`${match.path}`} render={props => <Home activeItem={this.activeItem} {...props} />} />
          <Route exact path={`${match.path}/chat/:userid`} render={props=><Chat socket={this.socket} {...props} />} />
          <Route exact path={`${match.path}/timeline`} render={props => <Timeline activeItem={this.activeItem} {...props} />} />
          <Route path={`${match.path}/reward`} render={props => <RouteReward activeItem={this.activeItem} {...props} />} />
          <Route path={`${match.path}/setting`} render={props => <RouteSetting activeItem={this.activeItem} {...props} />} />
          <Route path={`${match.path}/customermanagement`} render={props => <RouteCustomerManagement activeItem={this.activeItem} {...props} />} />
          <Route path={`${match.path}/review`} component={Review} />
          <Route path={`${match.path}/help/:id?`} component={Help} />
          <Route path={`${match.path}/staffblog`} component={RouteStaffBlog} />
          <Route path={`${match.path}/menu`} render={props => <Menu activeItem={this.activeItem} {...props} />} />
          <Route path={`${match.path}/mission`} render={props => <RouteMission activeItem={this.activeItem} {...props} />} />
          <Route component={NotFound} />
        </Switch>
        <DialogWarningFullChat ref={ dialog => this.dialog = dialog } closeWarningFullChatDialog={ this.closeWarningFullChatDialog }/>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
    ChatRequest: state.ChatRequest,
  }
}

export default connect(
  mapStateToProps
)(Layout);
