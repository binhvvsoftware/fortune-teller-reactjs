import React, {Component} from 'react'
import { connect } from 'react-redux'
import { glasConfig } from '../constants/Config'
import * as TemplateActions from '../actions/TemplateActions'
import * as Fetch from '../util/Fetch'
import SettingMenu from './SettingMenu'
import SettingTemplate from './SettingTemplate'

class SettingTemplates extends Component {
  constructor (props) {
    super(props)
  }

  componentWillMount() {
    const { dispatch, Template } = this.props
    const tellerId = Fetch.tellerId()

    // テンプレート取得
    dispatch(TemplateActions.getTemplate(tellerId))
  }

  render () {
    const { Template } = this.props

    let formNo = 0
    let templates = []
    if(Object.keys(Template.data).length){
      templates = Template.data.map((template) => {
        formNo++
        return <SettingTemplate key={`formTemplate${formNo}`} formNo={formNo} templateTitle={template.templateTitle} templateContent={template.templateContent} templateId={template.templateId} />
      })
    }
    if(glasConfig.template.template > Object.keys(Template.data).length){
      let start = Object.keys(Template.data).length
      for(start; start < glasConfig.template.template; start++){
        formNo++
        templates.push(
          <SettingTemplate key={`formTemplate${formNo}`} formNo={formNo} templateTitle={''} templateContent={''} templateId={''} />
        )
      }
    }

    return (
      <div className="content">
        <h1 className="content__title">設定</h1>
        <SettingMenu />
        <div className="primary">
          <div className="primary-title primary-title-template">
            <h1 className="primary-title__main hidden-sp">テンプレート</h1>
            <p className="primary-title__description primary-title__description-template">チャット相談の際に仕様できる、メッセージテンプレートを編集できます。<br/>テンプレートの内容は無料で送信されます。</p>
          </div>
          <div className="primary__inner_template">
            {templates}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    Template: state.Template
  }
}

export default connect(
  mapStateToProps
)(SettingTemplates);
