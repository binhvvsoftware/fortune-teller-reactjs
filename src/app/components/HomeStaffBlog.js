/**
 * Home画面に出すスタッフブログ
 *
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import StaffBlogList from './StaffBlogList'

class HomeStaffBlog extends Component {

  render () {
    const { data } = this.props

    if (!data.loaded) {
      return null
    }

    if (!Object.keys(data).length) {
      return null
    }

    return (
      <div>
        <h2 className="content__title lv_2">運営ブログ</h2>
        <ul className="list blog_list">
          <StaffBlogList data={data.data} />
        </ul>
        <Link to="/home/staffblog/1" className="link-to-staff" >運営ブログをもっと見る</Link>
      </div>
    )
  }
}




const mapStateToProps = (state) => {
  return {}
}

export default connect(
  mapStateToProps
)(HomeStaffBlog);
