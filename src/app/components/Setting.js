import React, {Component} from 'react'
import { connect } from 'react-redux'
import * as MyProfileActions from '../actions/MyProfileActions'
import * as Fetch from '../util/Fetch'
import { glasConfig, Styles, Methods, Genres } from '../constants/Config'
import SettingGenres from './SettingGenres'
import SettingMethods from './SettingMethods'
import SettingStyles from './SettingStyles'
import * as FileActions from '../actions/FileActions'
import SettingMenu from './SettingMenu'
import PreviewProfile from './PreviewProfile'
import Dialog from 'material-ui/Dialog';
// import FlatButton from 'material-ui/FlatButton';
import SettingProfileImage from './SettingProfileImage'
import SparkMD5 from 'spark-md5'
import * as WebSocket from '../util/WebSocket'
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar'
import FlatButton from 'material-ui/FlatButton';
import { AvatarStatus } from '../constants/Config';
import {PROFILE_SAVE_STATUS} from '../reducers/MyProfile'
import RequireSubmitChanges from '../components/RequireSubmitChanges'
import $ from 'jquery';

class Setting extends Component {

  constructor (props) {
    super(props)
    const { MyProfile } = this.props
    const fortuneTellerName = MyProfile.data.fortuneTellerName ? MyProfile.data.fortuneTellerName : ''
    const fortuneTellerMessage = MyProfile.data.message ? MyProfile.data.message : ''
    this.state = {
      tellerName: fortuneTellerName,
      tellerNameLength: fortuneTellerName.length,
      tellerMessage: fortuneTellerMessage,
      tellerMessageLength: fortuneTellerMessage.length,
      dialogOpen: false,
      message: '',
      saveButton: false,
      genresDefaultChecked: [],
      methodsDefaultChecked: [],
      stylesDefaultChecked: [],
      genresChecked: [],
      methodsChecked: [],
      stylesChecked: [],
      stylesDisabled: [],
      genresDisabled: [],
      methodsDisabled: [],
      postImage: '',
      selectedAvatar: false,
      isValidAvatar : false,
      openPreviewDialog: false,
      profileImage: './img/thumb_noimage.png',
      firstTimeSetting : true,
      focusOnMsgTxt: false,
      error: false,
      display:false,
      twitterUrl: MyProfile.data.twitterUrl ? MyProfile.data.twitterUrl : '',
      facebookUrl: MyProfile.data.facebookUrl ? MyProfile.data.facebookUrl : '',
      otherUrl: MyProfile.data.otherUrl ? MyProfile.data.otherUrl : '',
      checkSubmit:true,
      cancel:false,
      show: false,
      targetPathName:'',
      submitSuccess: false
    }
  }

  componentWillMount() {
    const { dispatch,MyProfile } = this.props
    dispatch(MyProfileActions.fetch('profile'))
    this.setState({
        genresDefaultChecked: MyProfile.data.judgementGenres,
        methodsDefaultChecked: MyProfile.data.fortuneMethods,
        stylesDefaultChecked: MyProfile.data.styles
    })
  }

  componentDidMount(){
    this.props.history.block((location,action) => {
      const currentPathName = window.location.pathname;
      if(!this.state.checkSubmit && !this.state.cancel){
        this.setState({show:true,targetPathName:location.pathname})
        return false;
      }
    })
  }

  componentWillReceiveProps(nextProps){

    let saveButton = this.state.saveButton
    if(nextProps.MyProfile.data.fortuneTellerName.replace(/\s/g, "").length === 0 && this.state.saveButton === false){
      saveButton = true
    }
    if(nextProps.MyProfile.data.message && nextProps.MyProfile.data.message.replace(/\s/g, "").length === 0 && this.state.saveButton === false){
      saveButton = true
    }
    let profileImage = './img/thumb_noimage.png'
    let postImage = ''
    let isChangingAvatar = false;
    if(nextProps.MyProfile.data.avatarPath){
      profileImage = nextProps.MyProfile.data.avatarPath
    }
    if(nextProps.File.profileImageSrc){
      isChangingAvatar = true;
      postImage = nextProps.File.profileImageSrc
    }

    if(!isChangingAvatar) {
      this.setState({
        tellerName: nextProps.MyProfile.data.fortuneTellerName,
        tellerNameLength: (nextProps.MyProfile.data.fortuneTellerName)?nextProps.MyProfile.data.fortuneTellerName.length: 0,
        tellerMessage: nextProps.MyProfile.data.message,
        tellerMessageLength: this.state.tellerMessageLength,
        saveButton: saveButton,
        genresDefaultChecked: nextProps.MyProfile.data.judgementGenres,
        methodsDefaultChecked: nextProps.MyProfile.data.fortuneMethods,
        stylesDefaultChecked: nextProps.MyProfile.data.styles,
        firstTimeSetting : nextProps.MyProfile.data.judgementGenres.length == 0 || nextProps.MyProfile.data.fortuneMethods.length == 0 || nextProps.MyProfile.data.styles.length == 0,
      });
      if(nextProps.MyProfile.data.avatarStatus == AvatarStatus.DENY) {
        profileImage = '';
        postImage = '';
      }
    }
    
    this.setState({
      profileImage: profileImage,
      postImage: postImage
    })

    if ((typeof this.props.MyProfile.savedProfile === 'undefined' || this.props.MyProfile.savedProfile === null) && typeof nextProps.MyProfile.savedProfile !== 'undefined' && nextProps.MyProfile.savedProfile !== null) {
      if (!nextProps.MyProfile.savedProfile && nextProps.MyProfile.savedCode !== PROFILE_SAVE_STATUS.SUCCESS) {
        let message = '';

        switch (nextProps.MyProfile.savedCode) {
            case PROFILE_SAVE_STATUS.LINK_HAS_NG_WORD:
              message = 'ご利用のURLは設定を禁止されております。詳しくは事務局までお問い合わせください。'
              break
            case PROFILE_SAVE_STATUS.TWITTER_LINK_ERROR:
              message = '設定されたURLはTwitterのものではありませんので、正しいURLを選択してください。'
              break
            case PROFILE_SAVE_STATUS.FACEBOOK_LINK_ERROR:
              message = '設定されたURLはFacebookのものではありませんので、正しいURLを選択してください。'
              break
            case PROFILE_SAVE_STATUS.OTHER_LINK_ERROR:
              message = '正しいURLが設定できておりません。もう一度ご確認ください。'
              break
            default:
              message = ''
        }

        this.setState({error: true, submitSuccess: false, message: message});
      } else {
        this.setState({error: false, submitSuccess: true, checkSubmit: true});
      }
    }
  }

  handleChange (e) {
    // 占い師名
    this.setState({checkSubmit:false})
    if (e.target.name === 'tellerName') {
      const value = e.target.value.replace(/\s/g, "")
      const length = value.length
      if ( length > glasConfig.setting.name ) {
        return
      }
      this.setState({
        tellerName: value,
        tellerNameLength: length
      })
    }

    // メッセージ
    if(e.target.name === 'tellerMessage'){
        const value = e.target.value
        const length = value.length
        this.cursor = e.target.selectionStart;

        if(length > glasConfig.setting.message){
            return
        }
        this.setState({
            tellerMessage: value,
            tellerMessageLength: length
        })
    }

    // プロフィール写真
    if(e.target.name === 'tellerAvatar'){
      if (e.target.files && e.target.files.length) {
        this.setState({
          selectedAvatar: true
        })
        this.handleOpen()
      }
    }
  }

  handleClick(event, isInputChecked, target, targetMessage){
    this.setState({checkSubmit:false})
    let checkCount = (this.state[target+'Checked'].length)?this.state[target+'Checked']:this.state[target+'DefaultChecked']

    let checked = []
    let disabled = []
    let message = ''
    let state = {
      message: message,
      error: false,
      saveButton: false,
    }
    let countFlag = false
    // チェックした
    if(isInputChecked){
        // 相談スタイルのみ個数制限
        if(target === 'styles' && glasConfig.setting.styles >= checkCount.length + 1){
          countFlag = true
          checked.push(event.target.value)
        }else if(target !== 'styles'){
          checked.push(event.target.value)
        }
    // チェック外した
    }else{
      checkCount = Array.prototype.filter.call(checkCount, val => val !== event.target.value)
    }

    for(let i in checkCount){
      if(countFlag && checkCount[i] === event.target.value){
        continue
      }
      checked.push(checkCount[i])
    }

    // 個数以上チェックした場合に無効化
    if(target === 'styles' && glasConfig.setting.styles === checked.length){
      disabled = Array.prototype.filter.call(Styles, val => checked.indexOf(val) === -1)
    }

    if(target === 'genres' && glasConfig.setting.genres === checked.length){
      disabled = Array.prototype.filter.call(Genres, val => checked.indexOf(val) === -1)
    }
    if(target === 'methods' && glasConfig.setting.methods === checked.length){
      disabled = Array.prototype.filter.call(Methods, val => checked.indexOf(val) === -1)
    }

    if(checkCount.length === 0){
      message = targetMessage
    }
    state[target+'Checked'] = checked
    // チェックを操作した時点でデフォルト値は不要なので全部削除
    state[target+'DefaultChecked'] = []
    // 相談スタイル無効化項目
    state[target+'Disabled'] = disabled
    state['firstTimeSetting'] = false

    if(this.state.saveButton === false && checked.length === 0){
      state['saveButton'] = true
      state['error'] = true
      state['message'] = message
    }
    this.setState(state)
  }

  // フォームデータ取得
  getFormData(){
    const fortuneMethods = (() => {
        let checkbox = Array.prototype.filter.call(document.querySelectorAll('input[name="methods[]"]'), elem => elem.checked)
        let list = []
        Array.prototype.forEach.call(checkbox, (elem) => {
            list.push(elem.value)
        })
        return list
    })()
    const judgementGenres = (() => {
        let checkbox = Array.prototype.filter.call(document.querySelectorAll('input[name="genres[]"]'), elem => elem.checked)
        let list = []
        Array.prototype.forEach.call(checkbox, (elem) => {
            list.push(elem.value)
        })
        return list
    })()
    const styles = (() => {
        let checkbox = Array.prototype.filter.call(document.querySelectorAll('input[name="styles[]"]'), elem => elem.checked)
        let list = []
        Array.prototype.forEach.call(checkbox, (elem) => {
            list.push(elem.value)
        })
        return list
    })()
    const fortuneTellerName = document.updateTeller.tellerName.value
    const tellerMessage = document.updateTeller.tellerMessage.value
    const twitterUrl = document.updateTeller.twitterUrl.value.trim()
    const facebookUrl = document.updateTeller.facebookUrl.value.trim()
    const otherUrl = document.updateTeller.otherUrl.value.trim()

    return {
      fortuneMethods,
      judgementGenres,
      styles,
      fortuneTellerName,
      tellerMessage,
      twitterUrl,
      facebookUrl,
      otherUrl
    }
  }

  handleSubmit(e){
    // 更新
    if(e.target.name === 'updateTeller'){
      e.preventDefault()

      const settingTypes = ['genres', 'methods', 'styles'];
      let isValidForm = true;

      for (let type of settingTypes) {
        const checkedItem = this.state[type+'Checked'].length ? this.state[type+'Checked'] : this.state[type+'DefaultChecked'];
        if(checkedItem.length < 3) {
          isValidForm = false;
          break;
        }
      }

      if(!this.state.firstTimeSetting && !isValidForm) {
        this.setState({error: true, message : '3つ以上選択してください '});
        return;
      }

      const { MyProfile, dispatch } = this.props

      let formData = this.getFormData()

      // 画像更新
      if(this.state.selectedAvatar && this.state.postImage){
        if(!this.state.isValidAvatar) {
          this.setState({error: true, message : 'プロフィール画像が正常に登録できませんでした。再度ご登録をお願い致します。'});
          return;
        }
        
        // canvasから画像生成
        let type = 'image/jpeg'
        let bin = atob(this.state.postImage.split(',')[1])
        let buffer = new Uint8Array(bin.length)
        for(let i=0;i<bin.length;i++){
          buffer[i] = bin.charCodeAt(i)
        }
        let blob = new Blob([buffer.buffer], {type: type})

        // チェックサム計算
        let fileReader = new FileReader()
        let spark = new SparkMD5.ArrayBuffer()
        fileReader.onload = (event) => {
          let spark = new SparkMD5.ArrayBuffer()
          spark.append(event.target.result)
          let hash = spark.end()

          let imgFormData = new FormData()
          imgFormData.append('file', blob, 'profile.jpg')
          imgFormData.append('md5Sum', hash)

          const data = {
            fileData: imgFormData,
            profileData: {
              fortuneTellerName  : formData.fortuneTellerName,
              message: formData.tellerMessage,
              fortuneMethods : formData.fortuneMethods,
              judgementGenres : formData.judgementGenres,
              styles : formData.styles,
              isNeedValidate : ! this.state.firstTimeSetting,
              twitterUrl: formData.twitterUrl,
              facebookUrl: formData.facebookUrl,
              otherUrl: formData.otherUrl
            }
          }
          dispatch(MyProfileActions.profileImagePost(this.props.MyProfile, data))
          document.updateTeller.tellerAvatar.value = ''
          this.setState({
            selectedAvatar: false
          })
        }
        fileReader.readAsArrayBuffer(blob)
      // 画像未更新
      }else{
        let avt = null
        if (this.state.profileImage !== './img/thumb_noimage.png') {
          avt = this.state.profileImage
        }
        
        if(!avt) {
          this.setState({error: true, message : 'プロフィール画像が正常に登録できませんでした。再度ご登録をお願い致します。'});
          return;
        }

        const data = {
          fortuneTellerName  : formData.fortuneTellerName,
          message: formData.tellerMessage,
          fortuneMethods : formData.fortuneMethods,
          judgementGenres : formData.judgementGenres,
          styles : formData.styles,
          isNeedValidate : ! this.state.firstTimeSetting,
          avatarPath: avt,
          twitterUrl: formData.twitterUrl,
          facebookUrl: formData.facebookUrl,
          otherUrl: formData.otherUrl
        }
        dispatch(MyProfileActions.put('profile',data))
      }
    }
  }

  handleBlur(event){
    // 占い師名
    if (event.target.name === 'tellerName'){
      const value = event.target.value.replace(/\s/g, "")
      const length = value.length
      let error = false
      let message = ''
      let saveButton =this.state.saveButton
      if(length == 0 && saveButton === false){
        message = '名前を入力してください'
        error = true
        saveButton = true
      }
      this.setState({
        message: message,
        error: error,
        saveButton: saveButton
      })
    }

    // メッセージ
    if(event.target.name === 'tellerMessage'){
      const value = event.target.value.replace(/\s/g, '')
      const length = value.length
      let error = false
      let message = ''
      let saveButton = this.state.saveButton
      if(length == 0 && saveButton === false){
        message = 'メッセージを入力してください'
        error = true
        saveButton = false
      }
      this.setState({
        message: message,
        error: error,
        saveButton: saveButton
      })
    }
  }

  // ダイアログ表示
  handleOpen(){
    this.setState({
      dialogOpen: true
    })
  }

  // ダイアログ非表示
  handleClose(status){
    if(status != 'upload'){
      document.updateTeller.tellerAvatar.value = ''
    }
    this.setState({
      dialogOpen: false
    })
  }

  handleErrorClose(){
    this.setState({
      error: false,
      message: ''
    })
  }

  handleSubmitClose(){
    this.setState({
      submitSuccess: false
    })
  }

  handleBtnClick(event){
    event.preventDefault()
    document.updateTeller.tellerAvatar.click()
  }

  validateAvaPath(file){
    if(!file.size) {
      return false
      this.setState({isValidAvatar : false});
    } 

    console.log(file);
    const fileType = file.type;
    const validFileExtensions = ["image/png", "image/jpeg", "image/jpg"];

    console.log(validFileExtensions.indexOf(fileType) >= 0);
    if(validFileExtensions.indexOf(fileType) == -1) {
      this.setState({error: true, message : 'プロフィール画像が正常に登録できませんでした。再度ご登録をお願い致します。'});
      return false;
    }

    this.setState({isValidAvatar : validFileExtensions.indexOf(fileType) >= 0});
    return true;
  }
  handleFocusMsgTxt(e){
    e.target.selectionStart = this.cursor;
    this.setState({focusOnMsgTxt : true});
  }

  handleBlurMsgTxt(){
    if(this.state.focusOnMsgTxt && ! this.state.tellerMessageLength) {
      this.setState({error: true, message : 'メッセージを入力してください'});
    }
  }

  handleBtnPreviewOnApp(e){
    e.preventDefault()
      
    const settingTypes = ['genres', 'methods', 'styles'];
    let isValidForm = true;
    for (let type of settingTypes) {
      const checkedItem = this.state[type+'Checked'].length ? this.state[type+'Checked'] : this.state[type+'DefaultChecked'];
      if(checkedItem.length < 3) {
        isValidForm = false;
        break;
      }
    }

    if(!isValidForm) {
      this.setState({error: true, message : '3つ以上選択してください '});
      return;
    }

    if(this.state.focusOnMsgTxt && ! this.state.tellerMessageLength) {
      this.setState({error: true, message : 'メッセージを入力してください'});
    }

    this.setState({openPreviewDialog: true})
  }

  onClickCancel = () => {
    this.setState({show:false})
  }

  onClickOk = () => {
    this.setState({checkSubmit : true}, () => {
      this.props.history.push(this.state.targetPathName);
    });
  }

  render () {
    const { MyProfile, File } = this.props
    if (!MyProfile.loaded) {
      return ''
    }

    let imageSituation = ''
    if(MyProfile.data.verifyAge === 'Reviewing' || parseInt(MyProfile.data.verifyAge, 10) === 0){
      imageSituation = (<figcaption className="prof-thumbnail__figcaption">審査中</figcaption>)
    }else if(MyProfile.data.verifyAge === 'ReviewNG' || parseInt(MyProfile.data.verifyAge, 10) === -1){
      imageSituation = (<figcaption className="prof-thumbnail__figcaption">審査NG</figcaption>)
    }

    let avatarStatus = '';
    if(MyProfile.data.avatarStatus == AvatarStatus.PENDDING){
      avatarStatus = (<figcaption className="prof-thumbnail__figcaption" style={{ backgroundColor: '#ec407a' }}>審査中</figcaption>)
    } else if(MyProfile.data.avatarStatus == AvatarStatus.MODIFY){
      avatarStatus = (<figcaption className="prof-thumbnail__figcaption" style={{ backgroundColor: '#0000ff' }}>加工中</figcaption>)
    }

    const tellerNameLength = ( glasConfig.setting.name > this.state.tellerNameLength ) ? glasConfig.setting.name - this.state.tellerNameLength + ' / 12' : ''
    const tellerMessageLength = ( glasConfig.setting.message > this.state.tellerMessageLength )? glasConfig.setting.message - this.state.tellerMessageLength + ' / 1500' : ''

    const actions = [
    ];

    return (
      <div className="content">
        {/* <Loading display={this.state.display}/> */}
        <h1 className="content__title">設定</h1>
        <SettingMenu />
        <div className="primary">
          <div className="primary-title">
            <h1 className="primary-title__main">プロフィール情報</h1>
            <p className="primary-title__subtitle subtitle_setting">お客様に公開される情報です</p>
          </div>
          <div className="primary__inner">
            <form name="updateTeller" onSubmit={e=>this.handleSubmit(e)}>
              <div className="definition wide_bottom prof-thumbnail-section">
                <div className="prof-thumbnail-section__left">
                  <dl className="definition wide_bottom">
                    <dt className="definition__title hidden-sp">プロフィール写真</dt>
                    <dd className="definition__data">
                      <div className="prof-thumbnail">
                        <figure className="prof-thumbnail__figure">
                          <img className="prof-thumbnail__image"
                               src="/img/spacer.gif"
                               style={{backgroundImage:`url(${this.state.postImage || this.state.profileImage})`}} alt="" />
                            {imageSituation}
                            {avatarStatus}
                        </figure>
                        <div className="prof-thumbnail__button btn-wrap">
                          <button className="btn-raised" onClick={e=>this.handleBtnClick(e)}><i className="material-icons btn__icons">insert_photo</i>写真選択</button>
                          <input type="file" name="tellerAvatar" accept="image/*" onChange={e=>this.handleChange(e)} style={{'display': 'none'}} />
                        </div>
                      </div>
                    </dd>
                  </dl>
                  <dl className="definition">
                    <dt className="definition__title">名前</dt>
                    <dd className="definition__data">
                      <div className="textfield">
                        <TextField
                            className="textfield__input"
                            name="tellerName"
                            maxLength="12"
                            id="handle_name"
                            hintText="名前を入力してください"
                            onBlur={(e)=>this.handleBlur(e)}
                            onChange={e=>this.handleChange(e)}
                            value={this.state.tellerName}
                            floatingLabelText={tellerNameLength}
                            floatingLabelFixed={true}
                            style={textfieldStyles.block}
                            floatingLabelStyle={textfieldStyles.label}
                            inputStyle={textfieldStyles.textarea}
                        />
                      </div>
                    </dd>
                  </dl>
                </div>
                <div className="prof-thumbnail-section__right">
                  <div className="thumbnail-guide">
                    <p className="thumbnail-guide__title">プロフィール写真を魅力的に加工します。</p>
                    <p className="thumbnail-guide__desc">下記条件のお写真をご登録ください。<span>条件を満たしていない場合は再登録となります。</span></p>
                    <div className="thumbnail-guide__image">
                      <img src="/img/profile-sample-image.png" />
                    </div>
                  </div>
                </div>
              </div>
              <SettingGenres checked={this.state.genresChecked} defaultChecked={this.state.genresDefaultChecked} disabled={this.state.genresDisabled} handleClick={this.handleClick.bind(this)}/>
              <SettingMethods checked={this.state.methodsChecked} defaultChecked={this.state.methodsDefaultChecked} disabled={this.state.methodsDisabled} handleClick={this.handleClick.bind(this)}/>
              <SettingStyles checked={this.state.stylesChecked} defaultChecked={this.state.stylesDefaultChecked} disabled={this.state.stylesDisabled} handleClick={this.handleClick.bind(this)}/>
              <dl className="definition wide_bottom">
                <dt className="definition__title">自己紹介</dt>
                <dd className="definition__data">
                  <div className="textfield spread_width">
                    <TextField
                      className="textfield__textarea"
                      fullWidth={true}
                      type="text"
                      name="tellerMessage"
                      id="introduction"
                      placeholder="ご自身の経歴や占術について記入してくだい"
                      defaultValue={this.state.tellerMessage}
                      onChange={e=>this.handleChange(e)} 
                      floatingLabelText={tellerMessageLength}
                      floatingLabelFixed={true}
                      rows={3}
                      multiLine={true}
                      onFocus={e => this.handleFocusMsgTxt(e)}
                      onBlur={e => this.handleBlurMsgTxt(e)}
                      // rowsMax={4}
                      hintStyle={textfieldStyles.hint}
                      floatingLabelStyle={textfieldStyles.label}
                      textareaStyle={textfieldStyles.textarea}
                    />
                  </div>
                </dd>
              </dl>
              <dl className="definition wide_bottom setting-external-url">
                <dt className="definition__title">外部サイトURL</dt>
                <dd className="definition__data">
                  <div className="labelfield">Twitter</div>
                  <div className="textfield spread_width">
                    <TextField
                        className="textfield__textarea"
                        fullWidth={true}
                        type="text"
                        name="twitterUrl"
                        id="twitter-url"
                        hintText="https://twitter.com/〜"
                        defaultValue={this.state.twitterUrl}
                    />
                  </div>
                  <div className="labelfield">Facebook</div>
                  <div className="textfield spread_width">
                    <TextField
                        className="textfield__textarea"
                        fullWidth={true}
                        type="text"
                        name="facebookUrl"
                        id="facebook-url"
                        hintText="https://www.facebook.com/〜"
                        defaultValue={this.state.facebookUrl}
                    />
                  </div>
                  <div className="labelfield">ホームページ/ブログ</div>
                  <div className="textfield spread_width">
                    <TextField
                        className="textfield__textarea"
                        fullWidth={true}
                        type="text"
                        name="otherUrl"
                        id="other-url"
                        defaultValue={this.state.otherUrl}
                    />
                  </div>
                </dd>
              </dl>
              <div className="btn-wrap">
                <input type="button" 
                  value="プロフィールをプレビューする" 
                  style={styles.previewBtn}
                  onClick={e => this.handleBtnPreviewOnApp(e)}
                  className="btn-raised color_default hidden-sp" 
                  disabled={this.state.saveButton} />
                <input type="button" 
                  value="プレビュー" 
                  style={styles.previewBtn}
                  onClick={e => this.handleBtnPreviewOnApp(e)}
                  className="btn-raised color_default-sp input1" 
                  disabled={this.state.saveButton} />
                <input type="submit" value="登録" className="btn-raised color_default hidden-sp" disabled={this.state.saveButton} />
                <input type="submit" value="登録" className="btn-raised color_default-sp input2" disabled={this.state.saveButton} />
              </div>
            </form>
          </div>
          <Dialog
            title="プロフィール写真"
            actions={actions}
            modal={false}
            open={this.state.dialogOpen}
            onRequestClose={(s)=>{this.handleClose(s)}}
            autoDetectWindowHeight={true}
            autoScrollBodyContent={true}
            bodyClassName="prof-dialog-wrap"
            contentClassName="avatar-dialog"
          >
            <div className="prof-dialog-wrap__subtitle hidden-sp">ご本人以外の写真の使用は禁止です。</div>
            <SettingProfileImage onClose={(s)=>{this.handleClose(s)}} {...this.props} validateAvaPath={ file => this.validateAvaPath(file) } />
          </Dialog>
          <Dialog
            title={false}
            contentStyle={styles.previewDialogStyle}
            autoScrollBodyContent
            actions={[<FlatButton
              label="OK"
              primary={true}
              keyboardFocused={true}
              onClick={() => this.setState({openPreviewDialog : false})}
            />]}
            modal={true}
            open={this.state.openPreviewDialog}
          >
            <PreviewProfile 
              image={`${this.state.postImage || this.state.profileImage}`}
              name={this.state.tellerName}
              message={this.state.tellerMessage}
              genresChecked={this.state.genresDefaultChecked.length ? this.state.genresDefaultChecked : this.state.genresChecked} 
              methodsChecked={this.state.methodsDefaultChecked.length ? this.state.methodsDefaultChecked : this.state.methodsChecked}
              stylesChecked={this.state.stylesDefaultChecked.length ? this.state.stylesDefaultChecked : this.state.stylesChecked} 
              profile={this.props.MyProfile.data}
            />
          </Dialog>
        </div>
        <Snackbar
          open={this.state.error}
          message={this.state.message}
          autoHideDuration={5000}
          onRequestClose={e=>this.handleErrorClose(e)}
          className="message-dialog"
        />
        <Snackbar
          open={this.state.submitSuccess}
          message="正常に保存しました。審査完了をお待ちください。"
          autoHideDuration={5000}
          onRequestClose={e=>this.handleSubmitClose(e)}
          className="message-dialog"
        />
        <RequireSubmitChanges {...this.props} onClickCancel={() => this.onClickCancel()} onClickOk={() => this.onClickOk()} show={this.state.show} targetPathName={this.state.targetPathName} />
      </div>
      
    )
  }
}

const styles = {
  previewDialogStyle : {
    width: 380,
    maxWidth: 'none',
  },
  previewBtn: {
    marginRight: 15
  }
};

const textfieldStyles = {
  label: {
    bottom: -40,
    top: "auto",
    right: -16
  },
  hint: {
    top: 0,
    bottom: "auto"
  },
  textarea: {
    marginTop: 0,
    marginBottom: 0,
    height: "100%"
  },
  block: {
    height: 44,
    width: '100%'
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
    File: state.File
  }
}

export default connect(
  mapStateToProps
)(Setting);
