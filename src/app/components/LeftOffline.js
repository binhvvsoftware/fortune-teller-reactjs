/**
 * 左メニューのオフライン時にリクエストをくれたユーザー一覧
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import LeftOfflineList from './LeftOfflineList'

class LeftOffline extends Component {

  constructor (props) {
    super(props)
    this.state = {
      offset: 0,
      limit: 20,
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.show && nextProps.show){
      this.setState({
        offset: 0,
      })
    }
  }


  loadData () {
    this.setState({
      offset: this.state.offset + this.state.limit
    })
  }


  render () {

    const { show, RequestOffline, chatUserId, userNameOfflineRequest } = this.props

    if ( !(show && RequestOffline.loaded) ){
      return null
    }

    //左カラムに表示する件数よりも多ければ「もっと見る」を押した際、顧客ページへ遷移させる
    const maxnum = Object.keys(RequestOffline.data).length

    if (!maxnum) {
      return (
        <div>
          <dt className="secondary-definition__title"><b>リクエスト受付</b></dt>
          <dd className="secondary-definition__data">
            <p className="secondary-definition__nodata">リクエストはありません</p>
          </dd>
        </div>
      )
    }

    //表示する件数
    const num = this.state.offset + this.state.limit

    //もっと見るボタンの生成
    const moreButton = ( maxnum <= num ) ? '' : (
        <div className="list__item">
          <button className="btn-customer customer_request" onClick={()=>this.loadData()}>もっと見る</button>
        </div>
      )

    const UserList = Object.values(RequestOffline.data).map( (res, i) => {
      if ( i < num ) {
        return (
          <LeftOfflineList key={`offlinelist_${i}`} data={res} chatUserId={chatUserId} userNameOfflineRequest = {userNameOfflineRequest}/>
        )
      }
    })

    return (
      <div>
        <dt className="secondary-definition__title">リクエスト受付</dt>
        <dd className="secondary-definition__data">
          <div className="list column_1">
            {UserList}
            {moreButton}
          </div>
        </dd>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    RequestOffline: state.RequestOffline,
  }
}

export default connect(
  mapStateToProps
)(LeftOffline)