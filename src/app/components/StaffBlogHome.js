import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import { Category } from '../constants/ConfigStaffblog'
import StaffBlog from './StaffBlog'

export default class StaffBlogHome extends Component {

  constructor (props) {
    super(props)
    this.state = {
      page: 1,
      size: 12,
      category: ''
    }
  }

  componentWillMount() {
    const { match } = this.props
    this.getData(match)
  }

  componentWillReceiveProps(nextProps) {
    const { match } = nextProps
    this.getData(match)
  }


  /**
   * 記事詳細を取得
   */
  getData (match) {

    const params = {
      page: match.params.page === undefined ? 1 : match.params.page,
      size: match.params.size === undefined ? 10 : match.params.size,
      category: match.params.category === undefined ? '' : match.params.category
    }

    this.setState(params)
  }



  render () {

    const baseurl = "/home/staffblog/1"

    return (
      <div className="content">
        <h1 className="content__title">運営ブログ</h1>
        <nav className="lead-nav lead-nav-blog">
          <ul className="list lead_nav">
          <li className="list__item"><Link className={this.state.category==='' ? "btn-tab is-active" : "btn-tab"} to={baseurl}>すべて</Link></li>
          {Object.keys(Category).map( (i) => {
            const className = ( i === this.state.category ) ? "btn-tab is-active" : "btn-tab"
            return <li key={i} className="list__item"><Link className={className} to={`${baseurl}/${i}`}>{Category[i]}</Link></li>
          })}
          </ul>
        </nav>
        <StaffBlog category={this.state.category} page={this.state.page} size={this.state.size} pager={true} />
      </div>
    )
  }
}