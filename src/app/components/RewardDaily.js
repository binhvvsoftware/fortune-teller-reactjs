import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { glasConfig } from '../constants/Config'
import request from 'axios'
import * as moment from 'moment'
import * as Fetch from '../util/Fetch'
import RewardMenu from './RewardMenu'
import * as UserInfoActions from '../actions/UserInfoActions'

class RewardDaily extends Component {

  constructor (props) {
    super(props)
    this.state = {
      month: moment().format("YYYYMM"),
      data: {}
    }
  }


  componentWillMount() {
    this.getReport(this.props.match.params.month)
  }


  componentWillReceiveProps(nextProps) {
    this.getReport(nextProps.match.params.month)
  }


  /**
   * Glasから取得したデータの成形。データがない日を0で埋める
   */
  getReportFormat = (month,data) => {

    if (!moment(month, "YYYYMM").isValid()) {
      return
    }
     
    let tmp = {}
    if ( data !== undefined && Object.values(data).length ) {
      Object.values(data).map((val)=>{
        const date = moment( val.date , "YYYYMMDD").date()
        tmp[date] = val.totalPoint
      })
    }
    const num = moment(month, "YYYYMM").daysInMonth();
    for ( let i=1; i<=num; i++){
      if (tmp[i] === undefined){
        tmp[i] = 0
      }
    }

    this.setState({
      month: month,
      data: tmp
    })

  }


  /**
   * 日別ログを取得
   */
  getReport (yyyymm) {

    const month = ( yyyymm === undefined ) ? moment().format('YYYYMM') : yyyymm
    if (!moment(month, "YYYYMM").isValid()) {
      return
    }
    const url = glasConfig.url_base + glasConfig.path_payment_reports
    const params = {
      month: month,
      fortuneTellerId: Fetch.tellerId()
    }
    const token = localStorage.getItem('token')
    const options = {
      method: 'GET',
      url: url ,
      params,
      headers: {
        'Authorization': token
      },
      json: true
    }

    request(options)
      .then(response => {
        if ( response.data.code === 0 ) {
          this.getReportFormat(month,response.data.data)
        }
      })
      .catch(error => {
        //throw error
      })
  }

  render () {
    const { MyProfile, dispatch } = this.props
    let momentObj = moment(MyProfile.data.registerTime,"YYYYMMDDHHmmss").add(9,'hours')
    let registerMonth = momentObj.month()
    let registerYear = momentObj.year()
    let currentDate = new Date()
    let currentMonth = currentDate.getMonth()
    let currentYear = currentDate.getFullYear()
    let month = moment(this.state.month,'YYYYMM').month()
    let year = moment(this.state.month,'YYYYMM').year()
    let canPrev = true
    let canNext = true

    if (year < registerYear || (year == registerYear && month <= registerMonth)) {
      year = registerYear
      month = registerMonth
      canPrev = false
    }

    if (year > currentYear || (year == currentYear && month >= currentMonth)) {
      year = currentYear
      month = currentMonth
      canNext = false
    }

    let prev = canPrev ? moment(this.state.month,'YYYYMM').subtract(1,'months').format("YYYYMM") : this.state.month
    let next = canNext ? moment(this.state.month,'YYYYMM').add(1,'months').format("YYYYMM") : this.state.month
    let prevClass = canPrev ? 'rdh__navigation__prev is-active' : 'rdh__navigation__prev'
    let nextClass = canNext ? 'rdh__navigation__next is-active' : 'rdh__navigation__next'

    return (
      <div className="content reward reward-daily">
        <h1 className="content__title">精算</h1>
        <RewardMenu />
        <div className="primary">
          <div className="primary-title">
            <h1 className="primary-title__main">日別報酬</h1>
          </div>
          <div className="primary__inner">
            <div className="primary-content">
              <div className="primary-content__body">
                <div className="primary-title lv_2 hidden-mobile">
                  <h1 className="primary-title__main">{year}年{month < 10 ? `0${month + 1}` : month + 1}月</h1>
                  <Link to={`/home/reward/daily/${prev}`}>前月</Link>
                  <Link to={`/home/reward/daily/${next}`}>翌月</Link>
                </div>
                <div className="reward-daily__header display-mobile">
                  <div className="rdh__date">{year}年{month < 10 ? `0${month + 1}` : month + 1}月</div>
                  <div className="rdh__navigation">
                    <Link to={`/home/reward/daily/${prev}`} className={prevClass}><i class="fas fa-chevron-left"></i></Link>
                    <Link to={`/home/reward/daily/${next}`} className={nextClass}><i class="fas fa-chevron-right"></i></Link>
                  </div>
                </div>
                <table className="table-th-head">
                  <thead>
                    <tr>
                      <th className="table-align-center">日付</th>
                      <th className="table-align-center">報酬</th>
                    </tr>
                  </thead>
                  <tbody>
                  { Object.values(this.state.data).map((val,i)=>{
                    return (<tr>
                      <td className="table-align-center">{month < 10 ? `0${month + 1}` : month + 1}/{moment(i+1,'DD').format("DD")}</td>
                      <td className="table-align-center">{ val ? Fetch.formatNumber(val) + "円" : "-" }</td>
                    </tr>)
                  })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile
  }
}

export default connect(
    mapStateToProps
)(RewardDaily);