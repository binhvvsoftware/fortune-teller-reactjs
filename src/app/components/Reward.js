import React, {Component} from 'react'
import { connect } from 'react-redux'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'
import * as MyProfileActions from '../actions/MyProfileActions'
import DialogNotifi from './Dialog'
import RewardMenu from './RewardMenu'
import RewardRegistForm from './RewardRegistForm'

class Reward extends Component {

  constructor (props) {
    super(props)
  }

  componentWillMount () {
    const { dispatch } = this.props
    dispatch(MyProfileActions.fetch())
  }

  componentDidMount () {
    //年齢認証がまだの場合は待機できない
    const { MyProfile } = this.props
    if ( !Fetch.isAgeVerify(MyProfile.data.verifyAge) ) {
      if (Object.keys(this.refs).length) {
        this.refs.dialog.handleOpen('AGEAUTH')
      }
    }
  }

  render () {
    const { MyProfile } = this.props

    return (
      <div>
        <div className="content reward reward-default">
          <h1 className="content__title">精算</h1>
          <RewardMenu />
          <div className="primary">
            <div className="primary-title">
              <h1 className="primary-title__main">報酬確認・精算</h1>
            </div>
            <RewardRegistForm  MyProfile={MyProfile} />
            <RewardNotice />
          </div>
        </div>
        <DialogNotifi ref='dialog' />
      </div>
    )
  }
}

const RewardNotice = () => {
  return (
  <div className="primary__inner">
    <div className="primary-content">
      <div className="primary-content__body">
        <div className="primary-title lv_2">
          <h1 className="primary-title__main"><span className="hidden-mobile">報酬確認・精算</span><span className="display-mobile">精算について</span></h1>
        </div>
        <div className="frame-table first-table">
          <table className="table-th-left margin_bottom">
            <tbody>
              <tr>
                <th className="table-align-left">最低精算額</th>
                <td className="table-align-left">5,540円 （初回は3,540円）</td>
              </tr>
              <tr>
                <th className="table-align-left">支払日</th>
                <td className="table-align-left">精算申請日から2営業日</td>
              </tr>
              <tr>
                <th className="table-align-left">振込名義</th>
                <td className="table-align-left">カ）ステラ</td>
              </tr>
              <tr>
                <th className="table-align-left">振込手数料</th>
                <td className="table-align-left">1,080円→540円(オープン記念で手数料半額)</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="primary-title lv_2">
          <h1 className="primary-title__main">支払日について</h1>
        </div>
        <div className="frame-table second-table">
          <table className="table-th-head">
            <thead>
              <tr>
                <th className="table-align-center">精算ボタンを押した日</th>
                <th className="table-align-center">口座への入金日時</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="table-align-center">月曜日</td>
                <td className="table-align-center">水曜日の9時〜15時の間</td>
              </tr>
              <tr>
                <td className="table-align-center">火曜日</td>
                <td className="table-align-center">木曜日の9時〜15時の間</td>
              </tr>
              <tr>
                <td className="table-align-center">水曜日</td>
                <td className="table-align-center">金曜日の9時〜15時の間</td>
              </tr>
              <tr>
                <td className="table-align-center">木曜日</td>
                <td className="table-align-center">翌週月曜日の9時〜15時の間</td>
              </tr>
              <tr>
                <td className="table-align-center">金曜日</td>
                <td className="table-align-center">翌週火曜日の9時〜15時の間</td>
              </tr>
              <tr>
                <td className="table-align-center">土曜日</td>
                <td className="table-align-center">翌週火曜日の9時〜15時の間</td>
              </tr>
              <tr>
                <td className="table-align-center">日曜日</td>
                <td className="table-align-center">翌週火曜日の9時〜15時の間</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  )
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
  }
}

export default connect(
  mapStateToProps
)(Reward);
