import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NextLogin from './NextLogin';
import Dialog from 'material-ui/Dialog';

class Menu extends Component {
    constructor(props){
        super(props);
        this.state = {
            showNextLogin: false
        }
    }

    componentWillMount() {
        this.props.activeItem(5)
    }

    render() {
        return (
            <div>
                <div className="bottom_menu" >
                    <ul className="bottom_menu_list" >
                        <li role="presentation">
                            <Link to='/home' data-badge="0"><span>ホーム</span></Link>
                        </li>
                        <li role="presentation">
                            <Link to='/home/timeline' data-badge="0" role="button"><span>タイムライン</span></Link>
                        </li>
                        <li role="presentation">
                            <Link to='/home/customermanagement' data-badge="0" role="button"><span>顧客管理</span></Link>
                        </li>
                        <li role="presentation">
                            <Link to='/home/reward' data-badge="0" role="button"><span>精算</span></Link>
                        </li>
                        <li role="presentation">
                            <Link to='/home/review' onClick={() => this.handeleBadge('numberUnreadReview')} role="button" data-badge><span>レビュー</span></Link>
                        </li>
                        <li role="presentation">
                            <Link to='/home/setting' data-badge="0" role="button"><span>設定</span></Link>
                        </li>
                        <li role="presentation">
                            <Link to='/home/help' role="button" data-badge="0"><span>ヘルプ</span></Link>
                        </li>
                    </ul>
                    <div className="bottom_list_logout" onClick={() => this.setState({ showNextLogin: !this.state.showNextLogin })}>
                        <a href="javascript:;"  role="button"><span>ログアウト</span></a>
                    </div>
                </div>
                <LogoutDialog show={this.state.showNextLogin} closeFunc={() => this.setState({ showNextLogin: false })} />
            </div>
        )
    }
}

const LogoutDialog = ({show,closeFunc}) => {

    if (!show) {
      return null
    }
  
    return (
      <Dialog
        open={true} 
        modal={false} 
        title="ログアウト" 
        onRequestClose={()=>closeFunc()}
        bodyClassName="dialog__content"
        contentStyle={customLogoutDialogStyle}
      >
        <p className="logout-dialog__text">次回のログイン予定を登録しておくと相談件数が向上します。</p>
        <NextLogin show={true} place='logout' closeFunc={()=>closeFunc()} />
      </Dialog>
    )
}

const customLogoutDialogStyle = {
    width: '90%',
}

export default Menu