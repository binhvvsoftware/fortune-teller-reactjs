/**
 * 左メニューの対応中のユーザー一覧
 *
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import LeftRequestList from './LeftRequestList'
import LeftOffline from './LeftOffline'

class LeftRequest extends Component {

  constructor (props) {
    super(props)
  }

  render () {

    const { show, data, chatUserId, RequestOffline, history, openWarningFullChatDialog, closeNavLeft, userNameOfflineRequest, MyProfile } = this.props
    if ( !show ){
      return null
    }

    const UserList = (Object.keys(data.data).length <= 0) ? <p className="secondary-definition__nodata">対応中のお客様はいません。お客様を待ちましょう。</p> : Object.values(data.data).map( (res, i) => {
      return (
        <LeftRequestList 
          key={`requestlist_${res.msgId}`} 
          data={res} chatUserId={chatUserId} 
          history={ history } 
          maxNumberChating={ MyProfile.data.maxNumberUserChatting }
          openWarningFullChatDialog={ chatUserId => openWarningFullChatDialog(chatUserId) }
          no={ i }
          numbers={Object.keys(data.data).length}
          closeNavLeft={closeNavLeft} />
      )
    })

    return (
        <div className="secondary-definition-wrap" style={ style.listChatWrap }>
          <dl className="secondary-definition">
            <dt className="secondary-definition__title"><b>対応中</b></dt>
            <dd className="secondary-definition__data">
              <div className="list column_1 accpet-user">
                {UserList}
              </div>
            </dd>
            <LeftOffline show={true} data={RequestOffline} chatUserId={chatUserId} userNameOfflineRequest ={userNameOfflineRequest}/>
          </dl>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    RequestOffline: state.RequestOffline,
    MyProfile : state.MyProfile
  }
}

export default connect(
  mapStateToProps
)(LeftRequest)

const style = {
  listChatWrap : {
    height: window.innerHeight - 332
  }
}
