import React from 'react'
import { Link } from 'react-router-dom'

export default () => {
  let pathName = window.location.pathname

  let reward = 'btn-tab' + ((pathName === '/home/reward')?' is-active':'')
  let daily = 'btn-tab' + ((pathName.indexOf('/home/reward/daily') !== -1)?' is-active':'')
  let history = 'btn-tab' + ((pathName.indexOf('/home/reward/history') !== -1)?' is-active':'')

  return (
    <nav className="lead-nav">
      <ul className="list lead_nav">
        <li className="list__item"><Link to='/home/reward' className={reward}>報酬確認・精算</Link></li>
        <li className="list__item"><Link to='/home/reward/daily' className={daily}>日別報酬</Link></li>
        <li className="list__item"><Link to='/home/reward/history' className={history}>精算履歴</Link></li>
      </ul>
    </nav>
  )
}