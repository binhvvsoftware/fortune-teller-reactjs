import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { glasConfig, representativeBank, katakanaList } from '../constants/Config'
import request from 'axios'
import SettingMenu from './SettingMenu'
import * as MyBankActions from '../actions/MyBankActions'
import $ from 'jquery'

/**
 * 銀行選択
 */

class SettingBank extends Component {

  constructor (props) {
    super(props)
    this.state = {
      initials: '',
      hidden: false
    }
  }

  componentWillMount(){
    const { dispatch } = this.props
    request
      .get(glasConfig.url_outside + glasConfig.path_bank_account, {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => {
        if (response.data.code === 0) {
          if (!response.data.data.accountHolder || !response.data.data.accountNumber || !response.data.data.bankcode
            || !response.data.data.sitencode) {
            this.setState({
              hidden: true
            })
          }
        }
      })
    dispatch(MyBankActions.getBank())
  }

  componentWillReceiveProps(nextProps){
    let initials = []
    let banks = nextProps.Bank.bankInitial
    let re = /([^A-Za-z]).*/
    if (Array.isArray(banks)) {
      banks.forEach((current, index) => {
        let matches = current.bank_furi.match(re)
        let zenkaku = this.toZenkakuKatakana(matches[1])
        if (initials.indexOf(zenkaku) === -1) {
          initials.push(zenkaku)
        }
      })
    }

    initials.sort((a, b) => {
        a = this.toKatakanaHiragana(a.toString());
        b = this.toKatakanaHiragana(b.toString());
        if(a < b){
            return -1;
        }else if(a > b){
            return 1;
        }
        return 0;
    });

    this.getKatakanaInitial(initials)
  }

  componentDidMount(){
    $('.secondary-header').addClass('hiddenClass');
    $('.header-back').removeClass('hiddenClass');
    $(".component-title").text("銀行口座情報編集");
  }

  componentWillUnmount(){
    $('.secondary-header').removeClass('hiddenClass');
    $('.header-back').addClass('hiddenClass');
    $(".component-title").text("");
    $('.menu-header').removeClass('hiddenClass');
  }

  toZenkakuKatakana(str){
    let ret = []

    let m =
    {
      0xFF61:0x300C, 0xFF62:0x300D, 0xFF63:0x3002, 0xFF64:0x3001, 0xFF65:0x30FB, // 。「」、・
      0xFF67:0x30A1, 0xFF68:0x30A3, 0xFF69:0x30A5, 0xFF6A:0x30A7, 0xFF6B:0x30A9, // ァ
      0xFF6C:0x30E3, 0xFF6D:0x30E5, 0xFF6E:0x30E7, 0xFF6F:0x30C3, 0xFF70:0x30FC, // ャュョッー
      0xFF71:0x30A2, 0xFF72:0x30A4, 0xFF73:0x30A6, 0xFF74:0x30A8, 0xFF75:0x30AA, // ア
      0xFF76:0x30AB, 0xFF77:0x30AD, 0xFF78:0x30AF, 0xFF79:0x30B1, 0xFF7A:0x30B3, // カ
      0xFF7B:0x30B5, 0xFF7C:0x30B7, 0xFF7D:0x30B9, 0xFF7E:0x30BB, 0xFF7F:0x30BD, // サ
      0xFF80:0x30BF, 0xFF81:0x30C1, 0xFF82:0x30C4, 0xFF83:0x30C6, 0xFF84:0x30C8, // タ
      0xFF85:0x30CA, 0xFF86:0x30CB, 0xFF87:0x30CC, 0xFF88:0x30CD, 0xFF89:0x30CE, // ナ
      0xFF8A:0x30CF, 0xFF8B:0x30D2, 0xFF8C:0x30D5, 0xFF8D:0x30D8, 0xFF8E:0x30DB, // ハ
      0xFF8F:0x30DE, 0xFF90:0x30DF, 0xFF91:0x30E0, 0xFF92:0x30E1, 0xFF93:0x30E2, // マ
      0xFF94:0x30E4,                0xFF95:0x30E6,                0xFF96:0x30E8, // ヤ
      0xFF97:0x30E9, 0xFF98:0x30EA, 0xFF99:0x30EB, 0xFF9A:0x30EC, 0xFF9B:0x30ED, // ラ
      0xFF9C:0x30EF,                0xFF66:0x30F2,                0xFF9D:0x30F3, // ワヲン
      0xFF70:0x30FC, // ー
      0xFF9E:0x309B, 0xFF9F:0x309C // 濁点、半濁点
    }

    let c = str.charCodeAt(0)
    ret.push(m[c] || c)
    return String.fromCharCode.apply(null, ret)
  }

  toKatakanaHiragana(src){
    return src.replace(/[\u30a1-\u30f6]/g, (match) => {
        let chr = match.charCodeAt(0) - 0x60
        return String.fromCharCode(chr)
    })
  }

  getKatakanaInitial(initials){
    let parentPath = this.getParentName(this.props.match.path)

    let katakana = Array.prototype.map.call(katakanaList, (current, index)=>{
      if(initials.indexOf(current.katakana) !== -1){
        return (<li className="list__item" key={current.katakana + index}><Link to={{pathname: `${parentPath}/initials/${current.initials}`}} className="bank-link width_short">{current.katakana}</Link></li>)
      }else{
        return (<li className="list__item" key={current.katakana + index}><span className="bank-link width_short">{current.katakana}</span></li>)
      }
    })

    this.setState({
      katakana: katakana
    })
  }

  getParentName(path){
    return path.split('/').slice(0, -1).join('/')
  }

  render () {
    let parentPath = this.getParentName(this.props.match.path)

    let bank = Array.prototype.map.call(representativeBank, (current, index)=>{
      let name = current.code == '0036' ? '楽天銀行' : current.name
      return (<li className="list__item" key={name + index}><Link to={{pathname: `${parentPath}/branches/${current.code}`}} className="bank-link width_long">{name}</Link></li>)
    })

    let katakana = null
    if(this.state.katakana){
      katakana = this.state.katakana
    }

    return (
      <div className="content bank-detail bank-setting">
        <h1 className="content__title">設定</h1>
        <SettingMenu />
        <div className="primary">
          <div className="primary-title">
            <h1 className="primary-title__main">銀行口座情報</h1>
          </div>
          <div className="primary__inner">
            <div className="primary-content">
              { this.state.hidden &&
                <div className="primary-content__header">
                  <p>報酬の振込先銀行口座を登録してください。</p> 
                </div>
              }
              <div className="primary-content__body">
                <div className="primary-title lv_2">
                  <h2 className="primary-title__main">銀行名を選択</h2>
                  <ul className="list flex_start margin_bottom">
                    {bank}
                  </ul>
                </div>
                <div className="primary-title lv_2">
                  <h2 className="primary-title__main">上記以外の銀行の場合<span className="hidden-mobile">は、下記より該当する銀行名の先頭文字を選択してください。</span></h2>
                  <ul className="list japanese_syllabary margin_bottom">
                    {katakana}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { Bank: state.Bank }
}

export default connect(
  mapStateToProps
)(SettingBank);
