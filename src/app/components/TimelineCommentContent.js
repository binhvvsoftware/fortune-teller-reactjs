import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as TimelineActions from '../actions/TimelineActions'
import { Gender } from '../constants/Config'
import * as moment from 'moment'
import TimelineSubCommentContent from './TimelineSubCommentContent'
import TimelineCommentInputContent from './TimelineCommentInputContent'

class TimelineCommentContent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      disabled: 'disabled',
      delCommentId : '',
    }
    this.handleClick = this.handleClick.bind(this)
  }

  componentWillMount() {
    const { dispatch, postId, commentNumber } = this.props
    if(commentNumber > 0){
      dispatch(TimelineActions.timelineCommentGet({postId: postId}))
    }
  }

  componentWillReceiveProps(nextProps){
  }

  handleChange(event){
    let disabled = 'disabled'
    if(event.target.value.length !== 0){
      disabled = ''
    }

    this.setState({
      'disabled': disabled
    })
  }

  acceptDeleteComment = () => {
    const { dispatch } = this.props;
    dispatch(TimelineActions.timelineCommentDelete(this.state.delCommentId));
    document.getElementById(`commentId${this.state.delCommentId}`).style.display = 'none';
  }

  handleClick(event, commentId){
    const { confirmDelDialog } = this.props;
    this.setState({ delCommentId : commentId }, () => {
      confirmDelDialog.openDialog( () => this.acceptDeleteComment());
    });
  }

  // 全コメント取得
  getComments(){
    let comments = []

    if(this.props.Timeline.hasOwnProperty('comments') && this.props.commentNumber > 0){
      let toYear = moment().year()
      if(this.props.postId in this.props.Timeline.comments){
        for(let current of this.props.Timeline.comments[this.props.postId]){
          comments.push(this.getComment(current, toYear))
          comments.push(this.getSubComment(current, toYear))
        }
      }
    }

    return comments
  }

  // コメント取得
  getComment(current, toYear){
    let comments = []

    let fromYear = (current.birthday)?moment(current.birthday, 'YYYYMMDD').year():1900
    let group = (Math.floor((toYear - fromYear) / 10)) * 10

    return (
      <li className={ current.approved ? "list__item" : "list__item not_approved"} key={current.commentId} id={`commentId${current.commentId}`}>
        <div className="timeline-comment comment_customer">
          <p className="timeline-comment__name">{group}代 {this.props.checkGender(current.gender)}</p>
          <p className="timeline-comment__time">{this.props.checkDiffDate(current.createdTime)}</p>
          <div className="timeline-comment__main">{current.commentValue}</div>
          <TimelineCommentInputContent postId={current.postId} commentId={current.commentId} />
        </div>
      </li>
    )
  }

  // サブコメント取得
  getSubComment(current, toYear){
    const fromYear = (current.birthday)?moment(current.birthday, 'YYYYMMDD').year():1900
    const group = (Math.floor((toYear - fromYear) / 10)) * 10;
    const currentTellerId = this.props.MyProfile.data.fortuneTellerId; 
    return (
      <TimelineSubCommentContent {...this.props} 
        currentTellerId={currentTellerId} 
        group={group} 
        subcomments={current.subcomments} 
        onClick={this.handleClick} 
      />
    )
  }

  render(){
    let comments = this.getComments()
    if(comments && this.props.isView){
      return (
        <div className="timeline__inner border_top">
          <ul className="list_comment column_1">
            {comments}
          </ul>
        </div>
      )
    }else{
      return false
    }
  }
}

const mapStateToProps = (store) => {
  return {
    Timeline: store.Timeline,
    MyProfile : store.MyProfile
  }
}

export default connect(
  mapStateToProps
)(TimelineCommentContent);
