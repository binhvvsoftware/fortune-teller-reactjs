/**
 * HOME画面のメインの表示部分
 *
 */
import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { HomeNews } from './HomeNews'
import HomeNextLogin from './HomeNextLogin'
import HomeStaffBlog from './HomeStaffBlog'
import HomeMission from './HomeMission'
import * as NewsActions from '../actions/NewsActions'
import * as StaffBlogActions from '../actions/StaffBlogActions'
import * as MissionActions from '../actions/MissionActions'
import * as Fetch from '../util/Fetch'

class Home extends Component {

  componentWillMount() {
    const { dispatch, News, StaffBlog, Mission, activeItem } = this.props
    activeItem(1)

    if (!News.loaded) {
      dispatch(NewsActions.fetchData(0,10))
    }

    if (!StaffBlog.loaded) {
      dispatch(StaffBlogActions.fetchData(1,10))
    }

    dispatch(MissionActions.getMissions())
  }

  render () {
    const { News, MyProfile, StaffBlog, Mission } = this.props
    return (
      <div className="content">
        { Fetch.isAgeVerify(MyProfile.data.verifyAge) ? '' : (
          <Link className="bar take_notice" to="/home/setting/ageauth">
            <i className="material-icons bar__icons">info_outline</i>
            年齢認証がお済みでないため、お仕事を開始できません。こちらから年齢認証を行ってください。
          </Link>
        ) }
        <HomeNextLogin />
        <HomeNews data={News} />
        <div className="main-content clearfix">
          <HomeMission data={Mission} />
          <HomeStaffBlog data={StaffBlog}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
    StaffBlog: state.StaffBlog,
    News: state.News,
    Mission: state.Mission
  }
}

export default connect(
  mapStateToProps
)(Home);
