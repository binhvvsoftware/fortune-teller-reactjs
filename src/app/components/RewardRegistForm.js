import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import request from 'axios'
import { glasConfig } from '../constants/Config'
import * as Fetch from '../util/Fetch'


export default class RewardRegistForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      registflg: 0, // 0:申請なし、1:申請あり
      transferdate: '',
      registdate: '',
      bankaccount: 0, // 0:銀行情報なし、1:あり
      commission: glasConfig.reward_commission, // 精算手数料
      bankminpay: glasConfig.payoff_min, // 銀行振込による精算の最低金額
    }
  }

  
  componentWillMount() {
    this.comfirmRegist()
  }
  

  /**
   * 精算の確認
   */
  comfirmRegist () {
    const url = glasConfig.url_outside + glasConfig.path_payment
    const options = {
      method: 'GET',
      url: url ,
      headers: {
          'Authorization': localStorage.getItem('token'),
      },
      json: true
    }

    request(options)
      .then(response => {
        if ( response.data.code === 0 ) {
          this.setState({
            transferdate: response.data.data.transferdate,
            registflg: response.data.data.registflg,
            bankaccount: response.data.data.bankaccount,
            commission: response.data.data.commission,
            bankminpay: response.data.data.bankminpay,
          })
        }
      })
      .catch(error => {
        //throw error
      })
  }

  /**
   * 精算の申請
   */
  handlerRegist () {
    const { MyProfile } = this.props

    const url = glasConfig.url_outside + glasConfig.path_payment
    const params = {
      point: MyProfile.data.point
    }
    const options = {
      method: 'PUT',
      url: url ,
      data: params,
      headers: {
          'Authorization': localStorage.getItem('token'),
          'Content-Type': 'application/json'
      },
      json: true
    }

    request(options)
      .then(response => {
        if ( response.data.code === 0 ) {
          this.setState({
            registflg: 1
          })
        }
      })
      .catch(error => {
        //throw error
      })
  }



  /**
   * 精算申請可能かの判定
   * return {boolean} true:可能
   */
  chkRegist () {
    const { MyProfile } = this.props
    if ( this.state.bankaccount === 1 && this.state.registflg === 0 &&  this.state.bankminpay <= MyProfile.data.point ) {
      return true
    }
    return false
  }


  render () {
    const { MyProfile } = this.props

    const point = MyProfile.data.point
    const commission = glasConfig.reward_commission
    const TransferMoney = ( point > commission ) ? point - commission : 0

    let ClassCheckOut = "checkout-box"
    if (this.state.registflg === 1){
      ClassCheckOut = ClassCheckOut + " checkout_done"
    } else {
      if ( this.chkRegist() ) {
        ClassCheckOut = ClassCheckOut + " checkout_ok"
      } else {
        ClassCheckOut = ClassCheckOut + " checkout_ng"
      }
    }

    return (
      <div className="checkout-box-wrap">
        <div className={ClassCheckOut}>
        {
          (this.state.registflg === 1) ?
            <div>
              <p className="checkout-box__title">精算申請完了</p>
              <p className="checkout-box__text">精算申請が完了しました<br />お振込予定日は 
              <strong className="checkout-box__emphasis">{this.state.transferdate}</strong>です<br />今しばらくお待ちくださいませ</p>
            </div>
          : 
            <div>
              <table className="table-checkout">
                <tbody>
                  <tr>
                    <th className="table-align-left">報酬確定額</th>
                    <td className="table-align-right">{Fetch.formatNumber(point)}円<br /><span className="text-secondary">{Fetch.formatNumber(point)}PT</span></td>
                  </tr>
                  <tr>
                    <th className="table-align-left">振込手数料</th>
                    <td className="table-align-right">-{Fetch.formatNumber(commission)}円</td>
                  </tr>
                  <tr className="table-checkout__result">
                    <th className="table-align-left">振込予定額</th>
                    <td className="table-align-right">{Fetch.formatNumber(TransferMoney)}円</td>
                  </tr>
                </tbody>
              </table>
              <p className="checkout-box__text">
                <CheckOutext MyProfile={MyProfile} data={this.state} />
              </p>
              <div className="btn-wrap">
                {this.state.bankaccount === 0 ?
                  <Link className="btn-raised color_default" to='/home/setting/bank/top' >銀行口座を登録する</Link>
                  : 
                  <button className="btn-raised color_default" disabled={!this.chkRegist()} onClick={()=>this.handlerRegist()}>精算する</button>
                }
              </div>
            </div>
        }
        </div>
      </div>
    )
  }
}

const CheckOutext = ({MyProfile,data}) => {
  const restPoint = data.bankminpay - MyProfile.data.point
  if ( data.bankaccount === 0 ) {
    return <strong className='checkout-box__emphasis'>お振込先が未登録のため精算できません</strong>
  } else {
    if ( data.registflg === 0 ) {
      if ( data.bankminpay <= MyProfile.data.point ) {
        return <span>ただいま精算申請されますと<br />お振込予定日は <strong className='checkout-box__emphasis'>{data.transferdate}</strong> となります</span>
      } else {
        return (
          <strong className='checkout-box__emphasis'>精算可能額まであと<br />{Fetch.formatNumber(restPoint)}円（{Fetch.formatNumber(restPoint)}PT）必要です</strong>
        )
      }
    }
  }

}