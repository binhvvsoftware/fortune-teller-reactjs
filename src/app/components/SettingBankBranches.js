import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { glasConfig, representativeBank, katakanaList } from '../constants/Config'
import SettingMenu from './SettingMenu'
import * as MyBankActions from '../actions/MyBankActions'
import $ from 'jquery'

/**
 * 銀行支店頭文字選択
 */

class SettingBankBranches extends Component {

  constructor (props) {
    super(props)
    this.state = {
      code: this.props.match.params.code,
      branches: [],
      katakana: null,
      back: false
    }
  }

  componentWillMount() {
    const { dispatch } = this.props
    dispatch(MyBankActions.getBankCode(this.state.code))
    dispatch(MyBankActions.getBranch(this.state.code))
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.Bank.branchInitial){
      let initials = []
      let branches = nextProps.Bank.branchInitial
      let re = /([^A-Za-z]).*/
      branches.forEach((current, index)=>{
        let matches = current.siten_furi.match(re)
        if(matches) {
          let zenkaku = this.toZenkakuKatakana(matches[1])
          if(initials.indexOf(zenkaku) === -1){
            initials.push(zenkaku)
          }
        }
      })

      initials.sort((a, b) => {
          a = this.toKatakanaHiragana(a.toString());
          b = this.toKatakanaHiragana(b.toString());
          if(a < b){
              return -1;
          }else if(a > b){
              return 1;
          }
          return 0;
      });

      this.getKatakanaInitial(initials)
    }
  }

  toZenkakuKatakana(str){
    let ret = []

    let m =
    {
      0xFF61:0x300C, 0xFF62:0x300D, 0xFF63:0x3002, 0xFF64:0x3001, 0xFF65:0x30FB, // 。「」、・
      0xFF67:0x30A1, 0xFF68:0x30A3, 0xFF69:0x30A5, 0xFF6A:0x30A7, 0xFF6B:0x30A9, // ァ
      0xFF6C:0x30E3, 0xFF6D:0x30E5, 0xFF6E:0x30E7, 0xFF6F:0x30C3, 0xFF70:0x30FC, // ャュョッー
      0xFF71:0x30A2, 0xFF72:0x30A4, 0xFF73:0x30A6, 0xFF74:0x30A8, 0xFF75:0x30AA, // ア
      0xFF76:0x30AB, 0xFF77:0x30AD, 0xFF78:0x30AF, 0xFF79:0x30B1, 0xFF7A:0x30B3, // カ
      0xFF7B:0x30B5, 0xFF7C:0x30B7, 0xFF7D:0x30B9, 0xFF7E:0x30BB, 0xFF7F:0x30BD, // サ
      0xFF80:0x30BF, 0xFF81:0x30C1, 0xFF82:0x30C4, 0xFF83:0x30C6, 0xFF84:0x30C8, // タ
      0xFF85:0x30CA, 0xFF86:0x30CB, 0xFF87:0x30CC, 0xFF88:0x30CD, 0xFF89:0x30CE, // ナ
      0xFF8A:0x30CF, 0xFF8B:0x30D2, 0xFF8C:0x30D5, 0xFF8D:0x30D8, 0xFF8E:0x30DB, // ハ
      0xFF8F:0x30DE, 0xFF90:0x30DF, 0xFF91:0x30E0, 0xFF92:0x30E1, 0xFF93:0x30E2, // マ
      0xFF94:0x30E4,                0xFF95:0x30E6,                0xFF96:0x30E8, // ヤ
      0xFF97:0x30E9, 0xFF98:0x30EA, 0xFF99:0x30EB, 0xFF9A:0x30EC, 0xFF9B:0x30ED, // ラ
      0xFF9C:0x30EF,                0xFF66:0x30F2,                0xFF9D:0x30F3, // ワヲン
      0xFF70:0x30FC, // ー
      0xFF9E:0x309B, 0xFF9F:0x309C // 濁点、半濁点
    }

    let c = str.charCodeAt(0)
    ret.push(m[c] || c)
    return String.fromCharCode.apply(null, ret)
  }

  toKatakanaHiragana(src){
    return src.replace(/[\u30a1-\u30f6]/g, (match) => {
        let chr = match.charCodeAt(0) - 0x60
        return String.fromCharCode(chr)
    })
  }

  getKatakanaInitial(initials){
    let parentPath = this.getParentName(this.props.match.path)

    let katakana = Array.prototype.map.call(katakanaList, (current, index)=>{
      if(initials.indexOf(current.katakana) !== -1){
        return (<li className="list__item"><Link to={{pathname: `${parentPath}/initials/${this.state.code}/${current.initials}`}} className="bank-link width_short" data-initials={current.initials}>{current.katakana}</Link></li>)
      }else{
        return (<li className="list__item"><span className="bank-link width_short">{current.katakana}</span></li>)
      }
    })

    this.setState({
      katakana: katakana
    })
  }

  getInitialBranches(initials){
    for(let branch in this.state.branches){
      if(this.state.branches[branch].kana.indexOf(initials) === 0){
        return true
      }
    }
    return false
  }

  getParentName(path){
    return path.split('/').slice(0, -1).join('/')
  }

  handleClick(e){
    this.setState({
      back: true
    })
  }

  componentDidMount(){
    $('.secondary-header').addClass('hiddenClass');
    $('.header-back').removeClass('hiddenClass');
    $(".component-title").text("銀行口座情報編集");
  }

  componentWillUnmount(){
    $('.secondary-header').removeClass('hiddenClass');
    $('.header-back').addClass('hiddenClass');
    $(".component-title").text("");
    $('.menu-header').removeClass('hiddenClass');
  }

  render(){
    if(this.state.back){
      return <Redirect to={`${this.props.match.path.split('/').slice(0, -2).join('/')}/top`} />
    }

    let katakana = null
    if(this.state.katakana){
      katakana = this.state.katakana
    }

    let content = ''
    if(this.state.code === glasConfig.bank.yucho){
      content = (
        <div>
          <p>下記より該当する銀行名の先頭文字を選択してください。</p>
          <p>支店名は漢数字の読みになります。</p>
          <p>例)支店名「三一八」は表の「サ」を選択。</p>
          <p><a href="http://www.jp-bank.japanpost.jp/kojin/sokin/furikomi/kouza/kj_sk_fm_kz_1.html" target="_blank">ゆうちょ記号番号から振込用の支店名・口座番号を調べる</a></p>
        </div>
      )
    }else{
      content = (
        <p>下記より該当する銀行名の先頭文字を選択してください。</p>
      )
    }

    let bank_name = ''
    if(this.props.Bank.bank.length){
      bank_name = this.props.Bank.bank[0].bank_name
    }

    return (
      <div className="content bank-detail bank-branches-setting">
        <h1 className="content__title">設定</h1>
        <SettingMenu />
        <div className="primary">
          <div className="hidden-mobile">
            <div className="primary-title">
              <h1 className="primary-title__main">銀行口座情報</h1>
            </div>
            <div className="primary__inner">
              <div className="bank-box">
                <table className="table-th-left">
                  <tbody>
                    <tr>
                      <th className="table-align-left">銀行名</th>
                      <td className="table-align-left">{bank_name}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="primary-title">
              <h1 className="primary-title__main">支店名選択</h1>
            </div>
            <div className="primary__inner">
              <div className="primary-title lv_2">
                {content}
              </div>
              <ul className="list japanese_syllabary margin_bottom">
                {katakana}
              </ul>
              <input type="button" className="btn-raised margin_right" value="戻る" onClick={e=>this.handleClick(e)} />
            </div>
          </div>
          <div className="display-mobile">
            <div className="primary__inner">
              <div className="primary-content">
                <div className="primary-content__header">
                  <div className="primary-content__header__left">銀行名</div>
                  <div className="primary-content__header__right">{bank_name}</div>
                </div>
                <div className="primary-content__body">
                  <div className="primary-title lv_2">
                    <h2 className="primary-title__main">支店名を選択</h2>
                  </div>
                  <ul className="list japanese_syllabary margin_bottom">
                      {katakana}
                  </ul>
                  <input type="button" className="btn-raised margin_right" value="戻る" onClick={e=>this.handleClick(e)} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { Bank: state.Bank }
}

export default connect(
  mapStateToProps
)(SettingBankBranches)
