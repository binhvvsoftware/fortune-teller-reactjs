import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import { glasConfig } from '../constants/Config'
import { Category } from '../constants/ConfigStaffblog'
import request from 'axios'
import * as moment from 'moment'
import * as Fetch from '../util/Fetch'
import StaffBlog from './StaffBlog'
import $ from 'jquery';

export default class StaffBlogDetail extends Component {

  constructor (props) {
    super(props)
    this.state = {
      data: {}
    }
  }

  componentWillMount() {
    this.getData(this.props.match.params.id)
  }

  componentDidMount(){
    $('.secondary-header').addClass('hiddenClass');
    $('.header-back').removeClass('hiddenClass');
    $(".component-title").text("運営ブログ");
  }

  componentWillUnmount(){
    $('.secondary-header').removeClass('hiddenClass');
    $('.header-back').addClass('hiddenClass');
    $(".component-title").text("");
  }

  componentWillReceiveProps(nextProps) {
    this.getData(nextProps.match.params.id)
  }

  /**
   * 記事詳細を取得
   */
  getData (id) {
    const url = glasConfig.staff_blog_url + "/" + id
    const options = {
      method: 'GET',
      url: url ,
      headers: {
          'Authorization': localStorage.getItem('token'),
      },
      json: true
    }

    request(options)
      .then(response => {
        this.setState({
          data: Object.keys(response.data).length ? response.data : {}
        })
      })
      .catch(error => {
        //throw error
      })
  }



  render () {

    if ( !Object.keys(this.state.data).length ) {
      return null
    }


    return (
      <div className="content">
        <h1 className="content__title">運営ブログ</h1>
        <Link to="/home/staffblog/1" className="link-to-staff-log" >一覧に戻る</Link>
        <ol className="breadcrumb hidden-sp">
          <li className="breadcrumb__item"><Link to="/home/staffblog/1">運営ブログ</Link></li>
          {Object.values(this.state.data.categories).map( (res) => {
            return <li key={res} className="breadcrumb__item hidden-sp"><p>{Category[res]}</p></li>
          })}
        </ol>
        <div className="card-wrap">
          <div className="card view_detail">
            <div className="card-main">
              <div className="card-main__inner">
                <h3 className="card-main__title">{this.state.data.title.rendered}</h3>
                <p className="card-main__time">{moment(this.state.data.date).format("YYYY/MM/DD HH:mm")}</p>
                <ul className="list">
                  {Object.values(this.state.data.categories).map( (res) => {
                    return <li className="list__item"><Link to={`/home/staffblog/1/${res}`}>{Category[res]}</Link></li>
                  })}
                </ul>
                <div className="card-main__content" dangerouslySetInnerHTML={{ __html: this.state.data.content.rendered }} />
              </div>
            </div>
          </div>
        </div>
        <h2 className="content__title lv_2">関連記事</h2>
        <StaffBlog category={this.state.data.categories[0]} page={1} size={7} exclude={this.state.data.id} />
      </div>
    )
  }
}