/**
 * チャット画面　右カラムのメモ
 */
import React, {Component} from 'react'
import { connect } from 'react-redux';
import * as UserInfoActions from '../actions/UserInfoActions'
import Dialog from 'material-ui/Dialog';
import $ from 'jquery';


class ChatUserMemo extends Component {

  constructor (props) {
    super(props)
    this.state = {
      memo: '',
      showNotice: false,
      showExpansion: false
    }
  }


  componentWillMount () {
    const { UserInfo } = this.props
    this.setState({
      memo: !UserInfo.memo ? '' : UserInfo.memo
    })
  }

  componentWillReceiveProps (nextProps) {
    const { UserInfo } = this.props
    const memo = nextProps.UserInfo.memo ? nextProps.UserInfo.memo : ''
    this.setState({
      memo: memo
    })
  }


  /**
   * 入力イベント
   * 
   * @param {*} e イベント
   * @return {void}
   */
  handleChange (e) {
    const memo = e.target.value
    this.setState({
      memo: memo
    })
  }


  /**
   * フォーカスが外れたときのイベント（入力を保存）
   * 
   * @return {void}
   */
  handleBlur () {
    //フォーカスが外れた時にメモを保存する
    const { dispatch,UserInfo } = this.props
    const orgmemo = UserInfo.memo ? UserInfo.memo : ''
    if (orgmemo === this.state.memo ){
      return 
    }
    const data = UserInfo
    data['memo'] = this.state.memo
    dispatch(UserInfoActions.post('memo',data))
    this.setState({
      showNotice: true
    })
  }

  handleExpansion () {
    this.setState({
      showExpansion: !this.state.showExpansion
    })
  }
  

  render () {
    const { show, UserInfo } = this.props
    if ( !(UserInfo.userId && show) ) {
      return null
    }
    $('.chat-memo-wrap').click(function (event) {
      event.stopPropagation();
    });
    return (
        <div className="chat-memo-wrap" >
          <div className="chat-memo">
            <Dialog 
              modal={false}
              open={this.state.showExpansion}
              onRequestClose={()=>this.setState({showExpansion:false})}
            >
              <div className="chat-memo-dialog" onClick={this.stopChatMemory}>
                <TextArea 
                  show={true} 
                  memo={this.state.memo} 
                  changeFunc={(e)=>this.handleChange(e)} 
                  blurFunc={()=>this.handleBlur()} 
                  focusFunc={()=>this.setState({showNotice:false})} 
                />
                <a className="chat-memo__scale-down" href="javascript:;" onClick={()=>this.setState({showExpansion:false})}><i className="material-icons">remove</i></a>
              </div>
            </Dialog>
          <TextArea 
            show={!this.state.showExpansion} 
            memo={this.state.memo} 
            changeFunc={(e)=>this.handleChange(e)} 
            blurFunc={()=>this.handleBlur()} 
            focusFunc={()=>this.setState({showNotice:false})} 
          />
          {this.state.showExpansion ? <div className="chat-memo-hide-text">メモ欄拡大表示中</div> : ''}

          <label className="chat-memo__label" htmlFor="secondary_comment"></label>
          <a className="chat-memo__fullscreen" href="javascript:;" onClick={()=>this.setState({showExpansion:!this.state.showExpansion})}><i className="material-icons">fullscreen</i></a>
          {this.state.showNotice ? <p className="chat-memo__text"><i className="material-icons chat-memo__icon">check</i>保存しました</p> : ''}

        </div>
      </div>
    )
  }
}

const TextArea = ({show,memo,changeFunc,blurFunc,focusFunc}) => {
  if (!show) {
    return null
  }
  return (
    <div style={{height:'100%',width:'100%'}} >
      <textarea 
        style={{width:"100%"}} 
        placeholder="メモはアナタ以外に見られることはありません" 
        className="chat-memo__input" 
        value={memo ? memo : ''} 
        onChange={(e)=>changeFunc(e)} 
        onBlur={()=>blurFunc()} 
        onFocus={()=>focusFunc()}
      />
      <textarea 
        style={{width:"98%"}} 
        placeholder="メモを記入してください" 
        className="chat-memo__input-sp" 
        value={memo ? memo : ''} 
        onChange={(e)=>changeFunc(e)} 
        onBlur={()=>blurFunc()} 
        onFocus={()=>focusFunc()}
      />
    </div>
  )
}

const mapStateToProps = (state) => {
  return {}
}

export default connect(
  mapStateToProps
)(ChatUserMemo)