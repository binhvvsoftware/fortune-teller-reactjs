import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';

class HelpDetail extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        $('.secondary-header').addClass('hiddenClass');
        $('#lead-nav').addClass('hiddenClass');
        $('.header-back').removeClass('hiddenClass');
        $(".component-title").text("ヘルプ");
      }
    
      componentWillUnmount(){
        $('.secondary-header').removeClass('hiddenClass');
        $('.menu-header').removeClass('hiddenClass');
        $('.header-back').addClass('hiddenClass');
        $(".component-title").text("");
      }

    render() {
        const { data } = this.props;
        const msg = data.body

        return (
            <div>
                <Link to="/home/help" className="link-to-home-help" onClick={() => this.handleReset()} style={{ fontSize: 12, color: 'rgba(17, 173, 157, 1)', margin: '8px 0px 12px', display: 'inherit !important' }} >一覧に戻る</Link>
                <div className="primary">
                    <div className="primary-title">
                        <h1 className="primary-title__main">
                            {data ? data.title : "見つかりませんでした"}
                        </h1>
                    </div>
                    {data ? (
                        <div className="primary__inner">
                            <p>{(msg && msg.match("\n") ?
                                msg.split("\n").map((m, i) => {
                                    return (<p key={`${data.id}_${i}`}>{m}</p>)
                                }) : msg
                            )}</p>
                        </div>
                    ) : ''}
                </div>
            </div>
        )
    }
}

export default HelpDetail;