/**
 * 占い師の待機状態
 * 
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as Fetch from '../util/Fetch'
import * as MyProfileActions from '../actions/MyProfileActions'
import * as MyAccountActions from '../actions/MyAccountActions'
import { fortuneTellerStatus, fortuneTellerStatusToString, LIMIT_CHAT_DEFAULT } from '../constants/Config'
import Toggle from 'material-ui/Toggle'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DialogNotifi from './Dialog'

class StatusControl extends Component {

  constructor(props) {
    super(props)
    const toggleDefault = this.getToggleDefault()
    this.state = {
      chat: toggleDefault.chat,
      call: toggleDefault.call,
      saved : true,
      limitChat : this.props.MyProfile.data.maxNumberUserChatting ? this.props.MyProfile.data.maxNumberUserChatting : LIMIT_CHAT_DEFAULT
    }
  }

  componentWillReceiveProps(nextProps){
    if(this.props.show) {
      this.setState({saved: true});
    }
    
  }

  componentWillMount(){
    const { dispatch,MyAccount } = this.props
    dispatch(MyAccountActions.fetch())
  }

  //待機状態からtoggleのdefaultの基本設定を取得する
  getToggleDefault() {

    const { MyProfile } = this.props
    const status = MyProfile.data.fortuneTellerStatus

    // chat: チャット待機、両待機であればtrueを返し、defaultで選択されている状態となる
    // call: 通話待機、両待機であればtrueを返し、defaultで選択されている状態となる
    // disabled : 通話中やチャット中のステータスであれば変更できないように、trueを返します。trueで変更不可、falseで変更可
    // status: 「待機中、対応中、オフライン」ステータスから状態を返します。

    switch (status) {
      case fortuneTellerStatus.waiting: {
        return { chat: true, call: true, disabled: false, status: "waiting" }
      }
      case fortuneTellerStatus.chat: {
        return { chat: true, call: false, disabled: false, status: "waiting" }
      }
      case fortuneTellerStatus.call: {
        return { chat: false, call: true, disabled: false, status: "waiting" }
      }
      case fortuneTellerStatus.chatting: {
        return { chat: false, call: false, disabled: true, status: "busy" }
      }
      case fortuneTellerStatus.calling: {
        return { chat: false, call: false, disabled: true, status: "busy" }
      }
      case fortuneTellerStatus.offline: {
        return { chat: false, call: false, disabled: false, status: "offline" }
      }
      case fortuneTellerStatus.chattingFull: {
        return { chat: false, call: false, disabled: true, status: "busy" }
      }
      default: {
        return { chat: false, call: false, disabled: false, status: "busy" }
      }
    }
  }

  //チャット、通話の選択状況によってAPIにリクエストする値を取得する
  //メソッド内でstateから取得しない理由は、setStateのあとにstateが即時に反映されないためです 
  getStatusByToggle(chat, call) {
    if (chat && call) {
      return fortuneTellerStatus.waiting
    }
    if (chat && !call) {
      return fortuneTellerStatus.chat
    }
    if (!chat && call) {
      return fortuneTellerStatus.call
    }
    return fortuneTellerStatus.offline
  }


  //toggleの状態が変更されたときに実行される
  handleChange(e) {

    const key = e.target.name
    const value = e.target.checked
    const { MyAccount } = this.props

    this.setState({
      [key]: value,
      saved: false
    })

    const toggleDefault = this.getToggleDefault()

    if( key === 'call' && value && MyAccount.data.phoneNumber === null ){
      this.setState({
        call: false
      });
      this.refs.dialog.handleOpen('REQUIRE_PHONE_NUMBER', {
        message: "電話相談を受け付けるためには電話番号を登録してください。"
      })
      return;
    }
    //今現在、待機状態であればtoggleの値が変更された時に即時反映。待機ボタンを押さない
    if (toggleDefault.status === 'waiting') {
      let chat = null
      let call = null
      if (key === 'chat') {
        chat = value
        call = this.state.call
      } else if (key === 'call') {
        chat = this.state.chat
        call = value
      }
      if (chat !== null && call !== null) {
        this.doChangeStatus(this.getStatusByToggle(chat, call))
      }
    }
  }


  //待機、オフラインどちらかのボタンが押された時に実行されます
  handleChangeStatus(e, type) {
    const { MyProfile,MyAccount } = this.props
    e.stopPropagation()
    
    if (!Fetch.isAgeVerify(MyProfile.data.verifyAge)) {
      if (Object.keys(this.refs).length) {
        this.refs.dialog.handleOpen('AGEAUTH')
      }
      return
    }

    if (!MyProfile.data.avatarPath) {
      if (Object.keys(this.refs).length) {
        this.refs.dialog.handleOpen('ALERT', {
          message: "まずはプロフィール写真を設定してください"
        })
      }
      return
    }
    if(this.state.call === true && MyAccount.data.phoneNumber === null && Object.keys(this.refs).length){ 
      this.setState({
        call: false,
      })
      this.refs.dialog.handleOpen('REQUIRE_PHONE_NUMBER', { 
        message: "電話相談を受け付けるためには電話番号を登録してください。" 
      }) 
      return
    }

    const toggleDefault = this.getToggleDefault()

    if (type === 'offline') {
      if (toggleDefault.status === 'busy' || toggleDefault.status === 'offline') {
        //オフラインや通話中などの時にオフラインボタンを押されたときは無視
        return
      }
      this.setState({
        chat: false,
        call: false
      })
      this.doChangeStatus(fortuneTellerStatus.offline)
    } else if ( type === 'wait' ) {
        this.doChangeStatus(this.getStatusByToggle( this.state.chat,  this.state.call ))
        window.addEventListener("beforeunload", (ev) => {  
          ev.preventDefault();
          return ev.returnValue = 'Are you sure you want to close?';
        }); 
    }
    this.setState({saved: true});
  }

  //glasAPIにリクエスト
  doChangeStatus(status) {
    const { dispatch, MyProfile } = this.props
    const data = MyProfile.data
    data['fortuneTellerStatus'] = status
    data['maxNumberUserChatting'] = this.state.limitChat
    dispatch(MyProfileActions.put('status', data))
  }

  onChangeLimitChat = (event, index, value) => {
    this.setState({ limitChat : value }, () => {
      const chat = this.state.chat
      const call = this.state.call
      if (chat !== null && call !== null) {
        this.doChangeStatus(this.getStatusByToggle(chat, call))
      } else {
        const { MyProfile } = this.props;
        this.doChangeStatus(MyProfile.data.fortuneTellerStatus)
      }
    });
  }

  render() {

    if (!this.props.show) {
      return null
    }

    const { MyProfile,MyAccount } = this.props
    const toggleDefault = this.getToggleDefault()

    //待機ボタンの表示について
    let StatusChangeButton = ''
    switch (toggleDefault.status) {
      case "waiting": {
        StatusChangeButton = (
          <button className="btn-raised color_accsent spread_width" onClick={e => this.handleChangeStatus(e, 'offline')}>オフラインにする</button>
        )
        break
      }
      case "offline": {
        StatusChangeButton = (
          <button className="btn-raised color_default spread_width" disabled={this.state.saved || !(this.state.call || this.state.chat)} onClick={e => this.handleChangeStatus(e, 'wait')}>待機する</button>
        )
        break
      }
      case "busy": {
        StatusChangeButton = (
          <p className="secondary-workstart-btn__text">{fortuneTellerStatusToString[MyProfile.data.fortuneTellerStatus]}<br />はステータスを変更できません</p>
        )
        break
      }
    }
    return (
      <div>
        <DialogNotifi ref='dialog' />
        <div className="secondary-workstart-wrap">
          <div className="secondary-workstart">
            <div className="secondary-workstart-btn">
              {StatusChangeButton}
            </div>
            <div className="secondary-workstart-toggle">
              <Toggle
                label="チャット待機"
                name='chat'
                defaultToggled={toggleDefault.chat}
                disabled={toggleDefault.disabled}
                toggle={String(this.state.chat)}
                onToggle={e => this.handleChange(e)}
              />
            </div>
            {
              ( this.state.chat || toggleDefault.chat || 
                (toggleDefault.status == 'busy' 
                  && (MyProfile.data.fortuneTellerStatus == fortuneTellerStatus.chatting || MyProfile.data.fortuneTellerStatus == fortuneTellerStatus.chattingFull)) ) &&
              <div className="secondary-workstart-toggle" style={{ paddingTop: 0, paddingBottom : 0}}>
                <div style={{display : 'flex', flexDirection : 'row', alignItems: 'baseline'}}>
                  <label 
                  style={ toggleDefault.disabled ? 
                    {
                      flex : 2,    
                      lineHeight: '24px',
                      color: '#b8b8b8',
                      cursor : 'not-allowed',
                      fontFamily: 'Roboto, sans-serif'
                    } :
                    {
                      flex : 3,    
                      lineHeight: '24px',
                      color: 'rgba(0, 0, 0, 0.87)',
                      fontFamily: 'Roboto, sans-serif'
                    }}>
                    同時鑑定
                  </label>
                  {
                    ! toggleDefault.disabled && 
                    <SelectField
                      onChange={ this.onChangeLimitChat }
                      value={ this.state.limitChat }
                      disabled={toggleDefault.disabled}
                      underlineStyle={{ top : '40px' }}
                      style={{ flex : 1, top : '4px', fontSize : '15px' }}
                    >
                      <MenuItem value={5} primaryText="5人" />
                      <MenuItem value={4} primaryText="4人" />
                      <MenuItem value={3} primaryText="3人" />
                      <MenuItem value={2} primaryText="2人" />
                      <MenuItem value={1} primaryText="1人" />
                    </SelectField>
                  }
                  {
                    ! toggleDefault.disabled && 
                    <div style={{flex : 1, whiteSpace : 'nowrap'}}>まで</div>
                  }

                  {
                    toggleDefault.disabled && 
                    <div style={{flex : 1, paddingLeft : 40, color : '#b8b8b8', whiteSpace : 'nowrap'}}>{this.state.limitChat}人まで</div>
                  }
                </div>
            </div>
            }
            <div className="secondary-workstart-toggle">
              <Toggle
                label="通話相談"
                name='call'
                defaultToggled={toggleDefault.call}
                disabled={toggleDefault.disabled}
                toggle={String(this.state.call)}
                onToggle={e => this.handleChange(e)}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    MyProfile: state.MyProfile,
    MyAccount: state.MyAccount
  }
}

export default connect(
  mapStateToProps
)(StatusControl)