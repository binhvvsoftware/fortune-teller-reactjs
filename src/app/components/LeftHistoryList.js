/**
 * 通話・チャットリクエスト（左カラムの対応中(inConversation:true)、相談依頼申請一覧(inConversation:false)）
 */
import React, {Component} from 'react'
import * as ConfigMessage from '../constants/ConfigMessage'
import * as Fetch from '../util/Fetch'
import { Link } from 'react-router-dom'
import moment from 'moment'
require('moment/locale/ja')

export default class LeftHistoryList extends Component {

  constructor (props) {
    super(props)
    const orgtime = this.makeOrgTime(this.props.data.serverTime)
    this.state = {
      org: orgtime,
      view: moment(orgtime).fromNow()
    }
  }

  makeOrgTime (time) {
    moment.locale('ja')
    return moment(time,"YYYYMMDDHHmmss").add(9,'hours').format('YYYY-MM-DD HH:mm:ss')
  }

  countUp () {
    this.setState({
      view : moment(this.state.org).fromNow()
    })
  }

  componentDidMount () {
    this.interval = setInterval(()=>this.countUp(), 1000);
  }

  componentWillReceiveProps (nextProps) {
    const nexttime = this.makeOrgTime(nextProps.data.serverTime)
    if (nexttime !== this.state.orgtime ) {
      this.setState({
        org: nexttime,
        view: moment(nexttime).fromNow()
      })
    }
  }

  componentWillUnmount () {
    clearInterval(this.interval);
  }

  render () {

    const { data, chatUserId, closeNavLeft } = this.props

    if (!data.fromId) {
      return null
    }

    const msgMap = ConfigMessage.getMessageData(data,'displayTextLeftHistory')
    const tellerId = Fetch.tellerId()
    const userId = ( data.fromId === tellerId ) ? data.toId : data.fromId

    return (
      <Link to={`/home/chat/${userId}`} className="list__item" key={data.msgId + data.serverTime} onClick={closeNavLeft}>
        <div className={(chatUserId === userId) ? "btn-customer customer_request-choose" : "btn-customer customer_default"}>
          <p className="btn-customer__item is_name">{data.fromName ? data.fromName : data.friendName}</p>
          <p className="btn-customer__item is_time">{this.state.view}</p>
          <p className="btn-customer__item is_comment">{msgMap.msg}</p>
        </div>
      </Link>
    )
  }
}
