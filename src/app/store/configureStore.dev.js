import { createStore , applyMiddleware, compose } from 'redux'
import { persistState } from 'redux-devtools';
import thunk from 'redux-thunk'
import rootReducers from '../reducers'
import { createLogger } from 'redux-logger'
import DevTools from '../components/DevTools';

const enhancer = compose(
  applyMiddleware(thunk),
  DevTools.instrument(),
  persistState(
    window.location.href.match(
      /[?&]debug_session=([^&#]+)\b/
    )
  )
);

const middlewares = [thunk]
middlewares.push(createLogger())

export default function configureStore() {
  const store = createStore(
    rootReducers,
    enhancer,
    applyMiddleware(...middlewares)
  )
  return store
}
