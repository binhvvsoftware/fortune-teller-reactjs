import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import BaseController from './app/components/BaseController'
import { PrivateRoute } from './app/components/PrivateRoute'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import LoadingBar from './app/components/LoadingBar'
import Layout from './app/components/Layout'
import Login from './app/components/Login'
import Password from './app/components/Password'
import Contract from './app/components/Contract'
import PasswordReset from './app/components/PasswordReset'
import PasswordAnnounce from './app/components/PasswordAnnounce'
// import AsyncController from './app/components/AsyncController';

// const Layout = AsyncController(() => import('./app/components/Layout'));
// const Login = AsyncController(() => import('./app/components/Login'));
// const Password = AsyncController(() => import('./app/components/Password'));
// const Contract = AsyncController(() => import('./app/components/Contract'));
// const PasswordReset = AsyncController(() => import('./app/components/PasswordReset'));
// const PasswordAnnounce = AsyncController(() => import('./app/components/PasswordAnnounce'));

class App extends Component {

  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div>
          <LoadingBar />
          <BaseController />
          <Switch>
            <PrivateRoute path="/home" component={Layout} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/password" component={Password} />
            <Route exact path="/password_reset/:auth_code" component={PasswordReset} />
            <Route exact path="/announce" component={PasswordAnnounce} />
            <Route exact path="/contract" component={Contract} />
            <Route exact path="/" render={props=><PrivateRoute component={Layout} />} />
            <Route component={Login} />
          </Switch>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default App
